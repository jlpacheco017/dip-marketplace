const obtenerNombreDelRegister = () => {

    const inputUsernameRegister = document.querySelector('#username');
    return inputUsernameRegister.value;
}

const obtenerEmailDelRegister = () => {

    const inputEmailRegister = document.querySelector('#email');
    return inputEmailRegister.value;
}

const obtenerPasswordDelRegister = () => {

    const inputPasswordRegister = document.querySelector('#password');
    return inputPasswordRegister.value;
}

const obtenerPasswordDelRegisterRep = () => {

    const inputPasswordRegisterRep = document.querySelector('#password_confirmation');
    return inputPasswordRegisterRep.value;
}

validarUsername = () => {
    const inputUsernameRegister = obtenerNombreDelRegister();
    const patternCharacters = /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]+$/u
    const charactersText = document.querySelector('#characters');

    if (inputUsernameRegister.length === 0) {
        charactersText.setAttribute("hidden", false);
    } else if (inputUsernameRegister.length != 0) {
        charactersText.removeAttribute("hidden");
    }

    // CHECK CHARACTERS
    if (!inputUsernameRegister.match(patternCharacters)) {
        charactersText.classList.remove("validated");
        charactersText.classList.add("errorpass");
    } else {
        charactersText.setAttribute("hidden", false);
    }
};

validarEmail = () => {
    const inputEmailRegister = obtenerEmailDelRegister();

    const patternEmail = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
    const emailText = document.querySelector('#emailText');

    if (inputEmailRegister.length === 0) {
        emailText.setAttribute("hidden", false);
    } else if (inputEmailRegister.length != 0) {
        emailText.removeAttribute("hidden");
    }

    // CHECK EMAIL
    if (!inputEmailRegister.match(patternEmail)) {
        emailText.classList.remove("validated");
        emailText.classList.add("errorpass");
    } else {
        emailText.setAttribute("hidden", false);
    }
};

validarPassword = () => {
    const inputPasswordRegister = obtenerPasswordDelRegister();
    const inputPasswordRegisterRep = obtenerPasswordDelRegisterRep();

    const patternUpper = /(?=.*[A-Z])/
    const patternMinus = /(?=.*[a-z])/
    const patternDigit = /(?=.*[0-9])/

    const lenghtText = document.querySelector('#lenght');
    const upperText = document.querySelector('#mayuscula');
    const minusText = document.querySelector('#minuscula');
    const digitText = document.querySelector('#numero');
    const equalsText = document.querySelector('#passEquals');

    if (inputPasswordRegister.length === 0) {
        upperText.setAttribute("hidden", false);
        minusText.setAttribute("hidden", false);
        digitText.setAttribute("hidden", false);
        lenghtText.setAttribute("hidden", false);
        equalsText.setAttribute("hidden", false);
    } else {
        upperText.removeAttribute("hidden");
        minusText.removeAttribute("hidden");
        digitText.removeAttribute("hidden");
        lenghtText.removeAttribute("hidden");
    }

    // CHECK LENGTH
    if (inputPasswordRegister.length > 7) {
        lenghtText.classList.remove("errorpass");
        lenghtText.classList.add("validated");
    } else {
        lenghtText.classList.remove("validated");
        lenghtText.classList.add("errorpass");
    }

    // CHECK MAYUS
    if (inputPasswordRegister.match(patternUpper)) {
        upperText.classList.remove("errorpass");
        upperText.classList.add("validated");
    } else {
        upperText.classList.remove("validated");
        upperText.classList.add("errorpass");
    }

    // CHECK MINUS
    if (inputPasswordRegister.match(patternMinus)) {
        minusText.classList.remove("errorpass");
        minusText.classList.add("validated");
    } else {
        minusText.classList.remove("validated");
        minusText.classList.add("errorpass");
    }

    // CHECK DIGIT
    if (inputPasswordRegister.match(patternDigit)) {
        digitText.classList.remove("errorpass");
        digitText.classList.add("validated");
    } else {
        digitText.classList.remove("validated");
        digitText.classList.add("errorpass");
    }

    // CHECK LENGTH PASSWORD REP
    if (inputPasswordRegisterRep.length === 0) {
        equalsText.setAttribute("hidden", false);
    } else if (inputPasswordRegisterRep.length != 0 && inputPasswordRegister !== "") {
        equalsText.removeAttribute("hidden");
    }

    // CHECK PASSWORD EQUALS
    if (inputPasswordRegister === inputPasswordRegisterRep) {
        equalsText.classList.remove("errorpass");
        equalsText.classList.add("validated");
        equalsText.innerHTML = '● Las contraseñas coinciden.';
    } else {
        equalsText.classList.remove("validated");
        equalsText.classList.add("errorpass");
        equalsText.innerHTML = '● Las contraseñas no coinciden.';
    }
}

if (document.querySelector('#username')) {
    const inputUsernameRegister = document.querySelector('#username');
    inputUsernameRegister.addEventListener('input', () => {
        validarUsername();
    });
}

if (document.querySelector('#email')) {
    const inputEmailRegister = document.querySelector('#email');

    inputEmailRegister.addEventListener('input', () => {
        validarEmail();
    });
}

if (document.querySelector('#password')) {
    const inputPasswordRegister = document.querySelector('#password');
    
    inputPasswordRegister.addEventListener('input', () => {
        validarPassword();
    });
}

if (document.querySelector('#password_confirmation')) {
    const inputPasswordRegisterRep = document.querySelector('#password_confirmation');
    
    inputPasswordRegisterRep.addEventListener('input', () => {
        validarPassword();
    });
}
