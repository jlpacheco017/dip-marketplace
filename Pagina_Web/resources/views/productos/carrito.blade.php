@extends('layouts.master')

@section('title', 'DIP Marketplace - HOME')

@section('content')

    <div id="carrito">
        <div class="container_total">
            <h4 class="total">Carrito de la Compra: </h4>

            @if (Auth::check())
                @if ($productos != null)
                    <h4 class="price_carrito">{{ $total }}€</h4>
                @else
                    <h4 class="price_carrito">0€</h4>
                @endif
            @else
                <h4 class="price_carrito"></h4>
            @endif

            @if (Auth::check() && $productos != null)
                <p id="carrito-value" hidden>{{ count($productos) }}</p>
            @endif
        </div>
        @if (Auth::check())
            <a href="{{ route('comprar.carrito') }}" class="btn_remove">Realizar Compra</i></a>
        @else
            <a href="{{ route('auth.showLogin') }}" class="btn_remove">Realizar Compra</i></a>
        @endif


    </div>
    <div id="productos">
        @if (Auth::check())
            <div id="carrito_server" class="container_carrito">
                @if ($errors->has('error'))
                    <div class="error-message">
                        <i class='bx bx-error'></i> {{ $errors->first('error') }}
                    </div>
                @endif
                @foreach ($productos as $producto)

                @if (in_array($producto->id, $disponible))
                <div id="{{ $producto->id }}" class="item_container_carrito noDisponible">
                    <a href="/productos/show/{{ $producto->id }}">
                        <img class="carrito_img"src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                    </a>

                    <div>
                        <a href="/productos/show/{{ $producto->id }}"
                            class="producto_detalles--titulo" hidden>{{ $producto->titulo }}</a>
                    </div>
                    <div>
                        <p class="price_carrito" hidden>0€</p>
                    </div>
                    <div class="btn_container_carrito">
                        <button class="btn_remove" id="btn_eliminar_producto">Eliminar producto <i
                                class='bx bx-trash'></i></button>
                    </div>
                </div>

                @else
                <div id="{{ $producto->id }}" class="item_container_carrito">
                    <a href="/productos/show/{{ $producto->id }}">
                        <img class="carrito_img"src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                    </a>

                    <div>
                        <a href="/productos/show/{{ $producto->id }}"
                            class="producto_detalles--titulo">{{ $producto->titulo }}</a>
                    </div>
                    <div>
                        <p class="price_carrito">{{ $producto->precio }}€</p>
                    </div>
                    <div>
                        <button class="btn_remove" id="btn_eliminar_producto">Eliminar producto <i
                                class='bx bx-trash'></i></button>
                    </div>
                </div>
                @endif

                @endforeach
                <a href="{{ route('productos.index') }}" class="btn_remove" onclick="vaciarCarrito()">Vaciar
                    carrito</i></a>
            </div>
        @else
            <div id="carrito_local" class="container_carrito">
            </div>
        @endif
    </div>

@endsection
@section('js')
    @if (Auth::check())
        <script src="/js/carritoDB.js"></script>
    @else
        <script src="/js/carrito.js"></script>
    @endif

    <script src="/js/header.js"></script>
@endsection
