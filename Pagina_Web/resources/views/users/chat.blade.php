@extends('layouts.master')

@section('title', 'DIP Marketplace - Confirmar Pedidos')

@section('content')

@endsection

<div class="chat" onload="">
    <div class="chat_header">
        <div class="logo_name">
            @if ($soy == 'comprador')
                <img class="photo_chat" src="/{{ $shop->logo }}" alt="Foto del Vendedor">
                <p> {{ $shop->shop_name }}</p>
            @else
                <img class="photo_chat" src="{{ asset('img/photo/' . $comprador->photo) }}" alt="Foto del Vendedor">
                <p> {{ $comprador->username }}</p>
            @endif

        </div>
        <p class="chat_id">Id del Pedido= {{ $pedido_id }}</p>
        @if ($soy == 'comprador')
        <a href="{{ route('pedido.detallesPedidoUser', ['id' => $pedido_id]) }}"
            class="btn_remove btn_confirm chat_btn">Volver</a>
        @else
        <a href="{{ route('store.detallesPedidoVendedor', ['id' => $pedido_id]) }}"
            class="btn_remove btn_confirm chat_btn">Volver</a>
        @endif
       
    </div>
    <div id="chat_content" class="chat_content clearfix">
        @foreach ($chat as $mensaje)
            @if ($mensaje->enviado_por == Auth::user()->id)
                <div class="mensaje_derecha">
                    <p class="fecha">{{ $mensaje->created_at }}</p>
                    <p style="text-align: right">{{ $mensaje->mensaje }}</p>
                    @if ($mensaje->leido == false)
                        <p class="visto"><i class='bx bx-check' style='font-size: 2rem;'></i></p>
                    @else
                        <p class="visto"><i class='bx bx-check-double' style='color:#003dff; font-size: 2rem;'></i></p>
                    @endif

                </div>
                <br>
            @else
                <div class="mensaje_izquierda">
                    <p class="fecha">{{ $mensaje->created_at }}</p>
                    <p style="text-align: left">{{ $mensaje->mensaje }}</p>
                </div>
                <br>
            @endif
        @endforeach
    </div>
    <div class="chat_input_container">
        <form class="chat_input" action="{{ route('send.chat', [$chat[0]->id_chat]) }}" method="POST">
            @csrf
            <input type="text" id="mensaje" name="mensaje" placeholder="Escribe aqui tu mensaje" required><br>
            <button class="send" type="submit"><i class='bx bxs-send' undefined></i></button>
        </form>
    </div>

</div>

<script>
    window.onload = function() {
        window.scrollTo(0, document.body.scrollHeight);
    };
</script>

@section('js')
    <script src="/js/header.js"></script>
    <script src="/js/confirm.js"></script>
@endsection
