@extends('layouts.master')

@section('title', 'DIP Marketplace - MANAGE')

@section('content')
<div class="manage-container">
    <form action="{{ route('store.addProduct', $shop) }}" id="create-form" method="POST" enctype="multipart/form-data">
        @csrf
        <h4>AGREGAR UN PRODUCTO</h4>
        <div class="form-group">
            <label for="title">Título: </label>
            <input type="text" id="title" name="title" required placeholder="Apple iPhone 14 (128 GB) - Blanco Estrella">
            <div class="feedbackEditProduct">
                <p hidden id="patternTitleText" class="text-p-small errorpass"> ● No puede contener caracteres especiales.</p>
                <p hidden id="patternTitleTextMaxLength" class="text-p-small errorpass"> ● No puede contener más de 50 caracteres.</p>
            </div>
        </div>

        <div class="form-group">
            <label for="description">Descripción: </label>
            <input type="text" id="description" name="description" required placeholder="El Apple iPhone 14 (128 GB) - Blanco Estrella es la última incorporación a la línea de teléfonos...">
            <div class="feedbackEditProduct">
                <p hidden id="patternDescriptionTextMaxLength" class="text-p-small errorpass"> ● No puede contener más de 255 caracteres.</p>
            </div>
        </div>

        <div class="form-group">
            <label for="price">Precio: </label>
            <input type="text" id="price" name="price" required placeholder="879">
            <div class="feedbackEditProduct">
                <p hidden id="patternPriceType" class="text-p-small errorpass"> ● Debe de ser un número entero.</p>
                <p hidden id="patternPriceRange" class="text-p-small errorpass"> ● El Precio debe de ser positivo y no mayor a 50000.</p>
            </div>
        </div>

         @for ($i = 1; $i <= 5; $i++)
        <div class="form-group">
            <details>
                <summary><i class='bx bxs-edit'></i> Foto {{ $i }} <i class='bx bx-chevron-down'></i></summary>
                <input id="inputFile-{{ $i }}" type="file" name="image[]" accept="image/png, image/gif, image/jpeg, image/jpg">
                <label for="image-{{ $i }}">Max 2MB.</label>
            </details>
        </div>
        @endfor

        <div class="form-group">
            <details>
                <summary><i class='bx bxs-edit'></i> Categorías primarias <i class='bx bx-chevron-down'></i></summary>
                @foreach ($categorias_nivel_1 as $categoria)
                <div>
                    <label>
                        <input type="radio" name="categoriasNivel1[]" value="{{ $categoria->id }}">
                        {{ $categoria->titulo }}
                    </label>
                </div>
                @endforeach
            </details>
        </div>

        <div class="form-group">
            <details>
                <summary><i class='bx bxs-edit'></i> Categorías secundarias <i class='bx bx-chevron-down'></i></summary>
                @foreach ($categorias_nivel_2 as $categoria)
                <div>
                    <label>
                        <input type="checkbox" name="categoriasNivel2[]" value="{{ $categoria->id }}">
                        {{ $categoria->titulo }}
                    </label>
                </div>
                @endforeach
            </details>
        </div>

        @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif

        @if ($errors->has('title'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('title') }}
        </div>
        @endif

        @if ($errors->has('description'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('description') }}
        </div>
        @endif

        @if ($errors->has('price'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('price') }}
        </div>
        @endif

        <div class="form-group">
            <button type="submit">Agregar un producto</button>
        </div>
    </form>

    <form action="{{ route('store.customize', $shop->shop_name) }}" id="customize-form" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <h4>EDITAR MI TIENDA</h4>
        <div class="form-group">

            <div id="photo-container">
                <img id="photo" src="{{ asset($shop->logo) }}" alt="" class="photo">
            </div>
            <input id="inputFile" type="file" name="image" accept="image/png, image/gif, image/jpeg, image/jpg">
            <label for="image">Max 2MB.</label>
        </div>

        <div class="form-group">

            <label for="titleStore">Título de la tienda:</label>
            <input type="text" name="titleStore" id="titleStore" value="{{ $shop->title }}">
            <div class="feedback_editUser">
                <p hidden id="lengthTitle" class="text-p-small errorpass">● La longitud debe ser menor de 35 caracteres</p>
            </div>
        </div>

        @if(session('confirmation'))
        <div class="alert alert-success">
            {{ session('confirmation') }}
        </div>
        @endif

        @if ($errors->has('image'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('image') }}
        </div>
        @endif

        @if ($errors->has('titleStore'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('titleStore') }}
        </div>
        @endif
        <div class="form-group">
            <button type="submit">Actualizar tienda</button>
        </div>
    </form>
</div>
<a class="manage-shop-link" href="{{ route('store.show', $shop->shop_name) }}">
    <div class="manage-shop">
        <i class='bx bx-store-alt'></i>
        <p>Volver a mi tienda </p>
    </div>
</a>
@endsection

@section('js')
<script src="/js/header.js"></script>
<script src="/js/productCheck.js"></script>
<script src="/js/storeCheck.js"></script>
@endsection
