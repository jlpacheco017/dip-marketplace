const slider = document.querySelector('.slider');
const slides = slider.querySelector('.slides');
const slideCount = slides.children.length;
const arrowLeft = slider.querySelector('.arrow-left');
const arrowRight = slider.querySelector('.arrow-right');
let currentIndex = 0;

function moveToSlide(index) {
    slides.style.transform = `translateX(-${index * 100}%)`;
    currentIndex = index;
}

function moveToNextSlide() {
    moveToSlide((currentIndex + 1) % slideCount);
}

function moveToPreviousSlide() {
    moveToSlide((currentIndex - 1 + slideCount) % slideCount);
}

arrowLeft.addEventListener('click', moveToPreviousSlide);
arrowRight.addEventListener('click', moveToNextSlide);
