@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true"><i class='bx bx-chevrons-left' ></i></span>
                </li>
            @else
                <li>
                    <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')"><i class='bx bx-chevrons-left' ></i></a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @for ($i = 1; $i <= $paginator->lastPage(); $i++)
                @if ($i <= 3 || $i >= $paginator->lastPage() - 2 || ($i >= $paginator->currentPage() - 1 && $i <= $paginator->currentPage() + 1))
                    @if ($i == $paginator->currentPage())
                        <li class="active" aria-current="page"><span>{{ $i }}</span></li>
                    @else
                        <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
                    @endif
                @elseif (($i == 4 && $paginator->currentPage() > 4) || ($i == $paginator->lastPage() - 3 && $paginator->currentPage() < $paginator->lastPage() - 3))
                    <li class="disabled" aria-disabled="true"><span>...</span></li>
                @endif
            @endfor

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')"><i class='bx bx-chevrons-right'></i></a>
                </li>
            @else
                <li class="disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span aria-hidden="true"><i class='bx bx-chevrons-right'></i></span>
                </li>
            @endif
        </ul>
    </nav>
@endif
