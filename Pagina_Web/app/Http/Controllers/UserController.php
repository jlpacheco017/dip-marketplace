<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function editUser()
    {
        return view('users.edit');
    }
    public function doEditUser(Request $request)
    {
        $username = $request->input('username');
        $photo = $request->file('imagen');
        $current_password = $request->input('current_password');
        $password = $request->input('password');
        $password_confirmation = $request->input('password_confirmation');

        if ($photo) {
            $request->validate(
                [
                    'imagen' => 'required|image|mimes:png,jpg,jpeg|max:2048'
                ]
            );

            $user = Auth::user();
            $UserPhotoID = Auth::user()->id . ".jpg";
            Auth::user()->photo = $UserPhotoID;
            $user->save();

            $image = $photo;
            $image->move(public_path('img/photo'), $UserPhotoID);
        }

        if ($current_password) {
            $request->validate(
                [
                    'current_password' => [
                        'required'

                    ],
                    'password' => [
                        'required',
                        'min:8',
                        'regex:/^(?=.*[a-z])(?=.*[A-Z]).+$/'
                    ],
                    'password_confirmation' => 'required|same:password'
                ],
                [
                    'current_password.required' => 'La contraseña es obligatoria.',
                    'current_password.min' => 'La contraseña debe contener mínimo 8 caracteres.',
                    'current_password.regex' => 'La contraseña no cumple los requisitos mínimos.',

                    'password.required' => 'La contraseña es obligatoria.',
                    'password.min' => 'La contraseña no cumple los requisitos mínimos.',
                    'password.regex' => 'La contraseña no cumple los requisitos mínimos.',

                    'password_confirmation.required' => 'Debe repetir la contraseña.',
                    'password_confirmation.same' => 'Las contraseñas no coinciden.'
                ]
            );
            if (Hash::check($current_password, Auth::user()->password)) {
                $request->user()->fill([
                    'password' => Hash::make($password)
                ])->save();
            } else {
                return back()->withErrors(['password' => 'La contaseña que has introducido no coincide con la contraseña de tu cuenta']);
            }
        }

        $user = Auth::user();
        if ($username && $user->username != $username) {
            $request->validate(
                [
                    'username' => 'required|unique:users|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]+$/u',
                ]
            );
            $user->username = $request->input('username');
            $user->save();
        }

        return back()->with(['confirmation' => 'Se han actualizado tus datos correctamente.']);
    }
}
