<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function test_entrar_con_un_usuario_por_su_email()
    {
        // CREAR UN USUARIO.
        $usuario = User::factory()->create([
            'username' => 'Iván Reyes Navarro',
            'email' => 'ivanguayguay@gmail.com',
            'password' => Hash::make('Jzk9P8XZ1'),
        ]);

        // ENTRAMOS CON EL USUARIO CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'ivanguayguay@gmail.com',
            'password' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertRedirect(Route('landing.index'));

        // COMPROBAR QUE ESTAMOS AUTENTICADOS COMO EL USUARIO CREADO.
        $this->assertAuthenticatedAs($usuario);
    }

    public function test_entrar_con_un_usuario_por_su_nombre()
    {
        // CREAR UN USUARIO.
        $usuario = User::factory()->create([
            'username' => 'Iván Reyes Navarro',
            'email' => 'ivanguayguay@gmail.com',
            'password' => Hash::make('Jzk9P8XZ1'),
        ]);

        // ENTRAMOS CON EL USUARIO CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'Iván Reyes Navarro',
            'password' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertRedirect(Route('landing.index'));

        // COMPROBAR QUE ESTAMOS AUTENTICADOS COMO EL USUARIO CREADO.
        $this->assertAuthenticatedAs($usuario);
    }

    public function test_entrar_con_un_usuario_que_no_existe()
    {
        // ENTRAMOS CON UN USUARIO QUE NO HEMOS CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'ivanguayguay@gmail.com',
            'password' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertSessionHasErrors('login');
    }

    public function test_entrar_con_un_usuario_con_una_contraseña_incorrecta()
    {
        // CREAR UN USUARIO.
        User::factory()->create([
            'username' => 'Iván Reyes Navarro',
            'email' => 'ivanguayguay@gmail.com',
            'password' => Hash::make('Jzk9P8XZ1'),
        ]);

        // ENTRAMOS CON UN USUARIO QUE NO HEMOS CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'ivanguayguay@gmail.com',
            'password' => 'Barrio Sésamo',
        ]);

        // COMPROBAR QUE HA HECHO LA REDIRECIÓN A LA 'LANDING'.
        $response->assertSessionHasErrors('login');
    }
}
