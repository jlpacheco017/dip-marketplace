import Carrito from "./clases/Carrito.js";
import Producto from "./clases/Producto.js";

// Crear una instancia de la clase Carrito
const carrito = new Carrito();

const obtenerElProductoDeDetalles = () => {
  const producto = document.querySelector('.product-details');

  return producto;
};

obtenerElProductoDeDetalles().addEventListener('click', evento => {
  if (evento.target.nodeName === 'BUTTON') {
    const id = evento.target.parentElement.id;
    const img = evento.target.parentNode.previousElementSibling.children[0].children[0].children[0].getAttribute('src');
    const titulo = evento.target.parentElement.children[0].textContent;
    const precio = evento.target.parentElement.children[2].textContent;
    const producto = new Producto(id, img, titulo, precio);

    if (evento.target.classList.contains('btn_add')) {
      fetch("/agregar-producto", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ id: id })
      });
      let contador = document.getElementById("carrito_numero").innerText;
      contador = parseInt(contador) + 1;
      document.getElementById("carrito_numero").innerText = contador;
      eliminarBoton(evento);

    } else {
      fetch("/rm-producto", {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ id: id })
      });
      let contador = document.getElementById("carrito_numero").innerText;
      contador = parseInt(contador) - 1;
      document.getElementById("carrito_numero").innerText = contador;
      agregarBoton(evento);
    }
  }

});


const eliminarBoton = (evento) => {
  evento.target.classList.remove('btn_add');
  evento.target.classList.add('btn_remove');
  evento.target.innerHTML = "Eliminar de la <i class='bx bxs-shopping-bag'></i>";
};

const agregarBoton = (evento) => {
  evento.target.classList.remove('btn_remove');
  evento.target.classList.add('btn_add');
  evento.target.innerHTML = "Añadir a la <i class='bx bxs-shopping-bag'></i>";
};

const carritoValue = document.getElementById('carrito-value').innerHTML;
const updateHeaerCount = document.getElementById('carrito_numero');

updateHeaerCount.innerHTML = carritoValue;