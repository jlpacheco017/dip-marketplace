@extends('layouts.master')

@section('title', 'DIP Marketplace - Confirmar Pedidos')

@section('content')

@endsection



<div class="pedidos">





    <h1 class="h1Title">Pedidos por Confirmar</h1>
    <div class="titulos superior">
        <p><b>Nombre de usuario</b></p>
        <p><b>Fecha de compra</b></p>
        <p><b>Total del pedido</b></p>
        <p><b>Detalles</b></p>
        <p><b>Confirmar</b></p>
    </div>
    @if ($info)
        @foreach ($info as $item)
            <div id="{{ $item['id'] }}" class="titulos">
                <p>{{ $item['username'] }}</p>
                <p>{{ $item['fecha_de_compra'] }}</p>
                <p style="color: #4E23D6;">{{ $item['suma_total'] }}€</p>
                <a href="{{ route('store.detallesPedidoVendedor', $item['id']) }}"
                    class="btn_remove btn_confirm">Detalles</a>
                <p><button class="btn_remove btn_confirm" data-pedido-id="{{ $item['id'] }}">Confirmar</button></p>
            </div>
        @endforeach
    @else
        <h5 class="noConfirmadosMensaje">No hay pedidos por Confirmar</h5>
    @endif










    <h1 class="Pedidos_Confirmados_Title h1Title">Pedidos Confirmados no Enviados</h1>
    <div " class="titulos superior">
            <p><b>Nombre de usuario</b></p>
            <p><b>Fecha de compra</b></p>
            <p><b>Total del pedido</b></p>
            <p><b>Detalles</b></p>
            <p><b>Confirmar Envio</b></p>
        </div>
          @if ($info_confirmada)
        @foreach ($info_confirmada as $item)
            <div id="{{ $item['id'] }}" class="titulos">
                <p>{{ $item['username'] }}</p>
                <p>{{ $item['fecha_de_compra'] }}</p>
                <p style="color: #4E23D6;">{{ $item['suma_total'] }}€</p>
                <a href="{{ route('store.detallesPedidoVendedor', $item['id']) }}"
                    class="btn_remove btn_confirm">Detalles</a>
                <a href="{{ route('store.confirmarEnvioPedido', $item['id']) }}"
                    class="btn_remove btn_confirm">Confirmar Envio</a>
            </div>
        @endforeach
    @else
        <h5 class="noConfirmadosMensaje">No hay pedidos confirmados</h5>

        @endif










        <h1 class="Pedidos_Confirmados_Title h1Title">Pedidos por llegar</h1>
        <div " class="titulos superior">
            <p><b>Nombre de usuario</b></p>
            <p><b>Fecha de compra</b></p>
            <p><b>Total del pedido</b></p>
            <p><b>Detalles</b></p>
            <p><b>Confirmar Llegada</b></p>
        </div>
          @if ($informacion_Enviado)
            @foreach ($informacion_Enviado as $item)
                <div id="{{ $item['id'] }}" class="titulos">
                    <p>{{ $item['username'] }}</p>
                    <p>{{ $item['fecha_de_compra'] }}</p>
                    <p style="color: #4E23D6;">{{ $item['suma_total'] }}€</p>
                    <a href="{{ route('store.detallesPedidoVendedor', $item['id']) }}"
                        class="btn_remove btn_confirm">Detalles</a>
                    <a href="{{ route('store.confirmarLlegadaPedido', $item['id']) }}"
                        class="btn_remove btn_confirm">Confirmar Llegada</a>
                </div>
            @endforeach
        @else
            <h5 class="noConfirmadosMensaje">No hay pedidos por llegar</h5>

            @endif






            <h1 class="Pedidos_Confirmados_Title h1Title">Pedidos Finalizados</h1>
            <div " class="titulos superior">
                <p><b>Nombre de usuario</b></p>
                <p><b>Fecha de compra</b></p>
                <p><b>Total del pedido</b></p>
                <p><b>Detalles</b></p>
                <p><b>Confirmado Llegada Comprador?</b></p>
            </div>
    
            @if ($informacion_Recibido)
            @foreach ($informacion_Recibido as $item)
                    <div id="{{ $item['id'] }}" class="titulos">
                        <p>{{ $item['username'] }}</p>
                        <p>{{ $item['fecha_de_compra'] }}</p>
                        <p style="color: #4E23D6;">{{ $item['suma_total'] }}€</p>
                        <a href="{{ route('store.detallesPedidoVendedor', $item['id']) }}"
                            class="btn_remove btn_confirm">Detalles</a>
                        @if ( $item['recibido'] == true)
                            <p><i class='bx bxs-check-circle' style="color:green !important; font-size: 3rem"></i></p>
                        @else
                        <p ><i class='bx bxs-x-circle' style="color:rgb(154, 1, 1) !important; font-size: 3rem"></i></p>
                        @endif
                    </div>
                @endforeach
            @else
                <h5 class="noConfirmadosMensaje">No hay pedidos finalizados</h5>

                @endif

            </div>

            @section('js')
                <script src="/js/header.js"></script>
                <script src="/js/confirm.js"></script>
            @endsection
