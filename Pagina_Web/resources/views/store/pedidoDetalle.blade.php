@extends('layouts.master')

@section('title', 'DIP Marketplace - Confirmar Pedidos')

@section('content')
    <div class="pedidoDetalles">
        <h1 class="h1PedidoDetail">Información de la Compra</h1>
        <div class="pedidoDetail">
            <p class="TableTitle">Identificador:</p>
            <p>{{ $pedido->id }}</p>

            <p class="TableTitle">Nombre de usuario: </p>
            <p>{{ $user->username }}</p>

            <p class="TableTitle">Productos comprados: </p>
            <p>{{ count($productos) }}</p>


            <p class="TableTitle">Dinero Gastado: </p>
            <p>{{ $suma }}€</p>


            <p class="TableTitle">Pago realizado? </p>
            <p style="color:rgb(1, 154, 1)"> Si</p>

            <p class="TableTitle">Pago Confirmado: </p>
            @if ($confirmado == true)
                <p style="color:rgb(1, 154, 1)">Si</p>
            @else
                <p style="color:rgb(154, 1, 1)">No</p>
            @endif

            <p class="TableTitle">Envio Confirmado: </p>
            @if ($enviado == true)
                <p style="color:rgb(1, 154, 1)">Si</p>
            @else
                <p style="color:rgb(154, 1, 1)">No</p>
            @endif

        </div>
        <div>
            @if ($mensajesNoLeidos == 0)
                <p class="chat_text">No hay mensajes nuevos</p>
            @else
                <p class="chat_text" style="color:rgb(154, 1, 1)">Hay mensajes sin leer: {{ $mensajesNoLeidos }} <i
                        class='bx bxs-circle'></i></p>
            @endif
            <a href="{{ route('user.chat', $chat->id) }}" class="btn_remove btn_confirm"><i
                    class='bx bxs-chat'></i> Chat</a>

        </div>
        @if ($confirmado != true)
            <button class="btn_remove btn_confirm" data-pedido-id="{{ $pedido->id }}">Confirmar Pedido</button>
        @endif
        @if ($confirmado == true && $enviado != true)
            <a href="{{ route('store.confirmarEnvioPedido', $pedido->id) }}" class="btn_remove btn_confirm">Confirmar
                Envio</a>
        @endif

        <h1 class="productoTitle">Productos</h1>
        <div class="productosDetails">
            <div id="1" class="titulos superior">
                <p>Titulo</p>
                <p>Descripcion</p>
                <p>Precio</p>
            </div>
            @foreach ($productos as $item)
                <div id="{{ $item->id }}" class="titulos">
                    <p>{{ $item->titulo }}</p>
                    <p>{{ $item->descripcion }}</p>
                    <p style="color: #4E23D6;">{{ $item->precio }}€</p>
                </div>
            @endforeach
        </div>
    </div>
@endsection



@section('js')
    <script src="/js/header.js"></script>
    <script src="/js/confirm.js"></script>
@endsection
