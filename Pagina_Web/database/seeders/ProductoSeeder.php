<?php

namespace Database\Seeders;

use App\Models\Producto;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProductoSeeder extends Seeder
{
    /**
     
Run the database seeds.*/
  public function run(): void{
      $productos = Producto::factory()->count(env('NUMERO_DE_PRODUCTOS_PARA_CREAR'))->create();


        foreach ($productos as $producto) {
            Http::get('http://172.16.50.60/api/random/'.$producto->id);
            $producto->img = env('API_IMAGE'). $producto->id . '/1';
            $producto->save();
        }
    }
}