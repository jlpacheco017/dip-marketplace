@extends('layouts.master')

@section('title', 'DIP Marketplace - Confirmar Pedidos')

@section('content')
    <div class="pedidoDetalles">
        <h1 class="h1PedidoDetail">Información de la Compra</h1>
        <div class="pedidoDetail">
            <p class="TableTitle">Identificador:</p>
            <p>{{ $pedido->id }}</p>

            <p class="TableTitle">Nombre de usuario: </p>
            <p>{{ $user->username }}</p>

            <p class="TableTitle">Productos comprados: </p>
            <p>{{ count($productos) }}</p>


            <p class="TableTitle">Dinero Gastado: </p>
            <p>{{ $suma }}€</p>


            <p class="TableTitle">Pago realizado? </p>
            <p style="color:rgb(1, 154, 1)"> Si</p>

            <p class="TableTitle">Pedido Recibido? </p>
            @if ($pedido->recibido == false)
                <p style="color:rgb(255, 0, 0)"> No</p>
            @else
                <p style="color:rgb(1, 154, 1)"> Si</p>
            @endif


        </div>
        @if ($productos_confirmados == count($productos) && $pedido->recibido == false)
            <a href="{{ route('pedido.detallesPedidoRecibidoUser', $pedido->id) }}" class="btn_remove btn_confirm">Confirmar
                Recepcion</a>
        @else
            @if ($pedido->recibido == false)
                <p class="confirm_msg">Todos los vendedores tienen que aceptar el pedido para confirmar que lo has recibido
                </p>
            @endif
        @endif

        @for ($i = 0; $i < count($vendedores); $i++)
            <p class="chat_text">Chat con el vendedor = {{$vendedores[$i]->shop_name}}</p>
            @if ($mensajesNoLeidos[$i] == 0)
                <p class="chat_text">No hay mensajes nuevos</p>
            @else
                <p class="chat_text" style="color:rgb(154, 1, 1)">Hay mensajes sin leer: {{ $mensajesNoLeidos[$i] }} <i
                        class='bx bxs-circle'></i></p>
            @endif
            <a href="{{ route('user.chat', $chat[$i]->id) }}" class="btn_remove btn_confirm"><i class='bx bxs-chat'></i>
                Chat</a>
        @endfor

        <h1 class="productoTitle">Productos</h1>
        <div class="productosDetails">
            <div id="1" class="titulos superior">
                <p>Titulo</p>
                <p>Descripcion</p>
                <p>Precio</p>
                <p>Vendedor</p>
                <p>Pago Confirmado</p>
                <p>Envio Confirmado</p>
                <p>Llegada Confirmada por Vendedor</p>
            </div>
            @foreach ($productos as $item)
                <div id="{{ $item->id }}" class="titulos">
                    <p>{{ $item->titulo }}</p>
                    <p>{{ $item->descripcion }}</p>
                    <p style="color: #4E23D6;">{{ $item->precio }}€</p>
                    <p>{{ $item->shop_name }}</p>
                    @if ($item->producto_pagado == true)
                        <p style="color:rgb(1, 154, 1)">Si</p>
                    @else
                        <p style="color:rgb(255, 0, 0)">No</p>
                    @endif
                    @if ($item->producto_enviado == true)
                        <p style="color:rgb(1, 154, 1)">Si</p>
                    @else
                        <p style="color:rgb(255, 0, 0)">No</p>
                    @endif
                    @if ($item->recibido_vendedor == true)
                        <p style="color:rgb(1, 154, 1)">Si</p>
                    @else
                        <p style="color:rgb(255, 0, 0)">No</p>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endsection



@section('js')
    <script src="/js/header.js"></script>
    <script src="/js/confirm.js"></script>
@endsection
