<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Chat;
use App\Models\Pedido;
use App\Models\Carrito;
use App\Models\Producto;
use App\Models\ChatUsuario;
use Illuminate\Http\Request;
use App\Models\LineasCarrito;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class CartController extends Controller
{
    /**
     * Muestra el carrito si el usuario esta logueado y comprueba si hay productos no disponibles.
     *  
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS.
     * 
     * @return View Devuelve la vista del carrito (carrito.blade.php).
     */
    public function showCarrito(Request $request)
    {
        if (Auth::check()) {
            $objCarrito = new Carrito();
            $carrito_select = $objCarrito->getCarritoActivoPorUsuario(Auth::user()->id);
            $productosNoDisponibles = Producto::where('disponible', false)->get();
            $productos = DB::table('lineas_carritos')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->where('lineas_carritos.carrito_id', '=', $carrito_select->id)
                ->select('productos.*')
                ->get();

            $productosNoDisponiblesCoinciden = $productosNoDisponibles->pluck('id')->intersect($productos->pluck('id'))->toArray();
            $total = 0;
            foreach ($productos as $key => $producto) {
                if ($producto->disponible == true) {
                    $total = $producto->precio + $total;
                } else {
                }
            }
            Log::debug($request->ip() . ' El usuario= ' . Auth::user()->id . ' Ha entrado en el carrito -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            return view('productos.carrito', ['productos' => Producto::all(), 'productos' => $productos, 'total' => $total, 'disponible' => $productosNoDisponiblesCoinciden]);
        }
        Log::debug($request->ip() . ' Mostrar carrito sin estar autenticado -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
        return view('productos.carrito', ['productos' => Producto::all()]);
    }

    /**
     * Si el usuario esta logueado, comprueba si el producto que se quiere agregar ya existe entre los productos del usuario.
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS y para recibir el id del producto a agregar.
     */

    public function agregar_producto(Request $request)
    {
        if (Auth::check()) {
            $carrito_select = Carrito::where('usuario_id', Auth::user()->id)->where('pagado', false)->first();
            $producto_existente = LineasCarrito::where('carrito_id', $carrito_select->id)->where('producto_id', $request->id);
            if (isset($producto_existente)) {
                $producto = Producto::findOrFail($request->id);
                $carrito_add = new LineasCarrito();
                $carrito_add->carrito_id = $carrito_select->id;
                $carrito_add->producto_id = $producto->id;
                $carrito_add->precio = $producto->precio;
                $carrito_add->save();
            }
            if (Auth::check()) {
                Log::debug($request->ip() . ' El usuario con id= ' . Auth::user()->id . ' Ha añadido del carrito el id= ' . $request->input('id') . ' estando autenticado -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            } else {
                Log::debug($request->ip() . ' Ha añadido del carrito el id= ' . $request->input('id') . ' -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            }
        }
    }
    /**
     * Si el usuario esta logueado, y el producto existe en el carrito del usuario lo elimina.
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS y para recibir 
     * el id del producto a eliminar.
     */
    public function rm_producto(Request $request)
    {
        if (Auth::check()) {
            $carrito_select = Carrito::where('usuario_id', Auth::user()->id)->where('pagado', false)->first();
            $producto_select = LineasCarrito::where('carrito_id', $carrito_select->id)->where('producto_id', $request->id)->first();
            if ($producto_select) {
                $producto_select->delete();
            }
            if (Auth::check()) {
                Log::debug($request->ip() . ' El usuario con id= ' . Auth::user()->id . ' Ha eliminado del carrito el id= ' . $request->input('id') . ' estando autenticado -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            } else {
                Log::debug($request->ip() . ' Ha eliminado del carrito el id= ' . $request->input('id') . ' -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            }
        }
    }

    /**
     * Si el usuario esta logueado, elimina todos los productos de su carrito.
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS y para recibir el id del producto a eliminar.
     */
    public function vaciar_carrito(Request $request)
    {
        if (Auth::check()) {
            $carrito_select = Carrito::where('usuario_id', Auth::user()->id)->where('pagado', false)->first();
            $producto_select = LineasCarrito::where('carrito_id', $carrito_select->id)->get();

            foreach ($producto_select as $key => $value) {
                $value->delete();
            }
            if (Auth::check()) {
                Log::debug($request->ip() . ' El usuario con id= ' . Auth::user()->id . ' Ha vaciado el carrito estando autenticado -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            } else {
                Log::debug($request->ip() . ' Ha vaciado el carrito -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            }
        }
    }

    /**
     * Si el usuario esta logueado, y no tiene productos que no esten disponibles realiza un join en lineas_carrito
     *  y inserta un pedido en la base de datos.
     *
     * @return View Devuelve la vista del index (index.blade.php).
     */
    public function comprarCarrito()
    {
        if (Auth::check()) {
            $carrito_select = Carrito::where('usuario_id', Auth::user()->id)->where('pagado', false)->first();
            $producto_select = LineasCarrito::where('carrito_id', $carrito_select->id)->get();
            $productos_noDisponible = LineasCarrito::join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->where('lineas_carritos.carrito_id', $carrito_select->id)
                ->where('productos.disponible', false)
                ->get();
            $productos_Disponibles = LineasCarrito::join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->where('lineas_carritos.carrito_id', $carrito_select->id)
                ->where('productos.disponible', true)
                ->get();

            if (!$productos_noDisponible->isEmpty()) {
                return back()->withErrors(['error' => 'Hay productos no disponibles en el carrito, por favor, eliminelos']);
            }
            if ($producto_select->isEmpty()) {
                return back()->withErrors(['error' => 'No hay productos en el carrito de la compra']);
            }
            $carrito_select->pagado = true;
            $carrito_select->fecha_compra = Carbon::now()->format('Y-m-d H:i:s');
            $carrito_select->save();
            Carrito::create([
                'usuario_id' => Auth::User()->id,
            ]);
            foreach ($productos_Disponibles as $producto) {
                $producto_select = Producto::find($producto->id);
                $producto_select->disponible = false;
                $producto_select->save();
            }
            $pedido = new Pedido();
            $pedido->usuario_id = Auth::user()->id;
            $pedido->carrito_id = $carrito_select->id;
            $pedido->fecha_compra = Carbon::now()->format('Y-m-d H:i:s');
            $pedido->save();


            $vendedores = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('stores.user_id')
                ->where('pedidos.id', '=', $pedido->id)
                ->distinct()
                ->get();

            foreach ($vendedores as $vendedor) {

                    $chatUsuario = new ChatUsuario;
                    $chatUsuario->id_vendedor = $vendedor->user_id;
                    $chatUsuario->id_comprador = Auth::user()->id;
                    $chatUsuario->id_pedido = $pedido->id;
                    $chatUsuario->save();
             

                $chat = new Chat;
                $chat->id_chat = $chatUsuario->id;
                $chat->enviado_por = Auth::user()->id;;
                $chat->recibido_por = $vendedor->user_id;
                $chat->mensaje = "He realizado un pedido nuevo, porfavor, revise que ha recibido el pago correctamente";
                $chat->leido = false;
                $chat->save();
                $chatUsuario->touch();
            }
            return redirect(Route('productos.index'));
        }
    }
}
