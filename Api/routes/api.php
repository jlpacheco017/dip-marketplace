<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('image/{productId}', [ImageController::class, 'getProductImages']);
Route::delete('image/{productId}', [ImageController::class, 'deleteProductImages']);
Route::get('image/{productId}/{position}', [ImageController::class, 'getProductImageUnic']);
Route::delete('image/{productId}/{position}', [ImageController::class, 'deleteProductImageUnic']);
Route::get('default', [ImageController::class, 'getProductImageDefault']);
Route::get('random/{productId}', [ImageController::class, 'getRandomImages']);

Route::post('image', [ImageController::class, 'uploadProductImage']);
