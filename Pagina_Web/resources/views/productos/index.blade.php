@extends('layouts.master')

@section('title', 'DIP Marketplace - HOME')

@section('content')

<form action="{{ route('productos.index') }}" method="GET">

    {{-- Listado categorias --}}
    <div id="categorias">
        @if (Auth::check() && $carrito != null)
        <p id="carrito-value" hidden>{{ count($idsCarrito) }}</p>
        @endif


        <button hidden type="submit" value="{{ old('categoria') }}">
        </button>
        <button class="categoria" type="submit" value="0">
            Todas las categorías
        </button>
        @foreach ($categorias as $categoria)
        <button class="categoria" type="submit" value="{{ $categoria->id }}">
            {{ $categoria->titulo }}
        </button>
        @endforeach
    </div>
    <input id="inputHidden" type="hidden" name="categoria" value="{{ old('categoria') }}">

    {{-- Barra de busqueda --}}
    <div id="barraDeBusqueda">
        <input type="search" placeholder="Buscar productos..." name="search" value="{{ old('search') }}">
        <button id="mySubmit" type="submit"><i class='bx bx-search'></i></button>
    </div>
    {{-- Ordenación --}}
    <div id="ordenacion">
        <select id="ordenacion" name="ordenacion" onchange="this.form.submit()">
            <option value="NombreAZ" {{ old('ordenacion') == 'NombreAZ' ? 'selected' : '' }}>Nombre: de A a Z</option>
            <option value="NombreZA" {{ old('ordenacion') == 'NombreZA' ? 'selected' : '' }}>Nombre: de Z a A</option>
            <option value="PrecioMenor" {{ old('ordenacion') == 'PrecioMenor' ? 'selected' : '' }}>Precio: de menor a
                mayor</option>
            <option value="PrecioMayor" {{ old('ordenacion') == 'PrecioMayor' ? 'selected' : '' }}>Precio: de mayor a
                menor</option>
        </select>
    </div>
</form>

<section class="productos">
    <div class="productos__center">
        @forelse ($productos as $producto)
        <div id="{{ $producto->id }}" class="producto">
            <a href="{{ route('productos.show', $producto->id) }}">
                <div class="imagen__container">
                    <img src="{{env('API_IMAGE').$producto->id}}/1" alt="">
                </div>
                <div class="producto__footer">
                    <h4>{{ $producto->titulo }}</h4>
                    <div class="precio">{{ $producto->precio }}€</div>
                </div>
            </a>
            <div class="bottom">
                <div class="btn__group">
                    @if (in_array($producto->id, $productosStore))
                    <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                    @else
                    @if (Auth::check() && $carrito != null)
                    @if (in_array($producto->id, $idsCarrito))
                    <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                    @else
                    <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                    @endif
                    @else
                    <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                    @endif
                    @endif
                </div>
            </div>
        </div>
        @empty
        <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente para reponer nuestro inventario lo antes posible.</p>
        @endforelse
    </div>
</section>
{{ $productos->links() }}
@endsection

@section('js')
<script script src="/js/categorias.js"></script>
<script src="/js/header.js"></script>
@if (Auth::check())
<script src="/js/carritoIndexDB.js" type="module"></script>
@else
<script src="/js/carritoIndex.js" type="module"></script>
@endif
@endsection
