<?php

namespace Tests\Feature;

use App\Models\Categoria;
use Tests\TestCase;
use App\Models\Producto;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Seeders\UserSeeder;
use Database\Seeders\StoreSeeder;

class ProductosIndexTest extends TestCase
{

    use RefreshDatabase;

    public function test_no_hay_productos()
    {
        $response = $this->get(route('productos.index'));
        $response->assertSee('Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente para reponer nuestro inventario lo antes posible.');
    }

    public function test_crear_un_producto_y_que_se_muestre()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $producto = Producto::factory()->create();

        $response = $this->get(route('productos.index'));
        $response->assertSee($producto->titulo);
    }

    public function test_crear_un_producto_con_todo_tipo_de_caracteres()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $producto = Producto::factory()->create(
            [
                'titulo' => 'Prøductø de prüëba', 'descripcion' => 'Dëscrïpcioñ dël prøductø dë prüëbä',
            ]

        );
        $response = $this->get(route('productos.index'));
        $response->assertSee($producto->titulo);
        $this->assertDatabaseHas('productos', ['titulo' => 'Prøductø de prüëba', 'descripcion' => 'Dëscrïpcioñ dël prøductø dë prüëbä']);
    }

    public function test_crear_un_producto_con_tildes()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $producto = Producto::factory()->create(

            [
                'titulo' => 'Prodùctó cón tíldès',
                'descripcion' => 'Déscrípción dél pròductó con tíldès',
            ]

        );
        $response = $this->get(route('productos.index'));
        $response->assertSee($producto->titulo);
        $this->assertDatabaseHas('productos', ['titulo' => 'Prodùctó cón tíldès', 'descripcion' => 'Déscrípción dél pròductó con tíldès']);
    }

    public function test_crear_un_producto_sin_descripcion()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $producto = Producto::factory()->create(

            [
                'titulo' => 'Producto con descripción vacía',
                'descripcion' => '',
            ]

        );
        $response = $this->get(route('productos.index'));
        $response->assertSee($producto->titulo);
        $this->assertDatabaseHas('productos', ['titulo' => 'Producto con descripción vacía', 'descripcion' => '']);
    }

    public function test_crear_un_producto_sin_titulo()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $producto = Producto::factory()->create(

            [
                'titulo' => '',
                'descripcion' => 'Descripción del producto',
            ]

        );
        $response = $this->get(route('productos.index'));
        $response->assertSee($producto->titulo);
        $this->assertDatabaseHas('productos', ['titulo' => '', 'descripcion' => 'Descripción del producto']);
    }

    public function test_buscar_un_producto_por_una_categoria()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        // CREAMOS LA CATEGORÍA
        Categoria::create(['titulo' => 'Categoria de prueba', 'icono' => 'bx bx-laptop', 'nivel' => '1', 'id_padre' => '0']);
        
        // CREAMOS UN PRODUCTO
        $producto = Producto::factory()->create(
            [
                'titulo' => 'Título',
                'descripcion' => 'Descripción del producto',
            ]
        );

        // HACEMOS EL 'ATTACH' DEL PRODUCTO CON LA CATEGORÍA
        $producto->find(1)->categorias()->attach(1);

        // REALIZAMOS LA BUSQUEDA CON EL QUERYSTRING DE LA CATEGORÍA '1'
        $response = $this->get(route('productos.index', [
            'categoria' => '1',
            'search' => '',
            'ordenacion' => ''
        ]));

        // COMPROBAMOS QUE SALE EN LA PÁGINA WEB
        $response->assertSee($producto->titulo);
    }

    public function test_buscar_un_producto_por_busqueda()
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        // CREAMOS UN PRODUCTO
        $producto = Producto::factory()->create(
            [
                'titulo' => 'Título',
                'descripcion' => 'Descripción del producto',
            ]
        );

        // REALIZAMOS LA BUSQUEDA CON EL QUERYSTRING DE BUSQUEDA 'Título'
        $response = $this->get(route('productos.index', [
            'categoria' => '',
            'search' => 'Título',
            'ordenacion' => ''
        ]));    

        // COMPROBAMOS QUE SALE EN LA PÁGINA WEB
        $response->assertSee($producto->titulo);
    }
}
