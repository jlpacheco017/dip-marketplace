<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageController extends Controller
{
    const NUMERO_MAXIMO_FOTOS = 5;

    public function uploadProductImage(Request $request)
    {
        // Realiza el log del cuerpo de la solicitud
        Log::debug($request);

        // checkeamos si la petición es correcta con los tokens de user y server.
        if (!$this->checkRequest($request)) {
            return response()->json(['error' => 'Petición no permitida'], 400);
        }

        // Validamos si el ID y la posición de la foto del producto corresponde con nuestras reglas.
        $idProduct = $request->idProduct;
        $imageProductPosition = $request->imageProductPosition;
        if (!ctype_digit($idProduct) || !ctype_digit($imageProductPosition)) {
            return response()->json(['error' => 'Unos de los valores introducidos es incorrecto. Debe ser un entero.'], 400);
        }

        // Verifica si la posición de la imagen del producto está en el rango de 1 a 7
        if ($imageProductPosition < 1 || $imageProductPosition > SELF::NUMERO_MAXIMO_FOTOS) {
            return response()->json(['error' => 'La posición de la imagen del producto debe estar entre 1 y ' . SELF::NUMERO_MAXIMO_FOTOS . '.'], 400);
        }

        $uploadedFile = $request->file('image');

        try {
            if (!$uploadedFile instanceof UploadedFile) {
                throw new Exception('No se ha enviado ningún archivo');
            }

            // Valida el tamaño de la imagen
            if ($uploadedFile->getSize() > 2000000) { // 2 MB, está en bytes
                throw new Exception('El archivo es demasiado grande');
            }

            // Crea una instancia de la imagen con Intervention Image
            $image = Image::make($uploadedFile);

            // Establece los valores de ancho y alto máximo deseados
            $maxWidth = 500;  // Reemplaza con el ancho máximo deseado
            $maxHeight = 500; // Reemplaza con la altura máxima deseada

            // Redimensiona la imagen al tamaño exacto deseado, sin mantener el aspecto de ancho y alto proporcionado
            $image->resize($maxWidth, $maxHeight);

            $imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];
            $basePath = 'public/images/' . $idProduct . '-' . $imageProductPosition;

            foreach ($imageExtensions as $ext) {
                $existingImagePath = $basePath . '.' . $ext;
                if (Storage::exists($existingImagePath)) {
                    Storage::delete($existingImagePath);
                }
            }

            // Guarda la imagen redimensionada en el directorio deseado
            $imagePath = $basePath . '.' . $uploadedFile->getClientOriginalExtension();
            $image->save(storage_path('app/' . $imagePath));
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response(['path' => $imagePath], Response::HTTP_CREATED);
    }

    public function getProductImages($productId)
    {
        if (!ctype_digit($productId)) {
            return response()->json(['error' => 'Los valores introducidos son incorrectos. Deben ser enteros.'], 400);
        }
        try {
            $files = Storage::files('public/images');
            $images = collect($files)
                ->filter(function ($filename) use ($productId) {
                    return preg_match('/^' . $productId . '-\d+\.(jpg|png)$/', basename($filename));
                })
                ->map(function ($filename) {
                    $baseFilename = pathinfo($filename, PATHINFO_FILENAME);
                    [$productId, $position] = explode('-', $baseFilename);
                    return url("/api/image/{$productId}/{$position}");
                });

            if ($images->isEmpty()) {
                return $this->getProductImageDefault();
            }

            return response()->json([
                'status' => 'success',
                'images' => $images
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => 'error',
                'message' => 'An error occurred while processing the request: ' . $exception->getMessage()
            ], 500);
        }
    }

    public function getProductImageUnic(Request $request, $productId, $position)
    {

        // CACHE NO ACABADA.
        // $ifNoneMatch = $request->header('If-None-Match');
        // $ifModifiedSince = $request->header('If-Modified-Since');
        // $method = $request->method();
        // $url = $request->url();
        // $fullUrl = $request->fullUrl();
        // $path = $request->path();
        // $input = $request->all();
        // $headers = $request->headers->all();

        // Log::info('Request Details:', [
        //     'method' => $method,
        //     'url' => $url,
        //     'fullUrl' => $fullUrl,
        //     'path' => $path,
        //     'input' => $input,
        //     'headers' => $headers
        // ]);

        // Validar productId y position
        if (!ctype_digit($productId) || !ctype_digit($position)) {
            return response()->json(['error' => 'Los valores introducidos son incorrectos. Deben ser enteros.'], 400);
        }

        // Verificar si la posición está en el rango permitido
        if ($position < 1 || $position > 5) {
            return response()->json(['error' => 'La posición debe estar entre 1 y 5.'], 400);
        }

        try {
            // Obtener todos los archivos en el directorio de imágenes
            $files = Storage::files('public/images');

            // Filtrar los archivos que coincidan con el productId y position
            $productImage = collect($files)->first(function ($filename) use ($productId, $position) {
                return preg_match('/^' . $productId . '-' . $position . '\.(jpg|png)$/', basename($filename));
            });

            // Verificar si se encontró una imagen
            if (!$productImage) {
                return $this->getProductImageDefault();
            }

            $etag = md5(Storage::get($productImage));

            $lastModified = Storage::lastModified($productImage);
            // Log::debug($lastModified);
            // Devolver la imagen solicitada
            $headers = [
                'Content-Type' => Storage::mimeType($productImage),
                'Content-Disposition' => 'inline; filename="' . basename($productImage) . '"',
                // Cache en los headers no acabado.
                // 'Cache-Control' => 'public, max-age=86400', // cache por 24 horas
                // 'ETag' => $etag,
                // 'Last-Modified' => gmdate('D, d M Y H:i:s', $lastModified) . ' GMT',
            ];

            return response(Storage::get($productImage), 200, $headers);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getProductImageDefault()
    {
        try {
            // Obtener todos los archivos en el directorio de imágenes
            $files = Storage::files('public/images');

            // Buscar la imagen con el nombre "default.png"
            $defaultImage = collect($files)->first(function ($filename) {
                return basename($filename) === 'default.png';
            });

            // Verificar si se encontró una imagen
            if (!$defaultImage) {
                return response()->json(['error' => 'Imagen no encontrada.'], 404);
            }

            // Devolver la imagen solicitada
            $headers = [
                'Content-Type' => Storage::mimeType($defaultImage),
                'Content-Disposition' => 'inline; filename="' . basename($defaultImage) . '"'
            ];

            return response(Storage::get($defaultImage), 200, $headers);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteProductImageUnic($productId, $position)
    {
        // Validar productId y position
        if (!ctype_digit($productId) || !ctype_digit($position)) {
            return response()->json(['error' => 'Los valores introducidos son incorrectos. Deben ser enteros.'], 400);
        }

        // Verificar si la posición está en el rango permitido
        if ($position < 1 || $position > 5) {
            return response()->json(['error' => 'La posición debe estar entre 1 y 5.'], 400);
        }

        try {
            // Obtener todos los archivos en el directorio de imágenes
            $files = Storage::files('public/images');

            // Filtrar los archivos que coincidan con el productId y position
            $productImage = collect($files)->first(function ($filename) use ($productId, $position) {
                return preg_match('/^' . $productId . '-' . $position . '\.(jpg|png)$/', basename($filename));
            });

            // Verificar si se encontró una imagen
            if (!$productImage) {
                return response()->json(['error' => 'Imagen no encontrada.'], 404);
            }

            // Eliminar la imagen
            Storage::delete($productImage);

            // Devolver una respuesta exitosa
            return response()->json(['success' => 'Imagen eliminada con éxito.'], 200);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteProductImages(Request $request)
    {
        // Validar productId
        $productId = intval($request->idProduct);

        if ($productId === 0) {
            return response()->json(['error' => 'Valor de producto incorrecto'], 400);
        }

        // checkeamos si la petición es correcta con los tokens de user y server.
        if (!$this->checkRequest($request)) {
            return response()->json(['error' => 'Petición no permitida'], 400);
        }

        try {
            // Obtener todos los archivos en el directorio de imágenes que coincidan con el productId
            $files = Storage::files('public/images');
            $productImages = collect($files)->filter(function ($filename) use ($productId) {
                return preg_match('/^' . $productId . '-\d+\.(jpg|png)$/', basename($filename));
            });

            // Eliminar todas las imágenes encontradas
            $deleted = 0;
            foreach ($productImages as $productImage) {
                if (Storage::delete($productImage)) {
                    $deleted++;
                }
            }

            // Devolver una respuesta con el número de imágenes eliminadas
            return response()->json([
                'status' => 'success',
                'message' => 'Se han eliminado ' . $deleted . ' imágenes del producto ' . $productId . '.'
            ]);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getRandomImages($productId)
    {
        // Comprobar que $productId es un número
        if (!ctype_digit((string) $productId)) {
            return response()->json(['message' => 'El ID del producto proporcionado no es un número entero válido']);
        }

        // Obtener la lista de todas las imágenes
        $images = Storage::disk('public')->files('productosImg');

        if (!empty($images)) {
            // Seleccionar una imagen aleatoria
            $randomImage = $images[array_rand($images)];

            // Extraer la extensión de la imagen
            $extension = pathinfo($randomImage, PATHINFO_EXTENSION);

            // Construir el nuevo nombre de la imagen
            $newImageName = "{$productId}-1.{$extension}";

            // Copiar la imagen a la nueva ubicación y renombrarla
            Storage::disk('public')->copy($randomImage, 'images/' . $newImageName);

            return response()->json(['message' => 'Imagen copiada correctamente', 'image' => $newImageName]);
        } else {
            return response()->json(['message' => 'No hay imágenes en el directorio especificado']);
        }
    }

    function aes_decrypt($data, $key)
    {
        $data = base64_decode($data);
        $iv = substr($data, 0, openssl_cipher_iv_length('aes-256-cbc'));
        $data = substr($data, openssl_cipher_iv_length('aes-256-cbc'));
        $decrypted = openssl_decrypt($data, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
        return $decrypted;
    }

    function checkRequest($request)
    {
        $token = $request->token;
        $tokenSrv = $this->aes_decrypt($request->tokenSrv, env('API_PASS'));
        $tokenSrvArray = explode('|', $tokenSrv);

        return env('API_PASS') === $tokenSrvArray[0] && $token === $tokenSrvArray[1];
    }
}
