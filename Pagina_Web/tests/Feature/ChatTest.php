<?php

namespace Tests\Feature;

use App\Models\Carrito;
use App\Models\Chat;
use App\Models\ChatUsuario;
use App\Models\Pedido;
use Database\Seeders\UserSeeder;
use Database\Seeders\StoreSeeder;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class ChatTest extends TestCase
{
    use RefreshDatabase;
    public function test_enviar_un_mensaje_y_leerlo(): void
    {   
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);

       $carrito = new Carrito;
       $carrito->usuario_id = 1;
       $carrito->pagado = true;
       $carrito->fecha_compra = now();
       $carrito->save();

       $pedido = new Pedido;
       $pedido->usuario_id = 1;
       $pedido->carrito_id = $carrito->id;
       $pedido->fecha_compra = $carrito->fecha_compra;
       $pedido->recibido = false;
       $pedido->save();

       $chatUsuario = new ChatUsuario;
       $chatUsuario->id_comprador = 1;
       $chatUsuario->id_vendedor = 2;
       $chatUsuario->id_pedido = $pedido->id;
       $chatUsuario->save();

       $chat = new Chat;
       $chat->id_chat = $chatUsuario->id;
       $chat->enviado_por = 1;
       $chat->recibido_por = 2;
       $chat->mensaje = "Hola que tal";
       $chat->leido = false;
       $chat->save();

       $user = User::find(1);
       $this->actingAs($user);

       $response = $this->get(route('user.chat', $chatUsuario->id));
       $response->assertSee('Hola que tal');

       }


       public function test_entrar_en_un_chat_que_no_es_tuyo(): void
       {   
           $this->seed(UserSeeder::class);
           $this->seed(StoreSeeder::class);
          $carrito = new Carrito;
          $carrito->usuario_id = 1;
          $carrito->pagado = true;
          $carrito->fecha_compra = now();
          $carrito->save();
   
          $pedido = new Pedido;
          $pedido->usuario_id = 1;
          $pedido->carrito_id = $carrito->id;
          $pedido->fecha_compra = $carrito->fecha_compra;
          $pedido->recibido = false;
          $pedido->save();
   
          $chatUsuario = new ChatUsuario;
          $chatUsuario->id_comprador = 1;
          $chatUsuario->id_vendedor = 2;
          $chatUsuario->id_pedido = $pedido->id;
          $chatUsuario->save();

   
          $user = User::find(4);
          $this->actingAs($user);
   
          $response = $this->get(route('user.chat', $chatUsuario->id));
          $response->assertRedirect(route('errorTienda.showErrorTienda'));
   
          }
       
}
