@extends('layouts.master')

@section('content')
<div id="login-container">
    <form method="POST" id="auth-form" action="{{ route('auth.doResetPassword') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <h3>Hola {{ $user }},</h3>
        <h3> Porfavor rellena el formulario para cambiar la contraseña</h3>
        <x-pass />
        @if ($errors->has('password'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('password') }}
        </div>
        @endif
        @if ($errors->has('password_confirmation'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('password_confirmation') }}
        </div>
        @endif
        <div class="form-group">
            <button type="submit">Restablecer contraseña</button>
        </div>
    </form>
</div>
@endsection
@section('js')
<script src="/js/passwordCheck.js"></script>
<script src="/js/header.js"></script>
@endsection
