<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\authController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ErrorController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\ResetPasswordController;

//  PAGINAS PRODUCTOS / DETALLES DE CADA PRODUCTO Y ORDENAR Y BUSQUEDA
Route::get('/', [ProductosController::class, 'landing'])->name('landing.index');
Route::get('/home', [ProductosController::class, 'index'])->name('productos.index');
Route::get('/productos/show/{id}', [ProductosController::class, 'show'])->name('productos.show');
Route::get('search', [ProductosController::class, 'searchProducts'])->name('productos.search');

// PAGINA DE CARRITO Y TODAS LAS FUNCIONES PARA LA GESTION DEL CARRITO
Route::get('/carrito', [CartController::class, 'showCarrito'])->name('show.carrito');
Route::post('/agregar-producto', [CartController::class, 'agregar_producto'])->name('agregar.producto');
Route::delete('/rm-producto', [CartController::class, 'rm_producto'])->name('rm.producto');
Route::delete('/vaciar-carrito', [CartController::class, 'vaciar_carrito'])->name('vaciar.carrito');
Route::get('/carrito-comprar', [CartController::class, 'comprarCarrito'])->name('comprar.carrito');

// FUNCIONES Y PAGINAS RELACIONADAS CON LA AUTENTIFICACION DEL USUARIO
Route::get('/login', [authController::class, 'showLogin'])->name('auth.showLogin');
Route::post('/login', [authController::class, 'doLogin'])->name('auth.doLogin');
Route::get('/register', [authController::class, 'showRegister'])->name('auth.showRegister');
Route::post('/register', [authController::class, 'doRegister'])->name('auth.doRegister');
// Route::get('dashboard', [authController::class, 'dashboard']);
Route::get('logout', [authController::class, 'logout'])->name('auth.logOut');

// PERFIL DE USUARIO
Route::get('user/{username}/edit', [UserController::class, 'editUser'])->name('user.edit')->middleware('auth');
Route::post('user/{username}/edit', [UserController::class, 'doEditUser'])->name('user.edit')->middleware('auth');



Route::post('/confirm-product', [StoreController::class, 'confirmProduct'])->name('store.confirmProduct')->middleware('auth');
Route::get('/confirmar-pedidos', [StoreController::class, 'confirmarPedidos'])->name('store.confirmarPedidos')->middleware('auth');
Route::get('/confirmar-envio-pedido/{id}', [StoreController::class, 'confirmarEnvioPedido'])->name('store.confirmarEnvioPedido')->middleware('auth');
Route::get('/confirmar-llegada-pedido/{id}', [StoreController::class, 'confirmarLlegadaPedido'])->name('store.confirmarLlegadaPedido')->middleware('auth');
Route::get('/detalles-pedido-vendedor/{id}', [StoreController::class, 'detallesPedidoVendedor'])->name('store.detallesPedidoVendedor')->middleware('auth');

Route::get('/pedidos-user', [StoreController::class, 'pedidosUser'])->name('pedido.pedidosUser')->middleware('auth');
Route::get('/detalles-pedido-user/{id}', [StoreController::class, 'detallesPedidoUser'])->name('pedido.detallesPedidoUser')->middleware('auth');
Route::get('/detalles-pedido-recibido/{id}', [StoreController::class, 'detallesPedidoRecibidoUser'])->name('pedido.detallesPedidoRecibidoUser')->middleware('auth');

// TIENDA - STORE
Route::get('store/{shop_name}', [StoreController::class, 'showStore'])->name('store.show');
Route::get('/createStore', [StoreController::class, 'createStore'])->name('store.create')->middleware('auth');
Route::post('store/create', [StoreController::class, 'doCreateStore'])->name('store.doCreate')->middleware('auth');
Route::post('store/add-product{shop_name}', [StoreController::class, 'addProduct'])->name('store.addProduct')->middleware('auth');
Route::get('store/{shop_name}/manage', [StoreController::class, 'manageStore'])->name('store.manage')->middleware('auth');

// STORE MANAGE
Route::put('store/manage/{shop_name}', [StoreController::class, 'storeCustomize'])->name('store.customize');
Route::get('store/manage/hide/{id}', [StoreController::class, 'manageHide'])->name('store.manage.hide');
Route::get('store/manage/edit/{id}', [StoreController::class, 'manageEdit'])->name('store.manage.edit');
Route::get('store/manage/delete/{id}', [StoreController::class, 'manageDelete'])->name('store.manage.delete');
Route::post('store/manage/edit/{id}', [StoreController::class, 'storeProductEdit'])->name('store.product.edit');


// FUNCIONES DE RESTABLECER CONTRASEÑA
Route::get('/recuperarContrasena', [ResetPasswordController::class, 'recoveryPassword'])->name('auth.recoveryPassword');
Route::post('/recuperarContrasena', [ResetPasswordController::class, 'recoveryPasswordSender'])->name('auth.doRecoveryPassword');
Route::get('reset-password/{token}', [ResetPasswordController::class, 'showResetPasswordForm'])->name('auth.showResetPassword');
Route::post('reset-password', [ResetPasswordController::class, 'submitResetPasswordForm'])->name('auth.doResetPassword');


Route::get('/errorGeneral', [ErrorController::class, 'showErrorGeneral'])->name('errorGeneral.showErrorGeneral');
Route::get('/errorProduct', [ErrorController::class, 'showErrorProduct'])->name('errorProduct.showErrorProduct');
Route::get('/errorTienda', [ErrorController::class, 'showErrorTienda'])->name('errorTienda.showErrorTienda');

// CHAT USUARIO
Route::get('/chat/{id}', [ChatController::class, 'showChat'])->name('user.chat');
Route::post('/chat/{id}', [ChatController::class, 'sendChat'])->name('send.chat');