<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\CategoriaNivel1Seeder;
use Database\Seeders\CategoriaNivel2Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $this->call([
            CategoriaNivel1Seeder::class,
            CategoriaNivel2Seeder::class,
            UserSeeder::class,
            StoreSeeder::class,
            ProductoSeeder::class,
            CategoriaproductoSeeder::class,
        ]);
    }
}
