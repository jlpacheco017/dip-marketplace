var btns_confirm = document.querySelectorAll('.btn_confirm');
window.addEventListener('pageshow', function(event) {
  if (event.persisted) {
    window.location.reload()
  }
});
btns_confirm.forEach(function(btn_confirm) {
    btn_confirm.addEventListener('click', function() {
        var pedido_id = btn_confirm.getAttribute('data-pedido-id');
        fetch("/confirm-product", {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({ id: pedido_id })
          });
          location.reload()
    });
});