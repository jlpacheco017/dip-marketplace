<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Carrito;
use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Http\Request;
use App\Models\LineasCarrito;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use \phpseclib\Crypt\AES;
use \phpseclib\Crypt\Random;

class ProductosController extends Controller
{
    // Muestra todos los productos en la pagina Index
    public function index(Request $request)
    {
        if (Auth::check()) {
            Log::debug($request->ip() . ' El usuario con id= ' . Auth::user()->id . ' Ha accedido a la vista de Productos -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
        } else {
            Log::debug($request->ip() . ' Ha accedido a la vista de Productos -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
        }
        $categoriaId = isset($request->categoria) ? $request->categoria : "0";
        $busqueda = $request->search;
        $productos_Store = [];
        $request->flash();
        // Ordenacion
        $tipoOrdenacion = $request->ordenacion;
        switch ($tipoOrdenacion) {
            default:
            case "NombreAZ":
                $ordenacionColumna = 'titulo';
                $ordenacionTipo = 'asc';
                break;
            case "NombreZA":
                $ordenacionColumna = 'titulo';
                $ordenacionTipo = 'desc';
                break;
            case "PrecioMayor":
                $ordenacionColumna = 'precio';
                $ordenacionTipo = 'desc';
                break;
            case "PrecioMenor":
                $ordenacionColumna = 'precio';
                $ordenacionTipo = 'asc';
                break;
        }

        if (Auth::check()) {
            $usuario = Auth::user();
            $carrito_select = Carrito::where('usuario_id', $usuario->id)->where('pagado', false)->first();
            $carritoServidor = LineasCarrito::where('carrito_id', $carrito_select->id)->get();
            $arrayCarrito = array();
            $idsCarrito = [];

            $store = Store::where('user_id', $usuario->id)->first();
            if ($store) {
                $productos_Store = Producto::where('store_id', $store->id)->get()->pluck('id')->toArray();
            }else{
                $productos_Store = [];
            }
            foreach ($carritoServidor as $key => $value) {
                $producto = Producto::findOrFail($value->producto_id);

                if (is_array($arrayCarrito)) {
                    array_push($arrayCarrito, [
                        'id' => $producto->id,
                        'img' => $producto->img,
                        'titulo' => $producto->titulo,
                        'precio' => $producto->precio,

                    ]);
                    array_push($idsCarrito, $producto->id);
                } else {
                    array_unshift($arrayCarrito, array(
                        'id' => $producto->id,
                        'img' => $producto->img,
                        'titulo' => $producto->titulo,
                        'precio' => $producto->precio,
                    ));
                    array_unshift($idsCarrito, $producto->id);
                }
            }
        } else {
            $arrayCarrito = 0;
            $idsCarrito = 0;
        }
        if ($categoriaId !== "0") {
            if (isset($busqueda)) {
                //Productos por categoria y busqueda
                return view('productos.index', [
                    'categorias' => Categoria::all()->where('nivel', 1),
                    'productos' => Producto::join('categoria_producto', 'categoria_producto.producto_id', '=', 'productos.id')
                        ->where('categoria_producto.categoria_id', '=', $categoriaId)->where('disponible', true)
                        ->where('titulo', 'LIKE', '%' . $busqueda . '%')
                        ->select('producto_id', 'productos.*')
                        ->orderBy($ordenacionColumna, $ordenacionTipo)
                        ->paginate(env('NUMERO_PAGINACION'))->withQueryString(),
                    'carrito' => $arrayCarrito,
                    'idsCarrito' => $idsCarrito,
                    'productosStore' => $productos_Store
                ]);
            } else {
                // Productos por categoria y sin busqueda
                return view('productos.index', [
                    'categorias' => Categoria::all()->where('nivel', 1),
                    'productos' => Producto::join('categoria_producto', 'categoria_producto.producto_id', '=', 'productos.id')
                        ->where('categoria_producto.categoria_id', '=', $categoriaId)->where('disponible', true)
                        ->select('producto_id', 'productos.*')
                        ->orderBy($ordenacionColumna, $ordenacionTipo)
                        ->paginate(env('NUMERO_PAGINACION'))->withQueryString(),
                    'carrito' => $arrayCarrito,
                    'idsCarrito' => $idsCarrito,
                    'productosStore' => $productos_Store
                ]);
            }
        } else {
            if (isset($busqueda)) {
                // Todos los productos y su busqueda
                return view('productos.index', [
                    'categorias' => Categoria::all()->where('nivel', 1),
                    'productos' => Producto::orderBy($ordenacionColumna, $ordenacionTipo)->where('disponible', true)
                        ->where('titulo', 'LIKE', '%' . $busqueda . '%')
                        ->paginate(env('NUMERO_PAGINACION'))->withQueryString(),
                    'carrito' => $arrayCarrito,
                    'idsCarrito' => $idsCarrito,
                    'productosStore' => $productos_Store

                ]);
            } else {
                // Todos los productos
                return view('productos.index', [
                    'categorias' => Categoria::all()->where('nivel', 1),
                    'productos' => Producto::orderBy($ordenacionColumna, $ordenacionTipo)->where('disponible', true)
                        ->paginate(env('NUMERO_PAGINACION'))->withQueryString(),
                    'carrito' => $arrayCarrito,
                    'idsCarrito' => $idsCarrito,
                    'productosStore' => $productos_Store
                ]);
            }
        }


        session(['categoria' => '']);
        session(['categoriaTitulo' => '']);
    }
    public function landing()
    {
        $productos_Store = [];
        if (Auth::check()) {
            $usuario = Auth::user();
            $carrito_select = Carrito::where('usuario_id', $usuario->id)->where('pagado', false)->first();
            $carritoServidor = LineasCarrito::where('carrito_id', $carrito_select->id)->get();
            $arrayCarrito = array();
            $idsCarrito = [];
            $store = Store::where('user_id', $usuario->id)->first();
            if ($store) {
                $productos_Store = Producto::where('store_id', $store->id)->get()->pluck('id')->toArray();
            }else{
                $productos_Store = [];
            }
            foreach ($carritoServidor as $key => $value) {
                $producto = Producto::findOrFail($value->producto_id);

                if (is_array($arrayCarrito)) {
                    array_push($arrayCarrito, [
                        'id' => $producto->id,
                        'img' => $producto->img,
                        'titulo' => $producto->titulo,
                        'precio' => $producto->precio,

                    ]);
                    array_push($idsCarrito, $producto->id);
                } else {
                    array_unshift($arrayCarrito, array(
                        'id' => $producto->id,
                        'img' => $producto->img,
                        'titulo' => $producto->titulo,
                        'precio' => $producto->precio,
                    ));
                    array_unshift($idsCarrito, $producto->id);
                }
            }
        } else {
            $arrayCarrito = 0;
            $idsCarrito = 0;
        }
        $todos_productos = Producto::all();
        if (count($todos_productos) >= 50) {
            # code...
            $productos_env = [env('PRODUCTO_1'), env('PRODUCTO_2'), env('PRODUCTO_3'), env('PRODUCTO_4')];
            $categoria1 = env('CATEGORIA_1');
            $categoria2 = env('CATEGORIA_2');
            $categoria3 = env('CATEGORIA_3');
            $categoria4 = env('CATEGORIA_4');

            #RECOGER LOS 4 PRODUCTOS DESTACADOS Y GUARDARLOS EN  CON TODOS LOS DETALLES DE DENTRO
            $productos_destacados = [];
            for ($i = 0; $i < count($productos_env); $i++) {
                $producto = Producto::find($productos_env[$i]);
                if ($producto) {
                    array_push($productos_destacados, $producto);
                } else {
                    $producto = Producto::all()->random();
                    array_push($productos_destacados, $producto);
                }
            }

            // RECOGER 2 PRODUCTOS ALEATORIOS DE LA CATEGORIA SELECCIONADA PARA MOSTRARLOS
            $productos_cat1 = [];
            $productos_cat2 = [];
            $productos_cat3 = [];
            $productos_cat4 = [];
            if ($categoria1 == null || !Categoria::where('id', $categoria1)->first()) {

                $categoria1 = Categoria::inRandomOrder()->first();
            } else {

                $categoria1 = Categoria::where('id', $categoria1)->first();
            }

            while (count($categoria1->productos()->get()) < 2) {
                $categoria1 = Categoria::inRandomOrder()->first();
            }
            $categoria1 = $categoria1->id;


            if ($categoria2 == null || !Categoria::where('id', $categoria2)->first()) {
                $categoria2 = Categoria::inRandomOrder()->first();
            } else {
                $categoria2 = Categoria::where('id', $categoria2)->first();
            }

            while (count($categoria2->productos()->get()) < 2) {
                $categoria2 = Categoria::inRandomOrder()->first();
            }
            $categoria2 = $categoria2->id;




            if ($categoria3 == null || !Categoria::where('id', $categoria3)->first()) {
                $categoria3 = Categoria::inRandomOrder()->first();
            } else {
                $categoria3 = Categoria::where('id', $categoria3)->first();
            }

            while (count($categoria3->productos()->get()) < 2) {
                $categoria3 = Categoria::inRandomOrder()->first();
            }
            $categoria3 = $categoria3->id;




            if ($categoria4 == null || !Categoria::where('id', $categoria4)->first()) {
                $categoria4 = Categoria::inRandomOrder()->first();
            } else {
                $categoria4 = Categoria::where('id', $categoria4)->first();
            }

            while (count($categoria4->productos()->get()) < 2) {
                $categoria4 = Categoria::inRandomOrder()->first();
            }
            $categoria4 = $categoria4->id;



            // CATEGORIA 1 SELECCION DE PRODUCTOS SI SE HAN PASADO CON NULL
            $categoria = Categoria::find($categoria1);
            if (env('PRODUCTO_1_1') == null) {
                array_push($productos_cat1, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_1_1'));
                if ($producto) {
                    array_push($productos_cat1, $producto);
                } else {
                    array_push($productos_cat1, $categoria->productos->random());
                }
            }
            if (env('PRODUCTO_1_2') == null) {
                array_push($productos_cat1, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_1_2'));
                if ($producto) {
                    array_push($productos_cat1, $producto);
                } else {
                    array_push($productos_cat1, $categoria->productos->random());
                }
            }


            // CATEGORIA 2 SELECCION DE PRODUCTOS SI SE HAN PASADO CON NULL
            $categoria = Categoria::find($categoria2);
            if (env('PRODUCTO_2_1') == null) {
                array_push($productos_cat2, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_2_1'));
                if ($producto) {
                    array_push($productos_cat2, $producto);
                } else {
                    array_push($productos_cat2, $categoria->productos->random());
                }
            }
            if (env('PRODUCTO_2_2') == null) {
                array_push($productos_cat2, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_2_2'));
                if ($producto) {
                    array_push($productos_cat2, $producto);
                } else {
                    array_push($productos_cat2, $categoria->productos->random());
                }
            }


            // CATEGORIA 3 SELECCION DE PRODUCTOS SI SE HAN PASADO CON NULL
            $categoria = Categoria::find($categoria3);
            if (env('PRODUCTO_3_1') == null) {
                array_push($productos_cat3, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_3_1'));
                if ($producto) {
                    array_push($productos_cat3, $producto);
                } else {
                    array_push($productos_cat3, $categoria->productos->random());
                }
            }
            if (env('PRODUCTO_3_2') == null) {
                array_push($productos_cat3, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_3_2'));
                if ($producto) {
                    array_push($productos_cat3, $producto);
                } else {
                    array_push($productos_cat3, $categoria->productos->random());
                }
            }


            // CATEGORIA 4 SELECCION DE PRODUCTOS SI SE HAN PASADO CON NULL
            $categoria = Categoria::find($categoria4);
            if (env('PRODUCTO_4_1') == null) {
                array_push($productos_cat4, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_4_1'));
                if ($producto) {
                    array_push($productos_cat4, $producto);
                } else {
                    array_push($productos_cat4, $categoria->productos->random());
                }
            }
            if (env('PRODUCTO_4_2') == null) {
                array_push($productos_cat4, $categoria->productos->random());
            } else {
                $producto = $categoria->productos()->where('disponible', true)->find(env('PRODUCTO_4_2'));
                if ($producto) {
                    array_push($productos_cat4, $producto);
                } else {
                    array_push($productos_cat4, $categoria->productos->random());
                }
            }
            $categoria1 = Categoria::find($categoria1);
            $categoria2 = Categoria::find($categoria2);
            $categoria3 = Categoria::find($categoria3);
            $categoria4 = Categoria::find($categoria4);
            $productos_suficientes = true;
            return view('productos.landing', [
                'categorias' => Categoria::all()->where('nivel', 1),
                'productos' => $productos_destacados,
                'cat1' => $productos_cat1,
                'cat2' => $productos_cat2,
                'cat3' => $productos_cat3,
                'cat4' => $productos_cat4,
                'categoria1' => $categoria1,
                'categoria2' => $categoria2,
                'categoria3' => $categoria3,
                'categoria4' => $categoria4,
                'carrito' => $arrayCarrito,
                'idsCarrito' => $idsCarrito,
                'productos_suficientes' => $productos_suficientes,
                'productosStore' => $productos_Store
            ]);
        } else {
            $productos_suficientes = false;
            return view('productos.landing', [
                'productos_suficientes' => $productos_suficientes,
                'categorias' => Categoria::all()->where('nivel', 1),
                'carrito' => $arrayCarrito,
                'idsCarrito' => $idsCarrito,
            ]);
        }
    }


    public function show(Request $request, string $id)
    {
        $imagenes = $this->getProductImages($id);
        Log::debug($request->ip() . ' Ha accedido al producto con id = ' . $id . ' -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
        if (Auth::check()) {
            $store = Store::where('user_id', Auth::user()->id)->first();
            if ($store) {
                $productos_Store = Producto::where('store_id', $store->id)->get()->pluck('id')->toArray();
            }else{
                $productos_Store = [];
            }
            $usuario = Auth::user();
            $carrito_select = Carrito::where('usuario_id', $usuario->id)->where('pagado', false)->first();
            $carritoServidor = count(LineasCarrito::where('carrito_id', $carrito_select->id)->get());
            $productChecker = LineasCarrito::where('carrito_id', $carrito_select->id)
                ->where('producto_id', $id)
                ->get();

            if (count($productChecker) > 0) {
                $productChecker = true;
            } else {
                $productChecker = false;
            }
            return view('productos.show', [
                'id' => $id,
                'producto' => Producto::findOrFail($id),
                'carrito' => $carritoServidor,
                'checker' => $productChecker,
                'imagenes' => $imagenes,
                'productosStore' => $productos_Store
            ]);
        }
        return view('productos.show', [
            'id' => $id,
            'producto' => Producto::findOrFail($id),
            'imagenes' => $imagenes
        ]);
    }

    public function getProductImages($id)
    {
        $response = Http::get(env('API_IMAGE').$id);
        $data = $response->json();

        if ($data !== null && $data['status'] === 'success') {
            return $data['images'];
        } else {
            return ['images' => env('API_IMAGE').$id];
        }
        return view('productos.show', ['id' => $id], ['producto' => Producto::findOrFail($id)]);
    }

    public function test(Request $request)
    {
        // Función de prubas de subida de fotos
        if (!$request->hasFile('image')) {
            return back()->with('error', 'No se ha cargado ninguna imagen');
        }
        
        if (!$request->file('image')->isValid()) {
            return back()->with('error', 'La imagen no es válida');
        }
        $roll = 8;
        $roll2 = 3;
        
        $response = Http::attach('image', file_get_contents($request->file('image')), $request->file('image')->getClientOriginalName())
        ->post(env('API_IMAGE'), [
            'idProduct' => $roll,
            'imageProductPosition' => $roll2,
            'token'    => Auth::user()->token,
            'tokenSrv' => $this->aes_encrypt(Auth::user()->token),
        ]);

        if ($response->successful()) {
            return back()->with('success', 'Imagen subida exitosamente');
        } else {
            dd("error");
            return back()->with('error', 'Error al subir la imagen');
        }
    }

    function aes_encrypt($userToken) {
        $tokenSrv = env('API_TOKEN').'|'.$userToken;
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($tokenSrv, 'aes-256-cbc',env('API_PASS'), OPENSSL_RAW_DATA, $iv);
        $msgForCrypt = base64_encode($iv . $encrypted); 

        $this->aes_decrypt($msgForCrypt, env('API_PASS'));

        return base64_encode($iv . $encrypted);
    }

    function aes_decrypt($data, $key) {
        $data = base64_decode($data);
        $iv = substr($data, 0, openssl_cipher_iv_length('aes-256-cbc'));
        $data = substr($data, openssl_cipher_iv_length('aes-256-cbc'));
        $decrypted = openssl_decrypt($data, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
        return $decrypted;
    }
}
