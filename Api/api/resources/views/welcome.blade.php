<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Images</title>
</head>
<body>
    <div id="images-container"></div>

    <script>
        const productId = 2; // Asegúrate de reemplazar esto con el ID del producto que deseas obtener las imágenes.

        fetch(`/api/image/${productId}`)
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success') {
                    const imagesContainer = document.getElementById('images-container');

                    data.images.forEach(imageUrl => {
                        const imgElement = document.createElement('img');
                        imgElement.src = imageUrl;
                        imgElement.alt = 'Product Image';
                        imgElement.style.width = '100px';
                        imgElement.style.height = '100px';

                        imagesContainer.appendChild(imgElement);
                    });
                } else {
                    console.error(data.message);
                }
            })
            .catch(error => {
                console.error('Error fetching product images:', error);
            });
    </script>
</body>
</html>