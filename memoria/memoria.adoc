== DIP Marketplace - ABP 2

=== Miembros: Diego Fernández, Iván Reyes, Pacheco Guerrero


=== Introducción

=== Memoria semanal

==== Primera semana -> 27/02/2023 - 03/03/2023

==== Tareas realizadas:
* Inicialización del proyecto de GIT.
* Creación de la máquina vagrant con una base de datos.
* Guía de estilos. 
* Diseños de Sketches, Wireframes y Mockups.
* Instalación de Laravel 10
* Creación de la tabla 'Productos' de la base de datos y sus relaciones.
* Creación de las 'Factories' y los 'Seeders' para poder rellenar la base de datos con datos falsos.
* Mostrar todos los productos que tenemos almacenados en la base de datos en la página 'Index'.

==== Problemas encontrados:
Esta primera semana un compañero ha faltado toda la semana y nos ha ralentizado en el arranque del proyecto. Por suerte, ya que este era el inicio del proyecto no teníamos demasiadas tareas complejas, ya que casi todas eran sobre diseño o planificación, hemos podido avanzar bastante más de lo que esperábamos para tener un compañero menos.

==== Segunda semana -> 06/03/2023 - 10/03/2023

==== Tareas realizadas:
* Realizado los testeos para los productos.
* Filtrado por categorías.
* Añadidas categorías de nivel 2.
* Creada la página de detalles para los productos.
* Al entrar en una categoría se ha implementado que nos muestre los productos que pertenecen a su subcategoría.
* Ahora los seeders son interactivos.
* Se han cambiado las fotos que obteníamos mediante faker por unas reales.
* Los LOGS han sido actualizados y automatizados para que escriba el nombre de la función y de la clase. 
* Creación de la vista del LOGIN sin funcionalidad.

==== Problemas encontrados:
En esta segunda semana hemos mejorado bastante en comparación de la primera.  

Hemos podido avanzar bastante porque ya estábamos los 3 miembros y se ha notado la diferencia, llevamos algo de retraso del segundo sprint y aún no hemos comenzado el carrito de la compra porque tenemos aún temas pendientes del primer sprint, pero generalmente no vamos tan mal. Preferimos ir poco a poco cerrando temas que intentar abarcar todo y al final no hacer nada.

==== Tercera semana -> 13/03/2023 - 17/03/2023

==== Tareas realizadas:
* Diseño responsive para Desktop y Mobile de productos.index.
* Diseño responsive para Desktop y Mobile del Header.
* Carrito de compra del usuario no autenticado.
* Hemos corregido la barra de búsqueda ya que funcionaba mal.
* Se ha comenzado la paginación.
* Los testeos sobre productos.index se han corregido y ahora funcionan correctamente.

==== Problemas encontrados:
En esta tercera semana hemos seguido trabajando y hemos podido sacar bastante agua del barco.  
Para esta tercera semana hemos pensado que lo mejor era empezar a matar temas y quitarnos cosas de encima. Lo primero que queríamos cambiar era hacer que la página fuera responsive y agradable a la vista, una vez conseguido decidimos implementa el carrito de la compra porque nos parecía algo bastante importante. También decidimos comenzar la paginación y teníamos que arreglar algunos problemas que estábamos arrastrando como la paginación o el filtrado de categorías que aún siguen en proceso de cerrar para siempre.

==== Cuarta semana -> 20/03/2023 - 24/03/2023
==== Tareas realizadas:
* Creación de la página de detalles de los productos.
* Actualizado Header como componente.
* Creado los botones de categorías para poder filtrar por ellas.
* Login y register.
* Relación entre el carrito y el usuario.
* Se ha añadido un dropmenu al header.
* Las categorías funcionan junto la busqueda y los filtros de ordenación.

==== Problemas encontrados:
En esta cuarta semana, se realizó la página de detalles de los productos simplemente mostrando la información mínima sin darle estilo. también se decidió hacer que el header sea un componente, ya que se utilizará en todas las páginas. En esta cuarta semana ya era hora de acabar el filtrado por categorías que ya lo estábamos arrastrando bastante. Finalmente, se ha conseguido acabar y funciona sorprendentemente bien.

==== Quinta semana -> 27/03/2023 - 31/03/2023
==== Tareas realizadas:
* Se ha arreglado la sincronización del servidor y el carrito.
* Las categorías se seleccionan correctamente.
* Corregido un bug del carrito.
* Se le ha dado diseño y estilo a la barra de busqueda y los filtros de ordenación.
* Corregido el tamaño de los productos al mostrar solo 1.
* Se ha dividido las funciones por controladores.

==== Problemas encontrados:
En esta quinta semana, antes de irnos de vacaciones y desconectar hemos acabado algunas cosas que teníamos pendientes. Primero hemos añadido que las categorías se puedan seleccionar y se quede el color de la categoría seleccionada. Hemos corregido algún que otro bug con el carrito que siempre se rompe y le hemos acabado de dar estilo a todos los filtros de búsqueda.

==== Sexta semana -> 10/04/2023 - 15/04/2023
==== Tareas realizadas:
* Se han añadido mas testeos para los Productos.
* Refactorización del carrito en clases
* Creación del perfil de usuario.
* Se han añadido usuarios por defecto.

==== Problemas encontrados:
En la sexta semana, hemos realizado los testeos de los productos para que sean un poco más complejos y se ha refactorizado la clase del carrito utilizando clases y la verdad que ha simplificado bastante código. Se ha acabado con la lógica del perfil de usuario, y se ha añadido los usuarios por defecto.
==== Septima semana -> 17/04/2023 - 21/04/2023

=== Diseño de la base de datos
    
.Todas las tablas
image::show_tables.png[Todas las tablas, align="center"]

.Mostrar la tabla categorías
image::select_categorias.png[Mostrar la tabla 'categorias', align="center"]

.Mostrar la tabla productos
image::select_productos.png[Mostrar la tabla 'productos', align="center"]

.Mostrar la tabla pivote de categorías y productos
image::select_categoria_producto.png[Mostrar la tabla pivote 'categoria_producto', align="center"]

*==== Tercera semana -> 20/03/2023 - 26/03/2023

==== Tareas realizadas:
* Carrito de la compra - Usuario no autenticado.
* Categorías junto la barra de búsqueda y la ordenación
* Dropmenu para el usuario en el header.


==== Problemas encontrados:
En esta semana los problemas que nos hemos encontrado han sido principalmente que hemos tenido bastante lio con el carrito de la compra, ya que al estar hecho primero para los usuarios no autenticados y despues para los autenticados, hemos tenido bastantes problemas a la hora de mantener los datos y no hemos conseguido que funcione del todo bien.

Por otro lado a la hora de hacer las busquedas, al no hacerlas del todo bien, hemos tenido tambien complicaciones a la hora de implementarlo, pero al final hemos conseguido sacarlo adelante.

*==== Tercera semana -> 27/03/2023 - 02/04/2023

==== Tareas realizadas:
* Carrito de la compra - Usuario autenticado.
* Rehacer los logs para enviar mensjes al hacer cualquier cosa
* Dropmenu para el usuario en el header.
* Corregir el tamaño de los productos


==== Problemas encontrados:
En esta semana los problemas que nos hemos encontrado han sido principalmente que al mezclar todo lo que hemos hecho, al estar tocando sobre el mismo fichero, hemos fusionado los dos porque todo servia y hemos terminado perdiendo cosas importantes que hacia que el codigo funcionase, lo hemos arreglado y a raiz de eso el carrito no ha parado de dar fallos, ahora mismo esta controlado. DE MOMENTO.

*==== Tercera semana -> 10/04/2023 - 16/04/2023

==== Tareas realizadas:
* Login se queda en blanco al meter usuarios erroneos
* Hacer la pagina de perfil de usuarios a medias.
* Mostrar y filtrar por categorias, orden y busqueda.
* Feedback de usuarios 
* Paginacion de la pagina de productos
* Carrito servidor aparentemente funciona


==== Problemas encontrados:
En el apartado de filtrar por categorias/busqueda/ordenar dio bastantes problemas ya que al ser varias funciones no teniamos muy claro el como hacer que funcionasen en conjunto.
En el carrito tuve problemas a la hora de pequeños errores que habian que hacer un gran refactor, porque era codigo reutilizado del carrito local

*==== Tercera semana -> 17/04/2023 - 23/04/2023

==== Tareas realizadas:
* Carrito servidor y local funciona correctamente
* El correo de restablecer contraseña se envia y funciona
* Creacion de todos los logs
* Añadir Feedback a usuarios de si ha puesto la contraseña correctamente o que caracteres debe de tener
* Paginacion de la pagina de productosEvitar que el usuario pueda añadir caracteres extraños en el formulario de registro
* Acabar la landin page con el archivo de configuracion
* Acabar y darle estilo a la pagina de Usuario
* Al hacer login y al refistrarte te envia correos

==== Problemas encontrados:
Ultimo refactor entero del carrito, nos hemos dado cuenta de que salia mas retable dividir los carritos en dos para hacer mas ordenado los js y no queden muy grandes.
Al principio los correos no se enviaban, pero, al final, conseguimos hacer que enviaran los correos y implementarlo en el login, registro y recuperar contraseña con estilos.
La paginacion estaba hecha y al hacer el merge de las categorias desaperecio, se tuvo que volver hacer.

=== Enlaces:

https://trello.com/b/Kv6k989T/abp-daw2at-projecte-2[Link Trello]

https://www.figma.com/file/EEfy2nJnPll5OmbhFRmLNW/Style-Guide---PID-Marketplace-ABP-2?node-id=0%3A1&t=k2SSytZnFMlkjnI6-1[Link StyleGuide Figma]

https://git.copernic.cat/reyes.navarro.ivan/dip_marketplace_abp2[Link GIT]




