<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Store;
use App\Models\Carrito;
use App\Models\Producto;
use App\Models\LineasCarrito;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Seeders\UserSeeder;
use Database\Seeders\StoreSeeder;

class CarritoTest extends TestCase
{
    use RefreshDatabase;

    public function test_crear_carrito_de_usuario(): void
    {   
       
        $carrito = new Carrito;
        $user = User::factory()->create();
        $carrito -> usuario_id = $user->id;
        $carrito -> save();
            
        $this->assertDatabaseHas('carritos', ['usuario_id' => 1, 'pagado' => false]);
    }

    public function test_añadir_producto_al_carrito(): void
    {   
       
        $carrito = new Carrito;
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $producto = Producto::factory()->create();
        $user = User::factory()->create();
        $lineas_carrito = new LineasCarrito();


        $carrito -> usuario_id = $user->id;
        $carrito -> save();

        
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id =$producto->id;
        $lineas_carrito->precio = $producto->id;
        $lineas_carrito->save();
        
        

        $this->assertDatabaseHas('lineas_carritos', ['producto_id' => $producto->id, 'carrito_id' => $carrito->id]);
    }

    public function test_mostrar_producto_no_disponible_en_el_carrito(): void
    {   
       
        $lineas_carrito = new LineasCarrito();
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $user = User::find(1);
        
        // ENTRAMOS CON EL USUARIO CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'jlpacheco',
            'password' => '1234',
        ]);

        
        $producto = Producto::factory()->create();
        $producto->disponible = false;
        $producto->save();

        
        $carrito = Carrito::where('usuario_id', $user->id)->first();
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id = $producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();

        $response = $this->get(route('show.carrito'));

        // SI NO VA EJECUTA EN EL TERMINAL " composer require symfony/dom-crawler "
        $crawler = new Crawler($response->getContent()); 
        $this->assertTrue($crawler->filter('.noDisponible')->count() > 0);
    }

    public function test_eliminar_producto_del_carrito(): void
    {   
       
        
        $lineas_carrito = new LineasCarrito();
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $user = User::find(1);
        
        // ENTRAMOS CON EL USUARIO CREADO.
        $response = $this->post(Route('auth.doLogin'), [
            'email' => 'jlpacheco',
            'password' => '1234',
        ]);

        
        $producto = Producto::factory()->create();
        $producto->disponible = false;
        $producto->save();

        
        $carrito = Carrito::where('usuario_id', $user->id)->first();
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id = $producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();
        
        $response = $this->delete(Route('rm.producto', ['id' => $producto->id]));

        $this->assertDatabaseMissing('lineas_carritos', ['producto_id' => $producto->id]);
        
    }

}
