<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carrito extends Model
{
    use HasFactory;
    protected $table = 'carritos';

    protected $fillable = [
        'usuario_id',
        'fecha_compra',
        'pagado'
    ];

    public function lineas()
    {
        return $this->hasMany(LineaCarrito::class, 'carrito_id');
    }
    
    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'usuario_id');
    }
    public function getCarritoActivoPorUsuario($id){
        return $this->query()->where('usuario_id', $id)->where('pagado', false)->first();
    }
}
