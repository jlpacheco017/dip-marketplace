<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\User;
use App\Models\Store;
use App\Models\Pedido;
use App\Models\Carrito;
use App\Models\Producto;
use App\Models\Categoria;
use App\Models\ChatUsuario;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\LineasCarrito;
use Illuminate\Validation\Rule;
use App\Models\CategoriaProducto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    public function showStore(String $shop_name)
    {
        $shop_name = trim($shop_name);

        $usuario = Auth::user();
        $carrito_select = Carrito::where('usuario_id', $usuario->id)->where('pagado', false)->first();
        $carritoServidor = LineasCarrito::where('carrito_id', $carrito_select->id)->get();
        $arrayCarrito = array();
        $idsCarrito = [];
        foreach ($carritoServidor as $key => $value) {
            $producto = Producto::findOrFail($value->producto_id);

            if (is_array($arrayCarrito)) {
                array_push($arrayCarrito, [
                    'id' => $producto->id,
                    'img' => $producto->img,
                    'titulo' => $producto->titulo,
                    'precio' => $producto->precio,

                ]);
                array_push($idsCarrito, $producto->id);
            } else {
                array_unshift($arrayCarrito, array(
                    'id' => $producto->id,
                    'img' => $producto->img,
                    'titulo' => $producto->titulo,
                    'precio' => $producto->precio,
                ));
                array_unshift($idsCarrito, $producto->id);
            }
        }
        $esPropietario = false;
        $objStore = new Store;
        $shop = Store::where('shop_name', $shop_name)->first();
        if ($shop) {
            if ($objStore->owner($shop->id)) {
                $esPropietario = true;
            }
        } else {
            return redirect(Route('errorTienda.showErrorTienda'));
        }

        $productos = Producto::where('store_id', $shop->id)->get();
        return view('store.show', [
            'shop' => $shop, 'owner' => $esPropietario, 'productos' => $productos, 'carrito' => $arrayCarrito,
            'idsCarrito' => $idsCarrito,
        ]);
    }

    public function createStore()
    {
        return view('store.create');
    }

    public function doCreateStore(Request $request)
    {
        if ($request->input('nifocif')) {
            if ($request->input('nifocif') == 'nif') {
                $validatedData = $request->validate(
                    [
                        'shop_name' => 'required|unique:stores|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]+$/u',
                        'responsible' => 'required|regex:/^[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ\s]+$/',
                        'nifocif' => 'required',
                        'id' => 'required|regex:/^[0-9]{8}[a-zA-Z]$/',
                        'image' => 'image|max:2048',
                        'titleStore' => 'max:25'
                    ],
                );
            }
            if ($request->input('nifocif') == 'cif') {
                $validatedData = $request->validate(
                    [
                        'shop_name' => 'required|unique:stores|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]+$/u',
                        'responsible' => 'required|regex:/^[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ\s]+$/',
                        'nifocif' => 'required',
                        'id' => 'required|regex:/^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/',
                        'image' => 'image|max:2048',
                        'titleStore' => 'max:25'
                    ],
                );
            }

            $store = new Store();
            $store->shop_name = Str::lower($validatedData['shop_name']);
            $store->responsible = $validatedData['responsible'];
            $store->identifier = $validatedData['id'];


            if ($request->input('titleStore') != null) {
                $store->title = $validatedData['titleStore'];
            } else {
                $store->title = '¡Bienvenido a nuestra tienda!';
            }

            $store->user_id = Auth::user()->id;


            $image = $request->file('image');

            if ($image != null) {
                $imageName = "img/store/" . Auth::user()->id . ".jpg";
                $image->move(public_path('img/store/'), $imageName);
            }

            $imageName = "img/store/logo-default.svg";
            $store->logo = $imageName;
            $store->save();

            return redirect()->route('store.show', Auth::user()->store->shop_name);
        }
        return back()->withErrors(['radio' => "Debes marcar 'NIF' o 'CIF'"])->withInput();
    }

    public function manageStore($shop_name)
    {
        $shop = Store::where('shop_name', $shop_name)->first();

        if (!$shop) {
            return redirect(Route('errorTienda.showErrorTienda'));
        }

        if (auth()->user()->id !== $shop->user_id) {
            return back();
        }

        $productos = Producto::where('store_id', $shop->id)->get();

        $categorias_nivel_1 = Categoria::where('nivel', 1)->get();
        $categorias_nivel_2 = Categoria::where('nivel', 2)->get();

        return view('store.manage', ['productos' => $productos, 'shop' => $shop, 'categorias_nivel_1' => $categorias_nivel_1, 'categorias_nivel_2' => $categorias_nivel_2]);
    }

     public function addProduct(Request $request, $shop_id)
    {

        $request->validate(
            [
                'title' => 'required|max:50|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ.\'"\s\-()]+$/u',
                'description' => 'required|max:255',
                'price' => 'required|integer|min:1|max:50000',
            ]
        );

        $producto = new Producto;
        $producto->titulo = $request->input('title');
        $producto->descripcion = $request->input('description');
        $producto->precio = $request->input('price');
        $producto->img = 'tmp';
        $producto->store_id = $shop_id;
        $producto->save();
        $producto->img = env('API_IMAGE') . $producto->id . '/1';
        $producto->save();

        // CATEGORÍAS NIVEL 1
        $categorias_seleccionadas_nivel_1 = $request->input('categoriasNivel1', []);

        foreach ($categorias_seleccionadas_nivel_1 as $categoria1) {
            $create = new CategoriaProducto;
            $create->producto_id = $producto->id;
            $create->categoria_id = intval($categoria1);
            $create->save();
        }

        // CATEGORÍAS NIVEL 2
        $categorias_seleccionadas_nivel_2 = $request->input('categoriasNivel2', []);

        foreach ($categorias_seleccionadas_nivel_2 as $categoria2) {
            $create = new CategoriaProducto;
            $create->producto_id = $producto->id;
            $create->categoria_id = intval($categoria2);
            $create->save();
        }


        $createdProductId = $producto->id;

        // IMAGEN
        $imagePosition = 1;

        if ($request->hasFile('image')) {
            foreach ($request->file('image') as $key) {
                if ($key->isValid()) {
                    $requestData = [
                        'idProduct' => $createdProductId,
                        'imageProductPosition' => $imagePosition,
                        'token' => Auth::user()->token,
                        'tokenSrv' => $this->aes_encrypt(Auth::user()->token),
                    ];

                    Http::attach('image', file_get_contents($key->path()), $key->getClientOriginalName())
                        ->post(env('API_IMAGE'), $requestData);

                    $imagePosition++;
                }
            }
        }

        return back()->with(['message' => 'El producto se ha actualizado correctamente.'])->withInput();
    }

    public function storeCustomize(Request $request, $shop_name)
    {
        $shop = Store::where('shop_name', $shop_name)->first();

        $photo = $request->file('image');

        if ($photo) {
            $request->validate(
                [
                    'image' => 'required|image|mimes:png,jpg,jpeg|max:2048'
                ]
            );

            $UserPhotoID = 'img/store/' . Auth::user()->id . ".jpg";
            $shop->logo = $UserPhotoID;
            $shop->save();

            $photo->move(public_path('img/store'), $UserPhotoID);
        }

        $title = $request->input('titleStore');

        if ($title === null) {
            $shop->title = "Bienvenido a nuestra tienda!";
            $shop->save();
        }

        if ($shop->title != $title) {
            $request->validate(
                [
                    'titleStore' => 'max:35',
                ]
            );
            $shop->title = $title;
            $shop->save();
        }

        return back()->with(['confirmation' => 'Se han actualizado tus datos correctamente.']);
    }

    public function manageHide($id)
    {
        $store = new Store();

        $producto = Producto::find($id);

        // COMPROBAR SI EL PRODCUTO EXISTE
        if ($producto === null) {
            return back();
        }
        // COMPROBAR SI EL PRODUCTO EXISTENTE ES TUYO
        $store = Store::find($producto->store_id);
        if ($store->user_id != Auth::user()->id) {
            return back();
        }

        if ($producto->disponible) {
            $store->deshabilitar($producto);
            return back()->with(['hide' => 'Se ha deshabilitado el producto "' . $producto->titulo . '" correctamente.']);
        } else {
            $store->habilitar($producto);
            return back()->with(['hide' => 'Se ha habilitado el producto "' . $producto->titulo . '" correctamente.']);
        }
    }

    public function manageEdit($id)
    {

        $producto = Producto::find($id);

        // COMPROBAR SI EL PRODCUTO EXISTE
        if ($producto === null) {
            return back();
        }
        // COMPROBAR SI EL PRODUCTO EXISTENTE ES TUYO
        $store = Store::find($producto->store_id);
        if ($store->user_id != Auth::user()->id) {
            return back();
        }

        $todasLasCategoriasNivel1 = Categoria::where('nivel', 1)->get();
        $todasLasCategoriasNivel2 = Categoria::where('nivel', 2)->get();

        $categoriasDelProducto = $producto->categorias()->get();

        return view('store.edit', ['producto' => $producto, 'todasLasCategoriasNivel1' => $todasLasCategoriasNivel1, 'todasLasCategoriasNivel2' => $todasLasCategoriasNivel2, 'categoriasDelProducto' => $categoriasDelProducto, 'shop' => $store]);
    }

    public function manageDelete($id)
    {
        $producto = Producto::find($id);

        // COMPROBAR SI EL PRODCUTO EXISTE
        if ($producto === null) {
            return back();
        }
        // COMPROBAR SI EL PRODUCTO EXISTENTE ES TUYO
        $store = Store::find($producto->store_id);
        if ($store->user_id != Auth::user()->id) {
            return back();
        }

        $producto = Producto::find($id);
        // Enviar una solicitud DELETE al servidor API de imágenes para eliminar la imagen
        $requestData = [
            'idProduct' => $producto->id,
            'token' => Auth::user()->token,
            'tokenSrv' => $this->aes_encrypt(Auth::user()->token),
        ];

        $url = env('API_IMAGE') . $producto->id;

        Http::withHeaders([
            'Accept' => 'application/json',
        ])->delete($url, $requestData);

        // Eliminar el producto de la base de datos
        $producto->delete();

        return back()->with(['delete' => 'Se ha borrado el producto "' . $producto->titulo . '" correctamente.']);
    }

    public function storeProductEdit(Request $request, $id)
    {
        $request->validate(
            [
                'title' => 'required|max:50|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ.\'"\s\-()]+$/u',
                'description' => 'required|max:255',
                'price' => 'required|integer|min:1|max:50000',
            ]
        );

        // IMAGEN
        $imagePosition = 1;

        if ($request->hasFile('image')) {
            foreach ($request->file('image') as $key) {
                if ($key->isValid()) {
                    $requestData = [
                        'idProduct' => $id,
                        'imageProductPosition' => $imagePosition,
                        'token' => Auth::user()->token,
                        'tokenSrv' => $this->aes_encrypt(Auth::user()->token),
                    ];

                    Http::attach('image', file_get_contents($key->path()), $key->getClientOriginalName())
                        ->post(env('API_IMAGE'), $requestData);

                    $imagePosition++;
                }
            }
        }

        $producto = Producto::find($id);

        // CATEGORÍAS NIVEL 1
        $categorias_seleccionadas_nivel_1 = $request->input('categoriasNivel1', []);

        $categorias_del_producto = CategoriaProducto::where('producto_id', $id)->get();

        foreach ($categorias_del_producto as $key => $categoria) {
            $delete = CategoriaProducto::where('id', $categoria->id)->first();
            $delete->delete();
        }

        foreach ($categorias_seleccionadas_nivel_1 as $key => $categoria) {
            $create = new CategoriaProducto;
            $create->producto_id = $id;
            $create->categoria_id = intval($categoria);
            $create->save();
        }

        // CATEGORÍAS NIVEL 2
        $categorias_seleccionadas_nivel_2 = $request->input('categoriasNivel2', []);

        $categorias_del_producto = CategoriaProducto::where('producto_id', $id)->get();

        foreach ($categorias_seleccionadas_nivel_2 as $key => $categoria) {
            $create = new CategoriaProducto;
            $create->producto_id = $id;
            $create->categoria_id = intval($categoria);
            $create->save();
        }


        $titulo_del_producto = $request->input('title');
        $descripcion_del_producto = $request->input('description');
        $precio_del_producto =  $request->input('price');

        $producto->titulo = $titulo_del_producto;
        $producto->descripcion = $descripcion_del_producto;
        $producto->precio = $precio_del_producto;

        $producto->save();

        return back()->with(['message' => 'El producto se ha actualizado correctamente.'])->withInput();
    }
    public function confirmarPedidos()
    {

        $pedidos_sinConfirmar = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->where('carritos.pagado', '=', 1)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->where('lineas_carritos.producto_pagado', '=', false)
            ->where('lineas_carritos.producto_enviado', '=', false)
            ->where('lineas_carritos.recibido_vendedor', '=', false)
            ->select('pedidos.*')
            ->distinct()
            ->get();
        $informacion_pedidosSinConfirmar = array();
        foreach ($pedidos_sinConfirmar as $key => $pedido) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('carritos.pagado', '=', true)
                ->where('lineas_carritos.producto_pagado', '=', false)
                ->where('lineas_carritos.producto_enviado', '=', false)
                ->where('lineas_carritos.recibido_vendedor', '=', false)
                ->where('pedidos.id', '=', $pedido->id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();
            $suma = $productos->sum('precio');
            $idComprador = Carrito::where('usuario_id', $pedido->usuario_id)->first()->id;
            $user = User::where('id', $idComprador)->first();
            $informacion = [
                'id' => $pedido->id,
                'username' => $user->username,
                'fecha_de_compra' => $pedido->fecha_compra,
                'suma_total' => $suma
            ];
            array_push($informacion_pedidosSinConfirmar, $informacion);
        }

        $pedidos_Confirmados = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->where('carritos.pagado', '=', 1)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->where('lineas_carritos.producto_pagado', '=', true)
            ->where('lineas_carritos.producto_enviado', '=', false)
            ->where('lineas_carritos.recibido_vendedor', '=', false)
            ->select('pedidos.*')
            ->distinct()
            ->get();
        $informacion_Confirmados = array();
        foreach ($pedidos_Confirmados as $key => $pedido) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('carritos.pagado', '=', true)
                ->where('lineas_carritos.producto_pagado', '=', true)
                ->where('lineas_carritos.producto_enviado', '=', false)
                ->where('lineas_carritos.recibido_vendedor', '=', false)
                ->where('pedidos.id', '=', $pedido->id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();
            $suma = $productos->sum('precio');
            $idComprador = Carrito::where('usuario_id', $pedido->usuario_id)->first()->id;
            $user = User::where('id', $idComprador)->first();
            $informacion = [
                'id' => $pedido->id,
                'username' => $user->username,
                'fecha_de_compra' => $pedido->fecha_compra,
                'suma_total' => $suma
            ];
            array_push($informacion_Confirmados, $informacion);
        }





        $pedidos_Enviado = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->where('carritos.pagado', '=', 1)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->where('lineas_carritos.producto_pagado', '=', true)
            ->where('lineas_carritos.producto_enviado', '=', true)
            ->where('lineas_carritos.recibido_vendedor', '=', false)
            ->select('pedidos.*')
            ->distinct()
            ->get();

        $informacion_Enviado = array();
        foreach ($pedidos_Enviado as $key => $pedido) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('carritos.pagado', '=', true)
                ->where('lineas_carritos.producto_pagado', '=', true)
                ->where('lineas_carritos.producto_enviado', '=', true)
                ->where('lineas_carritos.recibido_vendedor', '=', false)
                ->where('pedidos.id', '=', $pedido->id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();
            $suma = $productos->sum('precio');
            $idComprador = Carrito::where('usuario_id', $pedido->usuario_id)->first()->id;
            $user = User::where('id', $idComprador)->first();
            $informacion = [
                'id' => $pedido->id,
                'username' => $user->username,
                'fecha_de_compra' => $pedido->fecha_compra,
                'suma_total' => $suma
            ];
            array_push($informacion_Enviado, $informacion);
        }

        $pedidos_Recibido = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->where('carritos.pagado', '=', 1)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->where('lineas_carritos.producto_pagado', '=', true)
            ->where('lineas_carritos.producto_enviado', '=', true)
            ->where('lineas_carritos.recibido_vendedor', '=', true)
            ->select('pedidos.*')
            ->distinct()
            ->get();
        $informacion_Recibido = array();
        foreach ($pedidos_Recibido as $key => $pedido) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*', 'pedidos.recibido')
                ->where('carritos.pagado', '=', true)
                ->where('lineas_carritos.producto_pagado', '=', true)
                ->where('lineas_carritos.producto_enviado', '=', true)
                ->where('lineas_carritos.recibido_vendedor', '=', true)
                ->where('pedidos.id', '=', $pedido->id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();
            $suma = $productos->sum('precio');
            $idComprador = Carrito::where('usuario_id', $pedido->usuario_id)->first()->id;
            $user = User::where('id', $idComprador)->first();

            $informacion = [
                'id' => $pedido->id,
                'username' => $user->username,
                'fecha_de_compra' => $pedido->fecha_compra,
                'suma_total' => $suma,
                'recibido' => $productos[0]->recibido
            ];
            array_push($informacion_Recibido, $informacion);
        }
        return view('store.confirmation', ['info' => $informacion_pedidosSinConfirmar, 'info_confirmada' => $informacion_Confirmados, 'informacion_Enviado' => $informacion_Enviado,  'informacion_Recibido' => $informacion_Recibido]);
    }

    public function confirmProduct(Request $request)
    {
        
        $productos = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->select('productos.*')
            ->where('carritos.pagado', '=', true)
            ->where('lineas_carritos.producto_pagado', '=', false)
            ->where('pedidos.id', '=', $request->id)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->get();
        $pedido = Pedido::where('id', $request->id)->first();
        foreach ($productos as $producto) {

            $product = LineasCarrito::where('carrito_id', $pedido->carrito_id)->where('producto_id', $producto->id)->first();
            $product->producto_pagado = true;
            $product->save();
        }
        $chatUsuario = ChatUsuario::where('id_vendedor', Auth::user()->id)->where('id_comprador', $pedido->usuario_id)->first();
        $chat = new Chat;
        $chat->id_chat = $chatUsuario->id;
        $chat->enviado_por = Auth::user()->id;
        $chat->recibido_por = $pedido->usuario_id;
        $chat->mensaje = "Hemos recibido el pago y lo hemos confirmado, en breves continuaremos con el envio, tenga paciencia y le mantendremos informado";
        $chat->leido = false;
        $chat->save();
        $chatUsuario->touch();
    }


    public function confirmarEnvioPedido($id)
    {

        $productos = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->select('productos.*')
            ->where('carritos.pagado', '=', true)
            ->where('lineas_carritos.producto_pagado', '=', true)
            ->where('pedidos.id', '=', $id)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->get();
        $pedido = Pedido::where('id', $id)->first();

        foreach ($productos as $producto) {
            $product = LineasCarrito::where('carrito_id', $pedido->carrito_id)->where('producto_id', $producto->id)->first();
            $product->producto_enviado = true;
            $product->save();
        }
        $chatUsuario = ChatUsuario::where('id_vendedor', Auth::user()->id)->where('id_comprador', $pedido->usuario_id)->first();
        $chat = new Chat;
        $chat->id_chat = $chatUsuario->id;
        $chat->enviado_por = Auth::user()->id;
        $chat->recibido_por = $pedido->usuario_id;
        $chat->mensaje = "Hemos enviado su pedido, por favor aguarde";
        $chat->leido = false;
        $chat->save();
        $chatUsuario->touch();
        return back();
    }
    public function confirmarLlegadaPedido($id)
    {

        $productos = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->join('stores', 'stores.id', '=', 'productos.store_id')
            ->select('productos.*')
            ->where('carritos.pagado', '=', true)
            ->where('lineas_carritos.producto_pagado', '=', true)
            ->where('lineas_carritos.producto_enviado', '=', true)
            ->where('pedidos.id', '=', $id)
            ->where('stores.user_id', '=', Auth::user()->id)
            ->get();
        $pedido = Pedido::where('id', $id)->first();

        foreach ($productos as $producto) {
            $product = LineasCarrito::where('carrito_id', $pedido->carrito_id)->where('producto_id', $producto->id)->first();
            $product->recibido_vendedor = true;
            $product->save();
        }
        $chatUsuario = ChatUsuario::where('id_vendedor', Auth::user()->id)->where('id_comprador', $pedido->usuario_id)->first();
        $chat = new Chat;
        $chat->id_chat = $chatUsuario->id;
        $chat->enviado_por = Auth::user()->id;
        $chat->recibido_por = $pedido->usuario_id;
        $chat->mensaje = "Desde el equipo de envio nos han informado que has recibido el paquete";
        $chat->leido = false;
        $chat->save();
        $chatUsuario->touch();
        return back();
    }


    public function detallesPedidoVendedor($id)
    {
        $pedido = Pedido::find($id);
        if ($pedido == null) {
            return redirect(Route('errorGeneral.showErrorGeneral'));
        }
        $chat = ChatUsuario::where('id_vendedor', Auth::user()->id)->where('id_pedido', $id)->first();

        if ($chat != null) {
            $confirmado = false;
            $productos_Confirmados = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('carritos.pagado', '=', true)
                ->where('lineas_carritos.producto_pagado', '=', true)
                ->where('pedidos.id', '=', $id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();
            if (!$productos_Confirmados->isEmpty()) {
                $confirmado = true;
            }

            $enviado = false;
            $productos_Enviados = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('carritos.pagado', '=', true)
                ->where('lineas_carritos.producto_enviado', '=', true)
                ->where('pedidos.id', '=', $id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();

            if (!$productos_Enviados->isEmpty()) {
                $enviado = true;
            }
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('pedidos.id', '=', $id)
                ->where('stores.user_id', '=', Auth::user()->id)
                ->get();
            $suma = $productos->sum('precio');
            $user = User::find($pedido->usuario_id);

            $mensajesNoLeidos = Chat::join('chat_usuarios', 'chats.id_chat', '=', 'chat_usuarios.id')
                ->where('chat_usuarios.id_vendedor', Auth::user()->id)
                ->where('chat_usuarios.id_pedido', $id)
                ->where('chats.recibido_por', Auth::user()->id)
                ->where('chats.leido', false)
                ->count();
            $chat = ChatUsuario::where('id_pedido', $id)->where('id_vendedor', Auth::user()->id)->first();


            return view('store.pedidoDetalle', ['productos' => $productos, 'user' => $user, 'suma' => $suma, 'confirmado' => $confirmado, 'enviado' => $enviado, 'pedido' => $pedido, 'mensajesNoLeidos' => $mensajesNoLeidos, 'chat' => $chat]);
        } else {
            return redirect(Route('errorGeneral.showErrorGeneral'));
        }
    }

    public function pedidosUser()
    {
        $pedidos_Confirmados = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->where('carritos.pagado', '=', 1)
            ->where('pedidos.recibido', '=', false)
            ->where('pedidos.usuario_id', '=', Auth::user()->id)
            ->select('pedidos.*')
            ->distinct()
            ->get();

        $informacion_Confirmados = array();
        foreach ($pedidos_Confirmados as $key => $pedido) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('carritos.pagado', '=', true)
                ->where('pedidos.id', '=', $pedido->id)
                ->get();
            $suma = $productos->sum('precio');
            $idComprador = Carrito::where('usuario_id', $pedido->usuario_id)->first()->id;
            $user = User::where('id', $idComprador)->first();
            $informacion = [
                'id' => $pedido->id,
                'username' => $user->username,
                'fecha_de_compra' => $pedido->fecha_compra,
                'suma_total' => $suma
            ];
            array_push($informacion_Confirmados, $informacion);
        }



        $pedidos_Recibidos = DB::table('pedidos')
            ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
            ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
            ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
            ->where('carritos.pagado', '=', 1)
            ->where('lineas_carritos.recibido_vendedor', '=', true)
            ->where('pedidos.recibido', '=', true)
            ->where('pedidos.usuario_id', '=', Auth::user()->id)
            ->select('pedidos.*')
            ->distinct()
            ->get();
        $informacion_Recibida = array();
        foreach ($pedidos_Recibidos as $key => $pedido) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('productos.*')
                ->where('pedidos.id', '=', $pedido->id)
                ->get();
            $suma = $productos->sum('precio');
            $idComprador = Carrito::where('usuario_id', $pedido->usuario_id)->first()->id;
            $user = User::where('id', $idComprador)->first();
            $informacion = [
                'id' => $pedido->id,
                'username' => $user->username,
                'fecha_de_compra' => $pedido->fecha_compra,
                'suma_total' => $suma
            ];
            array_push($informacion_Recibida, $informacion);
        }
        return view('users.pedidos', ['info' => array_reverse($informacion_Confirmados), 'infoRecibida' => array_reverse($informacion_Recibida)]);
    }

    public function detallesPedidoUser($id)
    {
        $pedido = Pedido::find($id);

        if ($pedido != null && $pedido->usuario_id == Auth::user()->id) {
            $productos = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'productos.store_id', '=', 'stores.id')
                ->join('users', 'stores.user_id', '=', 'users.id')
                ->select('productos.*', 'lineas_carritos.*', 'users.*', 'stores.*')
                ->where('pedidos.id', '=', $id)
                ->orderBy('users.username')
                ->get();

            $vendedores = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'productos.store_id', '=', 'stores.id')
                ->join('users', 'stores.user_id', '=', 'users.id')
                ->select('users.*', 'stores.*')
                ->where('pedidos.id', '=', $id)
                ->orderBy('users.username')
                ->distinct()
                ->get();
            $chats = [];
            $mensajesNoLeidos = [];
            foreach ($vendedores as $vendedor) {
                $chat = ChatUsuario::where('id_vendedor', $vendedor->user_id)
                    ->where('id_comprador', Auth::user()->id)
                    ->where('id_pedido', $id)
                    ->first();
                $noLeidos = Chat::join('chat_usuarios', 'chats.id_chat', '=', 'chat_usuarios.id')
                    ->where('chat_usuarios.id_vendedor', $vendedor->user_id)
                    ->where('chat_usuarios.id_pedido', $id)
                    ->where('chats.recibido_por', Auth::user()->id)
                    ->where('chats.leido', false)
                    ->count();

                array_push($mensajesNoLeidos, $noLeidos);
                array_push($chats, $chat);
            }


            $productos_confirmados = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->select('productos.*', 'lineas_carritos.*')
                ->where('lineas_carritos.recibido_vendedor', '=', true)
                ->where('pedidos.id', '=', $id)
                ->get();
            $suma = $productos->sum('precio');
            $user = User::find($pedido->usuario_id);
            return view('users.pedidoDetalleUser', [
                'productos' => $productos,
                'user' => $user,
                'suma' => $suma,
                'pedido' => $pedido,
                'productos_confirmados' => count($productos_confirmados),
                'vendedores' => $vendedores,
                'mensajesNoLeidos' => $mensajesNoLeidos,
                'chat' => $chats
            ]);
        } else {
            return redirect(Route('errorGeneral.showErrorGeneral'));
        }
    }
    public function detallesPedidoRecibidoUser($id)
    {
        $pedido = Pedido::find($id);
        if ($pedido->usuario_id == Auth::user()->id) {
            $pedido->recibido = true;
            $pedido->save();

            $vendedores = DB::table('pedidos')
                ->join('carritos', 'pedidos.carrito_id', '=', 'carritos.id')
                ->join('lineas_carritos', 'lineas_carritos.carrito_id', '=', 'carritos.id')
                ->join('productos', 'productos.id', '=', 'lineas_carritos.producto_id')
                ->join('stores', 'stores.id', '=', 'productos.store_id')
                ->select('stores.user_id')
                ->where('pedidos.id', '=', $pedido->id)
                ->distinct()
                ->get();
            foreach ($vendedores as $vendedor) {

                $chatUsuario = ChatUsuario::where('id_vendedor', $vendedor->user_id)->where('id_comprador',  Auth::user()->id)->first();
                $chat = new Chat;
                $chat->id_chat = $chatUsuario->id;
                $chat->enviado_por = $pedido->usuario_id;
                $chat->recibido_por = Auth::user()->id;
                $chat->mensaje = "El comprador ha confirmado la llegada del pedido, el pedido se da por completado";
                $chat->leido = false;
                $chat->save();
                $chatUsuario->touch();
            }
            return back();
        } else {
            return back()->withErrors(['error' => 'Mensaje de error']);
        }
    }

    function aes_encrypt($userToken)
    {
        $tokenSrv = env('API_TOKEN') . '|' . $userToken;
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($tokenSrv, 'aes-256-cbc', env('API_PASS'), OPENSSL_RAW_DATA, $iv);
        $msgForCrypt = base64_encode($iv . $encrypted);

        // $this->aes_decrypt($msgForCrypt, env('API_PASS'));

        return base64_encode($iv . $encrypted);
    }
}
