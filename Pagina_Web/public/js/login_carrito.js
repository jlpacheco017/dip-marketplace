const recuentoDeProductos = () => {
    if (!(localStorage.getItem('carrito') === null)) {
        const count = JSON.parse(localStorage.getItem('carrito')).length;
        document.getElementById("carrito_numero").innerText = count;
    }
};
const carritoLocal = JSON.parse(localStorage.getItem("carrito")) || [];
const ids = carritoLocal.map(obj => parseInt(obj.id));
if(document.getElementById("local_carrito")){
   document.getElementById("local_carrito").value = ids; 
}
recuentoDeProductos();

