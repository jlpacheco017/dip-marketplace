@extends('layouts.master')

@section('title', 'DIP Marketplace - EDITAR PERFIL')

@section('content')

<div id="login-container">
    <form action="{{ route('user.edit', Auth::user()->username) }}" id="auth-form" method="post" enctype="multipart/form-data">
        @csrf
        <h4>EDITAR MI PERFIL</h4>
        <div class="form-group">
            <details>
                <summary><i class='bx bxs-edit'></i> Foto de perfil <i class='bx bx-chevron-down'></i></summary>
                <div id="photo-container">
                    <img id="photo" src="{{ asset('img/photo/' . Auth::user()->photo) }}" alt="" class="photo">
                </div>
                <input id="inputFile" type="file" name="imagen" accept="image/png, image/gif, image/jpeg, image/jpg">
                <label for="imagen">Max 2MB.</label>
        </div>
        </details>
        <div class="form-group">
            <details>
                <summary><i class='bx bxs-edit'></i> Nombre de usuario <i class='bx bx-chevron-down'></i></summary>
                <label for="username">Nombre de usuario:</label>
                <input type="text" name="username" id="username" value="{{ Auth::user()->username }}">
                <div class="feedback_editUser">
                    <p hidden id="characters" class="text-p-small errorpass"> ● No puede contener caracteres especiales y espacios.</p>
                </div>
            </details>
        </div>
        <div class="form-group">
            <details>
                <summary><i class='bx bxs-edit'></i> Contraseña <i class='bx bx-chevron-down'></i></summary>
                <label for="current_password">Contraseña actual:</label>
                <input type="password" name="current_password" id="current_password">
                <p hidden id="current_lenght" class="text-p-small">● Debe contener almenos 8 caracteres</p>
                <p hidden id="current_mayuscula" class="text-p-small">● Una letra en mayúscula</p>
                <p hidden id="current_minuscula" class="text-p-small">● Una letra en minúscula</p>
                <p hidden id="current_numero" class="text-p-small"> ● Un numero</p>
                <x-pass />
            </details>
        </div>

        @if(session('confirmation'))
        <div class="alert alert-success">
            {{ session('confirmation') }}
        </div>
        @endif
        @if ($errors->has('imagen'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('imagen') }}
        </div>
        @endif
        @if ($errors->has('username'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('username') }}
        </div>
        @endif
        @if ($errors->has('current_password'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('current_password') }}
        </div>
        @endif
        @if ($errors->has('password'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('password') }}
        </div>
        @endif
        @if ($errors->has('password_confirmation'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('password_confirmation') }}
        </div>
        @endif
        <div class="form-group">
            <button type="submit">Actualizar perfil</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script src="/js/header.js"></script>
<script src="/js/editUserCheck.js"></script>
<script src="/js/passwordCheck.js"></script>
@endsection
