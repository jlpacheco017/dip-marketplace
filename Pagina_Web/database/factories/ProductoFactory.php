<?php

namespace Database\Factories;

use App\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Producto>
 */
class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $tienda = Store::all();
        return [
            'img' => 'null',
            'titulo' => fake()->name(),
            'descripcion' => fake()->paragraph(1),
            'precio' => fake()->numberBetween(0, 999),
            'store_id' => $tienda->random()->id,
        ];
    }
}
