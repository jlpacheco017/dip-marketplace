const obtenerPasswordEditUser = () => {

    const inputPasswordEditUser = document.querySelector('#current_password');
    return inputPasswordEditUser.value;
}

validarCurrentPassword = () => {
    console.log(obtenerPasswordEditUser());
    const inputPasswordEditUser = obtenerPasswordEditUser();

    const patternUpper = /(?=.*[A-Z])/
    const patternMinus = /(?=.*[a-z])/
    const patternDigit = /(?=.*[0-9])/

    const lenghtText = document.querySelector('#current_lenght');
    const upperText = document.querySelector('#current_mayuscula');
    const minusText = document.querySelector('#current_minuscula');
    const digitText = document.querySelector('#current_numero');

    if (inputPasswordEditUser.length === 0) {
        upperText.setAttribute("hidden", false);
        minusText.setAttribute("hidden", false);
        digitText.setAttribute("hidden", false);
        lenghtText.setAttribute("hidden", false);
    } else {
        upperText.removeAttribute("hidden");
        minusText.removeAttribute("hidden");
        digitText.removeAttribute("hidden");
        lenghtText.removeAttribute("hidden");
    }

    // CHECK LENGTH
    if (inputPasswordEditUser.length > 7) {
        lenghtText.classList.remove("errorpass");
        lenghtText.classList.add("validated");
    } else {
        lenghtText.classList.remove("validated");
        lenghtText.classList.add("errorpass");
    }

    // CHECK MAYUS
    if (inputPasswordEditUser.match(patternUpper)) {
        upperText.classList.remove("errorpass");
        upperText.classList.add("validated");
    } else {
        upperText.classList.remove("validated");
        upperText.classList.add("errorpass");
    }

    // CHECK MINUS
    if (inputPasswordEditUser.match(patternMinus)) {
        minusText.classList.remove("errorpass");
        minusText.classList.add("validated");
    } else {
        minusText.classList.remove("validated");
        minusText.classList.add("errorpass");
    }

    // CHECK DIGIT
    if (inputPasswordEditUser.match(patternDigit)) {
        digitText.classList.remove("errorpass");
        digitText.classList.add("validated");
    } else {
        digitText.classList.remove("validated");
        digitText.classList.add("errorpass");
    }
}

if (document.querySelector('#current_password')) {
    const inputPasswordEditUser = document.querySelector('#current_password');

    inputPasswordEditUser.addEventListener('input', () => {
        validarCurrentPassword();
    });
}

if (document.querySelector('#inputFile')) {
    const img = document.querySelector('#photo');
    const inputFile = document.querySelector('#inputFile');
    inputFile.onchange = (e) => {
        if (inputFile.files[0]) {
            img.src = URL.createObjectURL(inputFile.files[0]);
        }
    }

}