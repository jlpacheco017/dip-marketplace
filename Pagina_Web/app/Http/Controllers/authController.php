<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Store;
use App\Models\Carrito;
use App\Models\Producto;
use Illuminate\Support\Str;
use App\Models\LineaCarrito;
use Illuminate\Http\Request;
use App\Models\LineasCarrito;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class authController extends Controller
{
    /**
     * Muestra la vista de la página de Login (login.blade.php).
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS.
     * @return View Devuelve la vista del login (login.blade.php) para el usuario.
     */
    public function showLogin(Request $request)
    {
        if (auth()->check()) {
            Log::debug($request->ip() . ' Usuario = ' . Auth::user()->id . ' redirigido al home por estar logeado -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);

            return redirect(Route('landing.index'));
        } else {
            Log::debug($request->ip() . ' Ha accedido a la vista Login por no estar logeado -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
            return view('auth.login');
        }
    }

    /**
     * Cierra la sesión y nos redirije a la vista login (login.blade.php)
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS.
     * @return View Devuelve la vista del login (login.blade.php) para el usuario.
     */
    public function logOut(Request $request)
    {
        Log::debug($request->ip() . ' Usuario = ' . Auth::user()->id . ' ha hecho logout -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);
        Auth::logout();
        return redirect(Route('auth.showLogin'));
    }

    /**
     * Valida los campos 'username' y 'password' y iniciar sesión inicializando el carrito con todos 
     * los productos que tenias en el carrito pasandolos al servidor.
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS y para recoger
     * el 'username' y 'password'.
     * @return View Devuelve la vista del landing (landing.blade.php) para el usuario en caso de que sea correcto,
     * si no redirige al login.
     */
    public function doLogin(Request $request)
    {
        $objUser = new User();
        $objCarrito = new Carrito();
        $objLineasCarrito = new LineasCarrito();
        $objProducto = new Producto;
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $login = $request->input('email');
        $password = $request->input('password');
        $remember = $request->input('remember');
        $user = Auth::attempt([
            'email' => $login,
            'password' => $password,
        ], $remember) || Auth::attempt([
            'username' => $login,
            'password' => $password,
        ], $remember);

        if ($user) {

            $user = $objUser->getUsersPorCorreo($request->email);
            if (!isset($user)) {
                $user = User::where('username', $request->email)->first();
            }
            if (filter_var($login, FILTER_VALIDATE_EMAIL)) {
                session()->put('userLogeado', $user->username);
                session()->put('userLogeadoId', $user->id);
            } else {
                session()->put('userLogeado', $login);
            }

            $carrito_user = $objCarrito->getCarritoActivoPorUsuario($user->id);
            if (!isset($carrito_user)) {
                $objCarrito->usuario_id = $user->id;
                $objCarrito->save();
            }

            $carrito_select = $objCarrito->getCarritoActivoPorUsuario($user->id);
            $carritoServidor = $objLineasCarrito->getLineaCarritoPorCarritoId($carrito_select->id);
            $arrayCarritoServidor = null;

            foreach ($carritoServidor as $key => $value) {

                if (is_array($arrayCarritoServidor)) {
                    array_push($arrayCarritoServidor, $value->producto_id);
                } else {
                    $arrayCarritoServidor = [$value->producto_id];
                }
            }

            $arrayCarritoLocal = explode(',', $request->local_carrito);
            if ($arrayCarritoLocal[0] !== "") {
                $store = Store::where('user_id', Auth::user()->id)->first();
                if ($store) {
                    $productos_Store = Producto::where('store_id', $store->id)->get()->pluck('id')->toArray();
                }
                if (isset($arrayCarritoServidor)) {
                    $noRepetidos = array_diff($arrayCarritoLocal, $arrayCarritoServidor);
                    foreach ($noRepetidos as $key => $value) {
                        if (in_array($value, $productos_Store)) {
                        } else {

                            $producto = $objProducto->getProductoPorId($value);
                            $objLineasCarrito = new LineasCarrito();
                            $objLineasCarrito->carrito_id = $carrito_select->id;
                            $objLineasCarrito->producto_id = $producto->id;
                            $objLineasCarrito->precio = $producto->precio;
                            $objLineasCarrito->save();
                        }
                    }
                } else {
                    foreach ($arrayCarritoLocal as $key => $value) {
                        if (in_array($value, $productos_Store)) {
                        } else {

                            $producto = $objProducto->getProductoPorId($value);
                            $objLineasCarrito = new LineasCarrito();
                            $objLineasCarrito->carrito_id = $carrito_select->id;
                            $objLineasCarrito->producto_id = $producto->id;
                            $objLineasCarrito->precio = $producto->precio;
                            $objLineasCarrito->save();
                        }
                    }
                }
            }

            if (env('SEND_MAIL') == true) {

                Mail::send('email.inicioSesion', ['username' => $user, 'request' => $request], function ($message) use ($user, $request) {
                    $message->to($user->email, $request->ip());
                    $message->subject('Inicio de Sesión');
                });
            }
            Log::debug($request->ip() . ' Usuario = ' . Auth::user()->id . ' se ha logeado y se han sincronizado los carritos -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);

            $request->session()->regenerate();
            $user->token = $this->tokenGenerator();
            $user->save();

            return redirect()->intended(route('landing.index'));
        } else {
            return back()->withErrors(['login' => 'El Usuario/Email o contraseña son incorrectos.'])->withInput();
        }
    }

    /**
     * Muestra la vista de la página de Register (register.blade.php).
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS.
     * @return View Devuelve la vista del register (register.blade.php) para el usuario.
     */
    public function showRegister(Request $request)
    {

        Log::debug($request->ip() . ' Ha entrado en la vista del Registro -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);

        return view('auth.register');
    }
    /**
     * Valida los campos 'username', 'email', 'password' y 'password_confirmation' y registra un usuario nuevo y 
     * le manda al correo 3 productos al azar al usuario.
     *
     * @param Request El objeto Request es solo utilizado para guardar datos en los LOGS y para recoger
     * el 'username', 'email', 'password' y 'password_confirmation'.
     * @return View Devuelve la vista del login (login.blade.php) para el usuario en caso de que sea correcto,
     * si no redirige al register.
     */
    public function doRegister(Request $request)
    {
        $request->validate(
            [
                'username' => 'required|max:20|unique:users|regex:/^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]+$/u',
                'email' => 'required|email|unique:users',
                'password' => [
                    'required',
                    'min:8',
                    'regex:/^(?=.*[a-z])(?=.*[A-Z]).+$/'
                ],
                'password_confirmation' => 'required|same:password'
            ]
        );

        $data = $request->all();
        User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
        $user = $data['username'];
        $productos = Producto::inRandomOrder()->take(3)->get();
        if (env('SEND_MAIL') == true) {

            Mail::send('email.registerMail', ['username' => $user, 'productos' => $productos], function ($message) use ($user, $data) {
                $message->to($data['email']); // Reemplaza con la dirección de correo electrónico del destinatario
                $message->subject('Registro');
            });
        }
        Log::debug($request->ip() . 'Se ha creado el usuario= ' . $data['username'] . ' -> ' . class_basename(Route::current()->getController()) . '@' . __FUNCTION__);

        return redirect()->route('auth.showLogin')->with('message', 'El usuario se ha creado correctamente!');
    }

    public function tokenGenerator()
    {
        $randomString = Str::random(40);
        $randomString = preg_replace('/[^a-zA-Z0-9]/', '', $randomString);

        while (strlen($randomString) < 40) {
            $randomString .= Str::random(40 - strlen($randomString));
            $randomString = preg_replace('/[^a-zA-Z0-9]/', '', $randomString);
        }
        return $randomString;
    }
}
