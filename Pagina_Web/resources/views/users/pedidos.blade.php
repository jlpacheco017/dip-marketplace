@extends('layouts.master')

@section('title', 'DIP Marketplace - Confirmar Pedidos')

@section('content')

@endsection

<div class="pedidos">
    <h1 class="h1Title">Lista de Pedidos en Curso</h1>
    <div class="titulos superior">
        <p><b>Identificador</b></p>
        <p><b>Fecha de compra</b></p>
        <p><b>Total del pedido</b></p>
        <p><b>Detalles</b></p>
    </div>
    @if ($info)

        @foreach ($info as $item)
            <div id="{{ $item['id'] }}" class="titulos">
                <p>{{ $item['id'] }}</p>
                <p>{{ $item['fecha_de_compra'] }}</p>
                <p style="color: #4E23D6;">{{ $item['suma_total'] }}€</p>
                <a href="{{ route('pedido.detallesPedidoUser', $item['id']) }}"
                    class="btn_remove btn_confirm">Detalles</a>
            </div>

        @endforeach
    @else
        <h5 class="noConfirmadosMensaje">No Hay Pedidos en Curso</h5>
    @endif

    <h1 class="Pedidos_Confirmados_Title h1Title">Lista de Pedidos Recibidos</h1>
    <div class="titulos superior">
        <p><b>Identidicador</b></p>
        <p><b>Fecha de compra</b></p>
        <p><b>Total del pedido</b></p>
        <p><b>Detalles</b></p>
    </div>
    @if ($infoRecibida)

        @foreach ($infoRecibida as $item)
            <div id="{{ $item['id'] }}" class="titulos">
                <p>{{ $item['id'] }}</p>
                <p>{{ $item['fecha_de_compra'] }}</p>
                <p style="color: #4E23D6;">{{ $item['suma_total'] }}€</p>
                <a href="{{ route('pedido.detallesPedidoUser', $item['id']) }}"
                    class="btn_remove btn_confirm">Detalles</a>
            </div>

        @endforeach
    @else
        <h5 class="noConfirmadosMensaje">No Hay Pedidos Terminados</h5>
    @endif

</div>

@section('js')
    <script src="/js/header.js"></script>
    <script src="/js/confirm.js"></script>
@endsection
