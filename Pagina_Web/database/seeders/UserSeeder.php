<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $usuarios = [
            ['username' => 'jlpacheco', 'email' => 'jlpacheco@gmail.com', 'password' => Hash::make('1234')],
            ['username' => 'dfernandez', 'email' => 'diego@test.com', 'password' => Hash::make('1234')],
            ['username' => 'ireyes', 'email' => 'ivan@test.com', 'password' => Hash::make('1234')],
            ['username' => 'comprador', 'email' => 'comprador@test.com', 'password' => Hash::make('12345678')],
            ['username' => 'venedor', 'email' => 'venedor@test.com', 'password' => Hash::make('12345678')],
        ];
        foreach ($usuarios as $usuario) {
            User::create($usuario);
        }
    }
}
