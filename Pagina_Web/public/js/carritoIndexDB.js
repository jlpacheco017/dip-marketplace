import Carrito from "./clases/Carrito.js";
import Producto from "./clases/Producto.js";

// Crear una instancia de la clase Carrito
const carrito = new Carrito();

const obtenerTodosLosProductos = () => {
    const productos = document.querySelector('.productos');
    return productos;
};

window.addEventListener('pageshow', function(event) {
  if (event.persisted) {
    window.location.reload()
  }
});

obtenerTodosLosProductos().addEventListener('click', evento => {
    if (evento.target.nodeName === 'BUTTON') {
        const id = evento.target.parentElement.parentElement.parentElement.id;
        const img = evento.target.parentElement.parentElement.parentElement.children[0].children[0].children[0].getAttribute('src');
        const titulo = evento.target.parentElement.parentElement.parentElement.children[0].children[1].children[0].textContent;
        const precio = evento.target.parentElement.parentElement.parentElement.children[0].children[1].children[1].textContent;

        const producto = new Producto(id, img, titulo, precio);

        if (evento.target.classList.contains('btn_add')) {
            fetch("/agregar-producto", {
                method: "POST",
                headers: {
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({id: id})
              });
              let contador = document.getElementById("carrito_numero").innerText;
              contador = parseInt(contador) + 1;
              document.getElementById("carrito_numero").innerText = contador;
            eliminarBoton(evento);
            
        } else {
            fetch("/rm-producto", {
                method: "DELETE",
                headers: {
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({id: id})
              });
              let contador = document.getElementById("carrito_numero").innerText;
              contador = parseInt(contador) -1;
              document.getElementById("carrito_numero").innerText = contador;
            agregarBoton(evento);
        }
    }

});

const eliminarBoton = (evento) => {
    evento.target.classList.remove('btn_add');
    evento.target.classList.add('btn_remove');
    evento.target.innerHTML = "Eliminar de la <i class='bx bxs-shopping-bag'></i>";
};

const agregarBoton = (evento) => {
    evento.target.classList.remove('btn_remove');
    evento.target.classList.add('btn_add');
    evento.target.innerHTML = "Añadir a la <i class='bx bxs-shopping-bag'></i>";
};

const carritoValue = document.getElementById('carrito-value').innerHTML;
const updateHeaerCount = document.getElementById('carrito_numero');

updateHeaerCount.innerHTML = carritoValue;






