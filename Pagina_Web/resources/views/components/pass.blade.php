<div class="form-group">
    <label for="password">Nueva contraseña: </label>
    <input type="password" id="password" name="password">
    <p hidden id="lenght" class="text-p-small">● Debe contener almenos 8 caracteres</p>
    <p hidden id="mayuscula" class="text-p-small">● Una letra en mayúscula</p>
    <p hidden id="minuscula" class="text-p-small">● Una letra en minúscula</p>
    <p hidden id="numero" class="text-p-small"> ● Un numero</p>
</div>
<div class="form-group">
    <label for="password_confirmation">Repite la Contraseña: </label>
    <input type="password" id="password_confirmation" name="password_confirmation">
    <p hidden id="passEquals" class="text-p-small"> ● N/A</p>
</div>
