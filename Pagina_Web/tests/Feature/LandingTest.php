<?php

namespace Tests\Feature;

use App\Models\Producto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LandingTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_hay_menos_de_50_productos(): void
    {   
        
        $response = $this->get(route('landing.index'));
        $response->assertSee('Lamentablemente en este momento no contamos con productos suficientes. Trabajamos arduamente para reponer nuestro inventario lo antes posible.');
    }
}
