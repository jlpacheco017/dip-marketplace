<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    public function producto()
    {
        return $this->belongsToMany(Producto::class);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUsersPorCorreo($email)
    {
        return $this->query()->where('email', $email)->first();
    }

    public function updateUserName($newName)
    {
        $this->username = $newName;
        $this->update();
    }

    public function store()
    {
        return $this->hasOne(Store::class);
    }

    public function chatsComprador()
    {
        return $this->hasMany(ChatUsuario::class, 'id_comprador');
    }

    public function chatsVendedor()
    {
        return $this->hasMany(ChatUsuario::class, 'id_vendedor');
    }

}
