const div_principal = document.querySelector('.container_carrito');

function countCart() {
    const carritoArray = JSON.parse(localStorage.getItem("carrito")) || [];
    const count = carritoArray.length;
    document.getElementById("carrito_header").innerHTML = "Mi carrito ( " + count + " ) <i class='bx bxs-shopping-bag'></i></a></li>";
}
function sumarCarrito() {
    const carritoJSON = JSON.parse(localStorage.getItem("carrito")) || [];
    let suma = 0;
    let solo_num = 0;
    for (let index = 0; index < carritoJSON.length; index++) {
        if(Number.isInteger(carritoJSON[index].precio)){
            solo_num = carritoJSON[index].precio;
        }else{
            solo_num = carritoJSON[index].precio.replace("€", "");
        }
        suma = suma + parseInt(solo_num);
    }
    const total = document.querySelector('.price_carrito');
    total.innerHTML = " " + suma + "€"
}

function elements() {
    const carritoJSON = JSON.parse(localStorage.getItem("carrito")) || [];
    let id = "";
    let titulo = "";
    let img = "";
    let precio = "";

    let codigo = "";
    let identificador = document.getElementById('carrito_local')
    if (identificador !== null) {

    for (let index = 0; index < carritoJSON.length; index++) {
        titulo = carritoJSON[index].titulo;
        img = carritoJSON[index].img;
        precio = carritoJSON[index].precio;
        id = carritoJSON[index].id;
        if(Number.isInteger(carritoJSON[index].precio)){
   
            codigo = `
        <div id="`+ id + `" class="item_container_carrito">
            <a href="/productos/show/`+id+`">
                <img class="carrito_img"src="`+ img + `" alt="">
            </a>
            
            <div>
                <a href="/productos/show/`+id+`" class="producto_detalles--titulo">`+ titulo + `</a>
            </div>
            <div>
                <p class="price_carrito">`+ precio + `€</p>
            </div>
            <div>
            <button class="btn_remove" id="btn_eliminar_producto">Eliminar producto <i class='bx bx-trash'></i></button>
            </div>
        </div>`
          }else{
            codigo = `
            <div id="`+ id + `" class="item_container_carrito">
                <a href="/productos/show/`+id+`">
                    <img class="carrito_img"src="`+ img + `" alt="">
                </a>
                
                <div>
                    <a href="/productos/show/`+id+`" class="producto_detalles--titulo">`+ titulo + `</a>
                </div>
                <div>
                    <p class="price_carrito">`+ precio + `</p>
                </div>
                <div>
                <button class="btn_remove" id="btn_eliminar_producto">Eliminar producto <i class='bx bx-trash'></i></button>
                </div>
            </div>`

          }
        
        div_principal.insertAdjacentHTML('beforeend', codigo);

    }
}

}

function vaciarCarrito() {
    localStorage.removeItem('carrito'); 
    fetch("/vaciar-carrito", {
        method: "DELETE",
      });
    elements();
}

const obtenerTodosLosBotones = () => {
    const container = document.querySelector('.container_carrito');
    return container;
};

obtenerTodosLosBotones().addEventListener('click', evento => {
    if (evento.target.nodeName === 'BUTTON') {
        let carritoArray = JSON.parse(localStorage.getItem("carrito")) || [];
        const id = evento.target.parentElement.parentElement.id;
        carritoArray = carritoArray.filter(obj => obj.id !== id);
        localStorage.setItem("carrito", JSON.stringify(carritoArray));
        fetch("/rm-producto", {
            method: "DELETE",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({id: id})
          });
        const div = document.getElementById(id);
        div.style.transition = "opacity 0.5s ease-in-out"; 
        div.style.opacity = 0; 
        setTimeout(() => { 
            div.remove();
            sumarCarrito()
            countCart();
        }, 500);
        sumarCarrito()
        countCart();
    }

});
elements();
sumarCarrito()
countCart();