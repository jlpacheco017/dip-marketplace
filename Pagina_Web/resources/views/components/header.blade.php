<header>
    @if (Auth::check())
    <a href="{{ route('landing.index') }}" class="logo"><i class='bx bxs-box'></i> DIP Marketplace</a>
    <i id="hamburger" class='bx bx-menu'></i>
    <ul class="navbar">
        <li><a href="{{ route('productos.index') }}"><i class='bx bxs-grid-alt'></i> Todos los productos </a></li>
        <li><a href="{{ route('show.carrito') }}" id="carrito_header"><i class='bx bxs-shopping-bag'></i> Mi carrito ( <span id="carrito_numero"> 0 </span> ) </a></li>
        <div class="dropdown">
            <button id="dropBtn"><i class='bx bxs-user'></i> {{ Auth::user()->username }}</button>
            <div id="myDropdown" class="dropdown-content">
                <li><a href="{{ route('user.edit', Auth::user()->username) }}"><i class='bx bx-edit-alt'></i> Editar mi cuenta</a></li>
                @if(Auth::user()->store)
                    <li><a href="{{ route('store.show', Auth::user()->store->shop_name) }}"><i class='bx bx-store-alt'></i> Visitar mi tienda</a></li>
                    <li><a href="{{ route('store.confirmarPedidos', Auth::user()->store->shop_name) }}"><i class='bx bx-store-alt'></i> Confirmar pedidos</a></li>
                @else
                    <li><a href="{{ route('store.create') }}"><i class='bx bx-plus-circle' ></i> Crear tienda</a></li>
                @endif
                <li><a href="{{ route('pedido.pedidosUser') }}" id="pedidosUser"><i class='bx bxs-package'></i> Mis Pedidos</a></li>
                <li><a href="{{ route('auth.logOut') }}" id="logout"><i class='bx bx-log-out-circle' ></i> Cerrar Sesión</a></li>
            </div>
        </div>
    </ul>
    @else
    <a href="{{ route('landing.index') }}" class="logo"><i class='bx bxs-box'></i> DIP Marketplace</a>
    <i id="hamburger" class='bx bx-menu'></i>
    <ul class="navbar">
        <li><a href="{{ route('productos.index') }}"><i class='bx bxs-grid-alt'></i> Todos los productos </a></li>
        <li><a href="{{ route('show.carrito') }}" id="carrito_header"><i class='bx bxs-shopping-bag'></i> Mi carrito ( <span id="carrito_numero"> 0 </span> ) </a></li>
        <div class="dropdown">
            <button id="dropBtn"><i class='bx bxs-user'></i> Mi cuenta</button>
            <div id="myDropdown" class="dropdown-content">
                <li><a href="{{ route('auth.showLogin') }}">Iniciar Sesión</a></li>
                <li><a href="{{ route('auth.showRegister') }}">Registrarse</a></li>
            </div>
        </div>
    </ul>
    @endif
</header>
