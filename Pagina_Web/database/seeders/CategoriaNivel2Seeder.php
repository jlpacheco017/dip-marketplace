<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class CategoriaNivel2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categoriasn2 = [
            ['titulo' => 'Ordenadores y Portatiles', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Componentes de Ordenador', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Impresoras y escáneres', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Teléfonos móviles y tablets', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Accesorios para móviles y tablets', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Televisores y proyectores', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Altavoces y sistemas de sonido', 'nivel' => 2, 'id_padre' => 1],
            ['titulo' => 'Cámaras y videocámaras', 'nivel' => 2, 'id_padre' => 1],

            ['titulo' => 'Ropa para mujeres', 'nivel' => 2, 'id_padre' => 2],
            ['titulo' => 'Ropa para hombres', 'nivel' => 2, 'id_padre' => 2],
            ['titulo' => 'Ropa para niños', 'nivel' => 2, 'id_padre' => 2],

            ['titulo' => 'Automóviles eléctricos', 'nivel' => 2, 'id_padre' => 3],
            ['titulo' => 'Automóviles híbridos', 'nivel' => 2, 'id_padre' => 3],
            ['titulo' => 'Automoviles familiares', 'nivel' => 2, 'id_padre' => 3],
            ['titulo' => 'Camionetas todoterreno (SUV)', 'nivel' => 2, 'id_padre' => 3],
            ['titulo' => 'Vans y minivans', 'nivel' => 2, 'id_padre' => 3],

           
            ['titulo' => 'Equipamiento de entrenamiento', 'nivel' => 2, 'id_padre' => 4],
            ['titulo' => 'Equipos de deportes de equipo', 'nivel' => 2, 'id_padre' => 4],
            ['titulo' => 'Accesorios de deportes', 'nivel' => 2, 'id_padre' => 4],
            ['titulo' => 'Artículos de protección deportiva', 'nivel' => 2, 'id_padre' => 4],

            ['titulo' => 'Juguetes de construcción', 'nivel' => 2, 'id_padre' => 5],
            ['titulo' => 'Juegos de mesa y de cartas', 'nivel' => 2, 'id_padre' => 5],
            ['titulo' => 'Juguetes para bebés y niños pequeños', 'nivel' => 2, 'id_padre' => 5],
            ['titulo' => 'Juegos de deportes y actividades al aire libre', 'nivel' => 2, 'id_padre' => 5],
            ['titulo' => 'Juguetes electrónicos', 'nivel' => 2, 'id_padre' => 5],

            ['titulo' => 'Cuidado de la piel', 'nivel' => 2, 'id_padre' => 6],
            ['titulo' => 'Maquillaje', 'nivel' => 2, 'id_padre' => 6],
            ['titulo' => 'Cuidado del cabello', 'nivel' => 2, 'id_padre' => 6],
            ['titulo' => 'Perfumes y fragancias', 'nivel' => 2, 'id_padre' => 6],
            ['titulo' => 'Kits de belleza y conjuntos de regalo', 'nivel' => 2, 'id_padre' => 6],

            ['titulo' => 'Guitarras', 'nivel' => 2, 'id_padre' => 7],
            ['titulo' => 'Bajos eléctricos', 'nivel' => 2, 'id_padre' => 7],
            ['titulo' => 'Baterías y percusión', 'nivel' => 2, 'id_padre' => 7],
            ['titulo' => 'Teclados y pianos digitales', 'nivel' => 2, 'id_padre' => 7],
            ['titulo' => 'Instrumentos de viento', 'nivel' => 2, 'id_padre' => 7],
            ['titulo' => 'Instrumentos de cuerda', 'nivel' => 2, 'id_padre' => 7],
            ['titulo' => 'Accesorios para instrumentos musicales', 'nivel' => 2, 'id_padre' => 7],

            ['titulo' => 'Sofás y sillones', 'nivel' => 2, 'id_padre' => 8],
            ['titulo' => 'Mesas de comedor', 'nivel' => 2, 'id_padre' => 8],
            ['titulo' => 'Camas y colchones', 'nivel' => 2, 'id_padre' => 8],
            ['titulo' => 'Armarios y cómodas', 'nivel' => 2, 'id_padre' => 8],
            ['titulo' => 'Estanterías y librerías', 'nivel' => 2, 'id_padre' => 8],
            ['titulo' => 'Escritorios y mesas de oficina', 'nivel' => 2, 'id_padre' => 8],

            ['titulo' => 'Ropa para bebés', 'nivel' => 2, 'id_padre' => 9],
            ['titulo' => 'Pañales y artículos de higiene', 'nivel' => 2, 'id_padre' => 9],
            ['titulo' => 'Alimentación del bebé', 'nivel' => 2, 'id_padre' => 9],
            ['titulo' => 'Asientos de coche para bebés', 'nivel' => 2, 'id_padre' => 9],
            ['titulo' => 'Juguetes para bebés', 'nivel' => 2, 'id_padre' => 9],
        ];
        foreach ($categoriasn2 as $categoria) {
            Categoria::create($categoria);
        }
    }
}
