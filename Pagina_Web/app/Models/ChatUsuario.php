<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatUsuario extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_vendedor',
        'id_comprador',
    ];

    public function comprador()
    {
        return $this->belongsTo(User::class, 'id_comprador');
    }

    public function vendedor()
    {
        return $this->belongsTo(User::class, 'id_vendedor');
    }
}
