const obtenerTodosLosBotones = () => {
    const productos = document.querySelector('#categorias');

    return productos;
};

obtenerTodosLosBotones().addEventListener('click', evento => {
    const inputHidden = document.querySelector('#inputHidden');

    if (evento.target.nodeName === 'BUTTON') {
        inputHidden.value = evento.target.value;
        sessionStorage.setItem('botonSeleccionado', evento.target.value);
    }
});

window.onload = function () {

    const buttons = document.querySelectorAll('.categoria');

    const urlConQueryStrings = window.location.search;
    const parametros = new URLSearchParams(urlConQueryStrings);

    const valorDeLaCategoria = parametros.get('categoria');

    buttons.forEach(button => {

        if (!valorDeLaCategoria && button.value == 0) {
            button.style.backgroundColor = '#4E23D6';
            button.style.color = '#FFF';
            button.style.fontWeight = 'bold';
        }

        if (valorDeLaCategoria === button.value) {
            button.style.backgroundColor = '#4E23D6';
            button.style.color = '#FFF';
            button.style.fontWeight = 'bold';
        }
    });
};