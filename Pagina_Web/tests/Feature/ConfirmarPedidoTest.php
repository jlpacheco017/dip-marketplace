<?php

namespace Tests\Feature;

use App\Models\Carrito;
use App\Models\LineasCarrito;
use App\Models\Pedido;
use App\Models\Producto;
use App\Models\User;
use Database\Seeders\UserSeeder;
use Database\Seeders\StoreSeeder;
use Database\Seeders\ProductoSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class ConfirmarPedidoTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_realizar_pago_y_se_envia_mensaje_al_chat(): void
    {

        $carrito = new Carrito;
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $lineas_carrito = new LineasCarrito();
        $user = User::find(1);
        $this->actingAs($user);
        
        
        $carrito -> usuario_id = $user->id;
        $carrito -> save();
        Producto::factory()->count(50)->create();
        $producto = Producto::where('store_id', 2)->first();

        
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id =$producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();
        $this->get(route('show.carrito'));
        $this->get(route('comprar.carrito'));

        $this->assertDatabaseHas('carritos', [
            'usuario_id' => $user->id,
            'pagado' => true,
        ]);

        $response = $this->get(route('user.chat', 1));
        $response->assertSee('He realizado un pedido nuevo, porfavor, revise que ha recibido el pago correctamente');
    }
    public function test_confirmar_pago_y_se_envia_mensaje_al_chat(): void
    {

        $carrito = new Carrito;
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $lineas_carrito = new LineasCarrito();
        $user = User::find(1);
        $this->actingAs($user);
        
        
        $carrito -> usuario_id = $user->id;
        $carrito -> save();
        Producto::factory()->count(50)->create();
        $producto = Producto::where('store_id', 2)->first();

        
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id =$producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();
        $this->get(route('show.carrito'));
        $this->get(route('comprar.carrito'));

        $vendedor = User::find(2);
        $this->actingAs($vendedor);

        $this->post(route('store.confirmProduct'), [
            'id' => 1
        ]);
        
        $this->assertDatabaseHas('lineas_carritos', [
            'carrito_id' => 1,
            'producto_id' => $producto->id,
            'producto_pagado' => true
        ]);

        $response = $this->get(route('user.chat', 1));
        $response->assertSee('Hemos recibido el pago y lo hemos confirmado, en breves continuaremos con el envio, tenga paciencia y le mantendremos informado');
    }

    public function test_confirmar_envio_y_se_envia_mensaje_al_chat(): void
    {

        $carrito = new Carrito;
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $lineas_carrito = new LineasCarrito();
        $user = User::find(1);
        $this->actingAs($user);
        
        
        $carrito -> usuario_id = $user->id;
        $carrito -> save();
        Producto::factory()->count(50)->create();
        $producto = Producto::where('store_id', 2)->first();

        
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id =$producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();
        $this->get(route('show.carrito'));
        $this->get(route('comprar.carrito'));

        $vendedor = User::find(2);
        $this->actingAs($vendedor);
        $this->post(route('store.confirmProduct'), [
            'id' => 1
        ]);
        $this->get(route('store.confirmarEnvioPedido',  1));
        
        $this->assertDatabaseHas('lineas_carritos', [
            'carrito_id' => 1,
            'producto_id' => $producto->id,
            'producto_pagado' => true,
            'producto_enviado' => true
        ]);

        $response = $this->get(route('user.chat', 1));
        $response->assertSee('Hemos enviado su pedido, por favor aguarde');
    }

    public function test_confirmar_llegada_y_se_envia_mensaje_al_chat(): void
    {

        $carrito = new Carrito;
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $lineas_carrito = new LineasCarrito();
        $user = User::find(1);
        $this->actingAs($user);
        
        
        $carrito -> usuario_id = $user->id;
        $carrito -> save();
        Producto::factory()->count(50)->create();
        $producto = Producto::where('store_id', 2)->first();

        
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id =$producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();
        $this->get(route('show.carrito'));
        $this->get(route('comprar.carrito'));

        $vendedor = User::find(2);
        $this->actingAs($vendedor);
        $this->post(route('store.confirmProduct'), [
            'id' => 1
        ]);
        $this->get(route('store.confirmarEnvioPedido',  1));
        $this->get(route('store.confirmarLlegadaPedido',  1));
        
        $this->assertDatabaseHas('lineas_carritos', [
            'carrito_id' => 1,
            'producto_id' => $producto->id,
            'producto_pagado' => true,
            'producto_enviado' => true,
            'recibido_vendedor' => true
        ]);

        $response = $this->get(route('user.chat', 1));
        $response->assertSee('Desde el equipo de envio nos han informado que has recibido el paquete');
    }
    public function test_confirmar_llegada_comprador_y_se_envia_mensaje_al_chat(): void
    {

        $carrito = new Carrito;
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);
        $lineas_carrito = new LineasCarrito();
        $user = User::find(1);
        $this->actingAs($user);
        
        
        $carrito -> usuario_id = $user->id;
        $carrito -> save();
        Producto::factory()->count(50)->create();
        $producto = Producto::where('store_id', 2)->first();

        
        $lineas_carrito->carrito_id = $carrito->id;
        $lineas_carrito->producto_id =$producto->id;
        $lineas_carrito->precio = $producto->precio;
        $lineas_carrito->save();
        $this->get(route('show.carrito'));
        $this->get(route('comprar.carrito'));

        $vendedor = User::find(2);
        $this->actingAs($vendedor);
        $this->post(route('store.confirmProduct'), [
            'id' => 1
        ]);
        $this->get(route('store.confirmarEnvioPedido',  1));
        $this->get(route('store.confirmarLlegadaPedido',  1));
     
        
        $this->assertDatabaseHas('lineas_carritos', [
            'carrito_id' => 1,
            'producto_id' => $producto->id,
            'producto_pagado' => true,
            'producto_enviado' => true,
            'recibido_vendedor' => true
        ]);

        $this->actingAs($user);
        $this->get(route('pedido.detallesPedidoRecibidoUser',  1));

        $this->assertDatabaseHas('pedidos', [
            'id' => 1,
            'recibido' => true
        ]);
        $response = $this->get(route('user.chat', 1));
        $response->assertSee('El comprador ha confirmado la llegada del pedido, el pedido se da por completado');
    }
}
