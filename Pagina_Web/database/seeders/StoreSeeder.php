<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tiendas = [
            ['shop_name' => 'PachecoShop', 'responsible' => 'Jose Luis Pacheco', 'identifier' => '12345678P', 'logo' => 'img/store/bitcoin.jpg', 'title' => 'Venta ilegal de criptomonedas', 'user_id'=> '1'],
            ['shop_name' => 'DiegoShop', 'responsible' => 'Diego Fernandez', 'identifier' => '12345678D', 'logo' => 'img/store/api.jpg', 'title' => 'Creacion de apis artesanas', 'user_id'=> '2'],
            ['shop_name' => 'IvanShop', 'responsible' => 'Ivan Reyes', 'identifier' => '12345678I', 'logo' => 'img/store/pulseras.jpg', 'title' => 'La Casa de la Artesanía', 'user_id'=> '3'],
            ['shop_name' => 'venedorShop', 'responsible' => 'Ivan Reyes', 'identifier' => '12345678I', 'logo' => 'img/store/logo-default.svg', 'title' => 'La tienda de un vendedor', 'user_id'=> '5'],
            
        ];
        foreach ( $tiendas as  $tienda) {
            Store::create($tienda);
        }
    
    }
}
