@extends('layouts.master')

@section('content')

<div id="login-container">
    <form method="POST" id="auth-form">
        @csrf
        <h4>INICIAR SESIÓN</h4>

        <div class="form-group">
            <label for="email">Email o Usuario: </label>
            <input type="text" id="email" name="email" required>
        </div>
        <div class="form-group">
            <label for="password">Contraseña: </label>
            <input type="password" id="password" name="password" required>
        </div>
        {{-- ERROR QUE SE MUESTRA AL RESTABLECER LA CONTRASEÑA Y DA ERROR --}}
        @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
        @endif

        {{-- ERROR DEL LOGIN --}}
        @error('login')
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $message }}
        </div>
        @enderror


        @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif
        <div class="form-group">
            <button type="submit">Entrar</button>
        </div>
        <div class="link">
            <a href="{{ route('auth.recoveryPassword') }}">¿Has olvidado la contraseña?</a>
        </div>
        <div class="signup-link">
            <p>¿No tienes cuenta? <a href="{{ route('auth.showRegister') }}">Registrarse</a></p>
        </div>
        <input type="hidden" name="local_carrito" id="local_carrito">
    </form>
</div>
@endsection
@section('js')
<script src="/js/login_carrito.js"></script>
<script src="/js/header.js"></script>
@endsection
