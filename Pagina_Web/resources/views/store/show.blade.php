@extends('layouts.master')

@section('title', 'DIP Marketplace - TIENDA')

@section('content')

<div class="container">
    <h1 class="title">{{ $shop->title }}</h1>
    <div class="photo-container">
        <img class="photo" src="/{{ $shop->logo }}" alt="Foto del Vendedor">
    </div>
    <div class="info">
        <div>
            <p>Nombre de la tienda: {{ $shop->shop_name }}</p>
            <br>
            <p>Responsable: {{ $shop->responsible }}</p>
        </div>
        <div>
            <p>Total de productos en venta: {{ count($productos) }}</p>
        </div>
    </div>
</div>

@if (Auth::check() && $carrito != null)
<p id="carrito-value" hidden>{{ count($idsCarrito) }}</p>
@endif

</section>
<section class="productos">
    @if (session('hide'))
    <div class="feedback">
        <div class="alert alert-success">
            {{ session('hide') }}
        </div>
    </div>
    @endif

    @if (session('delete'))
    <div class="feedback">
        <div class="alert alert-success">
            {{ session('delete') }}
        </div>
    </div>
    @endif
    <div class="productos__center">
        @forelse ($productos as $producto)
        <div id="{{ $producto->id }}" class="producto">
            <a href="{{ route('productos.show', $producto->id) }}">
                <div class="imagen__container">
                    <img src="{{ env("API_IMAGE"). $producto->id }}/1" alt="">
                </div>
                <div class="producto__footer">
                    <h4>{{ $producto->titulo }}</h4>
                    <div class="precio">{{ $producto->precio }}€</div>
                </div>
            </a>
            <div class="bottom">
                <div class="btn__group">
                    @if ($owner == false)
                    @if (Auth::check() && $carrito != null)
                    @if (in_array($producto->id, $idsCarrito))
                    <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                    @else
                    <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                    @endif
                    @else
                    <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                    @endif
                    @else
                    @if ($producto->disponible === 1)
                    <a class="btn_add en" href="{{ route('store.manage.hide', $producto->id) }}"><i class='bx bx-show'></i></a>
                    @else
                    <a class="btn_add dis" href="{{ route('store.manage.hide', $producto->id) }}"><i class='bx bx-hide'></i></a>
                    @endif
                    <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}"><i class='bx bxs-edit'></i></a>
                    <a class="btn_add" href="{{ route('store.manage.delete', $producto->id) }}"><i class='bx bxs-trash'></i></a>
                    @endif

                </div>
            </div>
        </div>
        @empty
        <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente para reponer
            nuestro inventario lo antes posible.</p>
        @endforelse
    </div>
</section>

@if ($owner)
<a class="manage-shop-link" href="{{ route('store.manage', $shop->shop_name) }}">
    <div class="manage-shop">
        <i class='bx bx-store-alt'></i>
        <p>Personalizar mi tienda </p>
    </div>
</a>
@endif
@endsection

@section('js')
<script src="/js/header.js"></script>
@if (Auth::check())
<script src="/js/carritoIndexDB.js" type="module"></script>
@else
<script src="/js/carritoIndex.js" type="module"></script>
@endif
@endsection
