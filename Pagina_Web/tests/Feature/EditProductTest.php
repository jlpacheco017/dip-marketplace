<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Producto;
use Database\Seeders\UserSeeder;
use Database\Seeders\StoreSeeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EditProductTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_crear_un_producto_correctamente(): void
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);

        $user = User::find(1);

        // Entrar con el usuario creado.
        $this->actingAs($user);

        // Crear un producto para editar
        $product = new Producto;

        $product->img = 'tmp';
        $product->titulo = 'titulo1';
        $product->descripcion = 'descripcion1';
        $product->precio = 1;
        $product->store_id = 1;
        $product->save();
        // Editar el producto
        $this->post(route('store.product.edit', $product->id), [
            'title' => 'titulo2',
            'description' => 'descripcion2',
            'price' => 1,
        ]);
        $this->assertDatabaseHas('productos', ['titulo' => 'titulo2', 'descripcion' => 'descripcion2']);
    }

   
}
