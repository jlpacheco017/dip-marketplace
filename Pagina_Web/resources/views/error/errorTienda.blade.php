
@extends('layouts.master')

@section('content')

<style>
  /* Estilos CSS para la página */
  body {
    font-family: Arial, sans-serif;
    background-color: #f0f0f0;
  }
  .container {
    max-width: 600px;
    margin: 10% auto;
    padding: 50px 20px;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 0 10px rgba(0,0,0,0.2);
    display: flex;
    flex-direction: column;
    align-items: center;
}
  h1 {
    font-size: 32px;
    color: #f44336;
    margin-top: 0;
  }
  .error_mensaje {
    margin-top: 3%;
    font-size: 18px;
    line-height: 1.5;
  }
  .button_error {
    display: inline-block;
    padding: 10px 20px;
    font-size: 18px;
    color: #fff;
    background-color: #f44336;
    border-radius: 5px;
    text-decoration: none;
    margin-top: 5%;
  }
  
  .error_mensaje{
    text-align: center;
    font-weight: bold;
  }

  .img_error{
    width: 85%;
  }
</style>

<div class="container">
  <h1>¡Lo siento, ha habido un error!</h1>
  <img class="img_error" src="../../../img/errorTienda.svg" alt="">
  <p class="error_mensaje">La tienda que buscas no se ha podido encontrar, por favor, busca una que este disponible</p>
  <a href="{{route('landing.index')}}" class="button_error">Volver al home</a>
</div>
@endsection
@section('js')
<script src="/js/login_carrito.js"></script>
<script src="/js/header.js"></script>
@endsection
