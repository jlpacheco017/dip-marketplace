@extends('layouts.master')

@section('title', 'DIP Marketplace - CREAR UNA TIENDA')

@section('content')

    <div id="login-container">
        <form method="POST" id="atuh-form" action="{{ route('store.doCreate') }}" enctype="multipart/form-data"> 
            @csrf
            <h4>CREAR UNA TIENDA</h4>

            <div class="form-group">
                <label for="shop_name">Nombre de la tienda:</label>
                <input type="text" id="shop_name" name="shop_name" value="{{ old('shop_name') }}" required>
                <div class="feedback_shop">
                    <p hidden id="shop_text" class="text-p-small errorpass"> ● No puede contener caracteres especiales y espacios.</p>
                </div>
            </div>
            <div class="form-group">
                <label for="responsible">Nombre completo del responsable:</label>
                <input type="text" id="responsible" name="responsible" value="{{ old('responsible') }}" required>
                <div class="feedback_shop">
                    <p hidden id="name_text" class="text-p-small errorpass"> ● No puede contener números, caracteres
                        especiales ni otros símbolos.</p>
                </div>
            </div>
            <div class="form-group">
                <input type="radio" id="nif" name="nifocif" value="nif">
                <label for="nif">NIF</label>
                <input type="radio" id="cif" name="nifocif" value="cif">
                <label for="cif">CIF</label>
            </div>

            <div id="divID" class="form-group">
                <input type="text" id="id" name="id" value="{{ old('id') }}" required>
                <div class="feedback_shop">
                    <p hidden id="IDText" class="text-p-small errorpass"></p>
                </div>
            </div>
            @if ($errors->has('radio'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('radio') }}
                </div>
            @endif
            @if ($errors->has('shop_name'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('shop_name') }}
                </div>
            @endif
            @if ($errors->has('responsible'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('responsible') }}
                </div>
            @endif
            @if ($errors->has('id'))
                <div class="error-message">
                    <i class='bx bx-error'></i> {{ $errors->first('id') }}
                </div>
            @endif
            <div class="form-group">
                <button type="submit">CREAR TIENDA</button>
            </div>
        </form>

    @endsection

    @section('js')
        <script src="/js/header.js"></script>
        <script src="/js/storeCheck.js"></script>
    @endsection
