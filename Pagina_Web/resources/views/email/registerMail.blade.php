<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
    <title>Bienvenido/a a DIP-Markeplace</title>
</head>

<body>
    <header>
        <h1><i class='bx bxs-box'></i> DIP-Markeplace</h1>
    </header>
    <main>
        <p>¡Hola <strong>{{ $username }}</strong>!</p>
        <p>Te damos la bienvenida a DIP-Markeplace. ¡Gracias por registrarte en nuestra plataforma!</p>
        <p>En DIP-Markeplace podrás encontrar una amplia variedad de productos y servicios para tu empresa. Estamos
            seguros de que encontrarás lo que necesitas.</p>
        <p>Si tienes alguna pregunta o necesitas ayuda, no dudes en ponerte en contacto con nosotros.</p>
        <p>¡Esperamos que disfrutes de tu experiencia en DIP-Markeplace!</p>
        <p>Gracias,</p>
        <p>El equipo de DIP-Markeplace</p>
    </main>
  
    <section class="productos">
      <h1 class="producto_destacado">Productos que te podrian interesar</h1>
      <div class="productos__center">
          @forelse ($productos as $producto)
              <div id="{{ $producto->id }}" class="producto">
                  <a href="{{ route('productos.show', $producto->id) }}">
                      <div class="imagen__container">
                          <img src="{{env('API_IMAGE').$producto->id}}/1" alt="">
                      </div>
                      <div class="producto__footer">
                        <div class="producto__footer">
                          <h4>{{ $producto->titulo }}</h4>
                          <div class="precio">{{ $producto->precio }}€</div>
                      </div>
                      </div>
                  </a>
              </div>
          @empty
          <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente para reponer nuestro inventario lo antes posible.</p>
              
          @endforelse
      </div>
  </section>
</body>
<style>
    /* Estilo global */
    body {
        background-color: #F3F3FF;
        color: #333;
        font-family: Arial, sans-serif;
        font-size: 16px;
        line-height: 1.5;
        margin: 0;
        padding: 0;
    }

    /* Encabezado */
    header {
        background-color: #4E23D6;
        padding: 40px;
        text-align: center;
    }

    h1 {
        color: #fff;
        font-size: 24px;
        margin: 0;
    }

    /* Contenido principal */
    main {
        background-color: #fff;
        border: 1px solid #ddd;
        border-top: none;
        padding: 20px;
    }

    /* Enlaces */
    a {
        color: #4E23D6;
        text-decoration: none;
    }

    a:hover {
        text-decoration: underline;
    }

    /* Botones */
    .producto_destacado{
      color: black;
      text-align: center;
      margin-bottom: 3%;
      font-size: 32px;
    }
    .boton {
        background-color: #4E23D6;
        border: none;
        border-radius: 5px;
        color: #fff;
        cursor: pointer;
        display: inline-block;
        font-size: 16px;
        margin-top: 20px;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        transition: background-color 0.3s ease;
    }

    .boton:hover {
        background-color: #3C1CAE;
    }


.productos {
    padding: 4rem 0;
}

.productos__center {
    padding: 0 3rem;
    display: grid;
    justify-items: center;
    grid-template-columns: repeat(auto-fit, minmax(150px, 230px));
    gap: 2rem 2rem;
}
.producto__footer h4 {
    overflow: hidden;
    text-align: center;
    color: #000000;
}
.precio {
    font-size: 2rem;
    color: #4E23D6;
    text-align: center;
    padding: 1rem 0 0 0;
}
.producto {
    display: flex;
    flex-direction: column;
    align-items: center;
    box-shadow: 0 0.5rem 1.5rem rgba(0, 0, 0, 0.2);
    background-color: $white;
    max-width: 40rem;
}

.imagen__container img {
    width: 100%;
    aspect-ratio: 1 / 1;
    object-fit: cover;
    aspect-ratio: 1 / 1;
}

.producto__footer {
    padding: 1rem 0 0 0;
    width: 100%;
}

.producto__footer h4 {
    overflow: hidden;
    text-align: center;
    color: $black;
}

.precio {
    font-size: 2rem;
    color: $purple;
    text-align: center;
    padding: 1rem 0 0 0;
}

.bottom {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 1rem 0 2rem 0;
}

.btn_add {
    display: inline-block;
    border: 0.1rem solid $black;
    padding: 1rem 1.8rem;
    font-size: 1.6rem;
    transition: all 300ms ease-in-out;
    color: $black;
    border: 0.1rem solid $black;
    background-color: transparent;
    cursor: pointer;
}
.btn_remove {
    display: inline-block;
    border: 0.1rem solid $black;
    padding: 1rem 1.8rem;
    font-size: 1.6rem;
    transition: all 300ms ease-in-out;
    color: $white;
    border: 0.1rem solid $black;
    background-color: $black;
    cursor: pointer;
}

.btn_add:hover {
    background-color: $purple;
    color: $white;
}
.btn_remove:hover {
    background-color: $red;
    color: $white;
}

@media screen and (max-width: 679px) {
    .productos__center {
        grid-template-columns: auto;
    }
}
.carrito_img{
    width: 10rem;
}
</style>

</html>
