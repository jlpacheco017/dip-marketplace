<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function test_registrar_un_usuario_correctamente()
    {
        // CREAR UN USUARIO.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'JosepQueraltMolina',
            'email' => 'josep.queralt@copernic.cat',
            'password' => 'Jzk9P8XZ1',
            'password_confirmation' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE HACE EL REDIRECT AL CREARLO.
        $response->assertRedirect(Route('auth.showLogin'));

        // COMPROBAR HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseHas('users', [
            'username' => 'JosepQueraltMolina',
            'email' => 'josep.queralt@copernic.cat',
        ]);
    }

    public function test_registrar_un_usuario_con_tildes()
    {
        // CREAR UN USUARIO CON TILDES.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'MiguelÁngelLuján',
            'email' => 'miguel.a.lujan@copernic.cat',
            'password' => 'Jzk9P8XZ1',
            'password_confirmation' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE HACE EL REDIRECT AL CREARLO.
        $response->assertRedirect(Route('auth.showLogin'));

        // COMPROBAR HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseHas('users', [
            'username' => 'MiguelÁngelLuján',
            'email' => 'miguel.a.lujan@copernic.cat',
        ]);
    }

    public function test_registrar_un_usuario_con_nombre_invalido()
    {
        // CREAR UN USUARIO CON CARACTERES ESPECIALES.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => '<h1>JordiBosomValmaña</h1>',
            'email' => 'jordi.bosom@copernic.cat',
            'password' => 'Jzk9P8XZ1',
            'password_confirmation' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE NOS DA UN ERROR DE 'USERNAME'.
        $response->assertSessionHasErrors('username');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => '<h1>JordiBosomValmaña</h1>',
            'email' => 'jordi.bosom@copernic.cat',
        ]);
    }

    public function test_registrar_un_usuario_con_correo_invalido()
    {
        // CREAR UN USUSARIO CON UN EMAIL INVALIDO.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'JordiLordanBurgos',
            'email' => 'jør∂î.lør∂añ@copernic@copernic.cat',
            'password' => 'Jzk9P8XZ1',
            'password_confirmation' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE NOS DA UN ERROR DE 'EMAIL'.
        $response->assertSessionHasErrors('email');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'JordiLordanBurgos',
            'email' => 'jør∂î.lør∂añ@copernic@copernic.cat',
        ]);
    }

    public function test_registrar_un_usuario_con_un_correo_que_ya_existe()
    {
        // CREAR UN CORREO.
        User::factory()->create([
            'email' => 'estecorreo@yaexiste.com',
        ]);

        // CREAR UN USUARIO CON ESE MISMO CORREO.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'IvánReyesNavarro',
            'email' => 'estecorreo@yaexiste.com',
            'password' => 'Jzk9P8XZ1',
            'password_confirmation' => 'Jzk9P8XZ1',
        ]);

        // COMPROBAR QUE DA ERROR TRAS INTENTAR CREAR UN USUARIO CON EL MISMO CORREO
        $response->assertSessionHasErrors('email');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'IvánReyesNavarro',
            'email' => 'estecorreo@yaexiste.com',
        ]);
    }

    public function test_registrar_un_usuario_con_una_password_debil()
    {
        // CREAR UN USUARIO CON UNA CONTRASEÑA DÉBIL.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'DiegoFernandezPerez',
            'email' => 'fernandez.perez.diego@alumnat.copernic.cat',
            'password' => 'contraseña_segura',
            'password_confirmation' => 'contraseña_segura',
        ]);

        // COMPROBAR QUE NOS DA UN ERROR DE 'PASSWORD'.
        $response->assertSessionHasErrors('password');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'DiegoFernandezPerez',
            'email' => 'fernandez.perez.diego@alumnat.copernic.cat',
        ]);
    }

    public function test_registrar_un_usuario_con_una_password_que_no_coincida()
    {
        // CREAR UN USUARIO CON UNA CONTRASEÑA QUE NO COINCIDA.
        $response = $this->post(Route('auth.doRegister'), [
            'username' => 'JoséLuisPachecoGuerrero',
            'email' => 'jlpacheco@gmail.com',
            'password' => 'Jzk9P8XZ1',
            'password_confirmation' => 'Fnw5L3VD2',
        ]);

        // COMPROBAR QUE NOS DA UN ERROR DE 'PASSWORD CONFIRMATION'.
        $response->assertSessionHasErrors('password_confirmation');

        // COMPROBAR QUE NO HA SIDO INSERTADO EN LA BBDD.
        $this->assertDatabaseMissing('users', [
            'username' => 'JoséLuisPachecoGuerrero',
            'email' => 'jlpacheco@gmail.com',

        ]);
    }
}
