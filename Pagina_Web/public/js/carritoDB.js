const div_principal = document.querySelector('.container_carrito');

const carritoValue = document.getElementById('carrito-value').innerHTML;
const updateHeaerCount = document.getElementById('carrito_numero');
updateHeaerCount.innerHTML = carritoValue;

function vaciarCarrito() {
  localStorage.removeItem('carrito');
  fetch("/vaciar-carrito", {
    method: "DELETE",
  });
  updateHeaerCount.innerHTML = updateHeaerCount.innerHTML - updateHeaerCount.innerHTML;
}
function sumarCarrito(suma) {
  const precioCarrito = document.querySelector('.price_carrito');
  var precioCarritoSinSimb = precioCarrito.innerHTML.slice(0, precioCarrito.innerHTML.length - 1);
  precioCarritoSinSimb = parseInt(precioCarritoSinSimb) - parseInt(suma);
  precioCarrito.innerHTML = precioCarritoSinSimb+"€";
}

const obtenerTodosLosBotones = () => {
  const container = document.querySelector('.container_carrito');
  return container;
};

window.addEventListener('pageshow', function (event) {
  if (event.persisted) {
    window.location.reload()
  }
});

obtenerTodosLosBotones().addEventListener('click', evento => {
  if (evento.target.nodeName === 'BUTTON') {
    let carritoArray = JSON.parse(localStorage.getItem("carrito")) || [];
    const id = evento.target.parentElement.parentElement.id;
    const precio = evento.target.parentElement.parentElement.children[2].children[0].textContent;
    var precioCarritoSinSimb = precio.slice(0, precio.length - 1);
    carritoArray = carritoArray.filter(obj => obj.id !== id);
    localStorage.setItem("carrito", JSON.stringify(carritoArray));
    fetch("/rm-producto", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({ id: id })
    });
    const div = document.getElementById(id);
    div.style.transition = "opacity 0.5s ease-in-out";
    div.style.opacity = 0;
    setTimeout(() => {
      div.remove();
    }, 500);
    sumarCarrito(precioCarritoSinSimb);
    updateHeaerCount.innerHTML = updateHeaerCount.innerHTML - 1;
  }

});



sumarCarrito(0)

