<?php

namespace Database\Seeders;

use App\Models\Producto;
use App\Models\Categoria;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategoriaproductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $productos = Producto::all();
        $numeroDeProductosTotales = count($productos);
        $categorias = Categoria::all();

        for ($i = 1; $i < $numeroDeProductosTotales + 1; $i++) {
            $categoria_select = $categorias->random();
            if ($categoria_select->id_padre == 0) {
                $productos->find($i)->categorias()->attach($categoria_select);
                
            }else{
                $padre = Categoria::find($categoria_select->id_padre);
                $productos->find($i)->categorias()->attach($padre);
                $productos->find($i)->categorias()->attach($categoria_select);
                
            }
        }
    }
}
