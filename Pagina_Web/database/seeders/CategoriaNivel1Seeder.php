<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class CategoriaNivel1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categorias = [
            ['titulo' => 'Electrónica', 'icono' => 'bx bx-laptop', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Ropa y accesorios', 'icono' => 'bx bxs-t-shirt', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Automóviles', 'icono' => 'bx bx-car', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Deportes', 'icono' => 'bx bx-basketball', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Juguetes', 'icono' => 'bx bxs-castle', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Belleza', 'icono' => 'bx bxs-paint', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Instru. Musicales', 'icono' => 'bx bxs-music', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Muebles', 'icono' => 'bx bx-chair', 'nivel' => 1, 'id_padre' => 0],
            ['titulo' => 'Artículos para bebés', 'icono' => 'bx bxs-baby-carriage', 'nivel' => 1, 'id_padre' => 0],
        ];
        foreach ($categorias as $categoria) {
            Categoria::create($categoria);
        }
    }
}
