<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function showErrorGeneral()
    {
        return view('error.errorGeneral');
    }

    public function showErrorProduct()
    {
        return view('error.errorProduct');
    }

    public function showErrorTienda()
    {
        return view('error.errorTienda');
    }
}
