<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    public function recoveryPassword()
    {
            return view('auth.emailForm');
    }

    public function recoveryPasswordSender(Request $request)
    {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users'
            ]);
        
            if ($validator->fails()) {
                return back()->withErrors(['email' => 'El email proporcionado no existe.'])->withInput();
            }
            
    
            $token = Str::random(64);
    
            DB::table('password_reset_tokens')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now(),
                'expires_at' => Carbon::now()->addDay()
            ]);
            $username = User::where('email', $request->email)->first();
            Mail::send('email.forgetPassword', ['token' => $token, 'username' => $username], function ($message) use ($request) {
                $message->to($request->email);
                $message->subject('Recuperar contraseña');
            });
    
            return back()->with('message', 'Hemos enviado un correo con el enlace para recuperar la contraseña!');
    }

    public function showResetPasswordForm($token)
    {
            $passwordResetToken = DB::table('password_reset_tokens')
                ->where('token', $token)
                ->where('expires_at', '>', Carbon::now())
                ->first();
    
            if (!$passwordResetToken || !$passwordResetToken->email) {
                return redirect()->route('auth.showLogin')->with('error', 'El enlace para restablecer la contraseña no es válido o ha expirado.');
            }
            $user = User::where('email', $passwordResetToken->email)->first();
            return view('auth.newPassword', ['token' => $token, 'user' => $user->username]);
    }

    public function submitResetPasswordForm(Request $request)
    {
            $request->validate([
                'password' => [
                    'required',
                    'min:8',
                    'regex:/^(?=.*[a-z])(?=.*[A-Z]).+$/'
                ],
                'password_confirmation' => 'required|same:password'
            ],
            [
                'password.required' => 'La contraseña es obligatoria',
                'password.min' => 'La contraseña no es válida',
                'password.regex' => 'La contraseña no es válida',
                'password_confirmation.required' => 'Debe repetir la contraseña',
                'password_confirmation.same' => 'Las contraseñas no coinciden'
            ]);
            $passwordResetToken = DB::table('password_reset_tokens')
                ->where('token', $request->token)
                ->where('expires_at', '>', Carbon::now())
                ->first();
            if (!$passwordResetToken) {
                return back()->with('error', 'El enlace para restablecer la contraseña no es válido o ha expirado.');
            }
            $updatePassword = DB::table('password_reset_tokens')
                ->where([
                    'email' => $passwordResetToken->email,
                    'token' => $request->token
                ])
                ->first();
    
            if (!$updatePassword) {
                return back()->withInput()->with('error', 'El token no es válido.');
            }
    
            $user = User::where('email', $passwordResetToken->email)
                ->update(['password' => Hash::make($request->password)]);
    
            DB::table('password_reset_tokens')->where(['email' => $passwordResetToken->email])->delete();
    
            return redirect()->route('auth.showLogin')->with('message', 'La contraseña se ha cambiado correctamente!');
    }
}
