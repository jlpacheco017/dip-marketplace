<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class ImageController extends Controller
{
    const NUMERO_MAXIMO_FOTOS = 5;

    public function uploadProductImage(Request $request)
    {
        // Validamos si el ID y la posición de la foto del producto corresponde con nuestras reglas.
        $idProduct = $request->idProduct;
        $imageProductPosition = $request->imageProductPosition;
        if (!ctype_digit($idProduct) || !ctype_digit($imageProductPosition)) {
            return response()->json(['error' => 'Unos de los valores introducidos es incorrecto. Debe ser un entero.'], 400);
        }

        // Verifica si la posición de la imagen del producto está en el rango de 1 a 7
        if ($imageProductPosition < 1 || $imageProductPosition > SELF::NUMERO_MAXIMO_FOTOS) {
            return response()->json(['error' => 'La posición de la imagen del producto debe estar entre 1 y ' . SELF::NUMERO_MAXIMO_FOTOS . '.'], 400);
        }

        try {
            if (!$request->hasFile('image')) {
                throw new Exception('No se ha enviado ningún archivo');
            }
            $uploadedFile = $request->file('image');

            // Valida el tamaño de la imagen
            if ($uploadedFile->getSize() >  2000000) { // 2 MB, esta en bytes
                throw new Exception('El archivo es demasiado grande');
            }

            // Crea una instancia de la imagen con Intervention Image
            $image = Image::make($uploadedFile);

            // Establece los valores de ancho y alto máximo deseados
            $maxWidth = 500;  // Reemplaza con el ancho máximo deseado
            $maxHeight = 500; // Reemplaza con la altura máxima deseada

            // Redimensiona la imagen al tamaño exacto deseado, sin mantener el aspecto de ancho y alto proporcionado
            $image->resize($maxWidth, $maxHeight);

            // Guarda la imagen redimensionada en el directorio deseado
            $imagePath = 'public/images/' . $idProduct . '-' . $imageProductPosition . '.' . $uploadedFile->getClientOriginalExtension();
            $image->save(storage_path('app/' . $imagePath));

            return response(['path' => $imagePath], Response::HTTP_CREATED);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function getProductImages($productId)
    {
        if (!ctype_digit($productId)) {
            return response()->json(['error' => 'Los valores introducidos son incorrectos. Deben ser enteros.'], 400);
        }
        try {
            $files = Storage::files('public/images');
            // dd($files);
            $images = collect($files)
                ->filter(function ($filename) use ($productId) {
                    return preg_match('/^' . $productId . '-\d+\.(jpg|png)$/', basename($filename));
                })
                ->map(function ($filename) {
                    return url(Storage::url($filename));
                });
    
            // dd($images);
            if ($images->isEmpty()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'No images found for the given product id.'
                ], 404);
            }
    
            return response()->json([
                'status' => 'success',
                'images' => $images
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'status' => 'error',
                'message' => 'An error occurred while processing the request: ' . $exception->getMessage()
            ], 500);
        }
    }

    public function getProductImageUnic($productId, $position)
    {
        // Validar productId y position
        if (!ctype_digit($productId) || !ctype_digit($position)) {
            return response()->json(['error' => 'Los valores introducidos son incorrectos. Deben ser enteros.'], 400);
        }

        // Verificar si la posición está en el rango permitido
        if ($position < 1 || $position > 5) {
            return response()->json(['error' => 'La posición debe estar entre 1 y 5.'], 400);
        }

        try {
            // Obtener todos los archivos en el directorio de imágenes
            $files = Storage::files('public/images');

            // Filtrar los archivos que coincidan con el productId y position
            $productImage = collect($files)->first(function ($filename) use ($productId, $position) {
                return preg_match('/^' . $productId . '-' . $position . '\.(jpg|png)$/', basename($filename));
            });

            // Verificar si se encontró una imagen
            if (!$productImage) {
                return $this->getProductImageDefault();
                // return response()->json(['error' => 'Imagen no encontrada.'], 404);
            }

            // Devolver la imagen solicitada
            $headers = [
                'Content-Type' => Storage::mimeType($productImage),
                'Content-Disposition' => 'inline; filename="' . basename($productImage) . '"'
            ];

            return response(Storage::get($productImage), 200, $headers);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getProductImageDefault()
    {
        try {
            // Obtener todos los archivos en el directorio de imágenes
            $files = Storage::files('public/images');

            // Buscar la imagen con el nombre "default.png"
            $defaultImage = collect($files)->first(function ($filename) {
                return basename($filename) === 'default.png';
            });

            // Verificar si se encontró una imagen
            if (!$defaultImage) {
                return response()->json(['error' => 'Imagen no encontrada.'], 404);
            }

            // Devolver la imagen solicitada
            $headers = [
                'Content-Type' => Storage::mimeType($defaultImage),
                'Content-Disposition' => 'inline; filename="' . basename($defaultImage) . '"'
            ];

            return response(Storage::get($defaultImage), 200, $headers);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteProductImageUnic($productId, $position)
    {
        // Validar productId y position
        if (!ctype_digit($productId) || !ctype_digit($position)) {
            return response()->json(['error' => 'Los valores introducidos son incorrectos. Deben ser enteros.'], 400);
        }

        // Verificar si la posición está en el rango permitido
        if ($position < 1 || $position > 5) {
            return response()->json(['error' => 'La posición debe estar entre 1 y 5.'], 400);
        }

        try {
            // Obtener todos los archivos en el directorio de imágenes
            $files = Storage::files('public/images');

            // Filtrar los archivos que coincidan con el productId y position
            $productImage = collect($files)->first(function ($filename) use ($productId, $position) {
                return preg_match('/^' . $productId . '-' . $position . '\.(jpg|png)$/', basename($filename));
            });

            // Verificar si se encontró una imagen
            if (!$productImage) {
                return response()->json(['error' => 'Imagen no encontrada.'], 404);
            }

            // Eliminar la imagen
            Storage::delete($productImage);

            // Devolver una respuesta exitosa
            return response()->json(['success' => 'Imagen eliminada con éxito.'], 200);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function deleteProductImages($productId)
    {
        // Validar productId
        if (!ctype_digit($productId)) {
            return response()->json(['error' => 'El valor introducido es incorrecto. Debe ser un entero.'], 400);
        }

        try {
            // Obtener todos los archivos en el directorio de imágenes que coincidan con el productId
            $files = Storage::files('public/images');
            $productImages = collect($files)->filter(function ($filename) use ($productId) {
                return preg_match('/^' . $productId . '-\d+\.(jpg|png)$/', basename($filename));
            });

            // Eliminar todas las imágenes encontradas
            $deleted = 0;
            foreach ($productImages as $productImage) {
                if (Storage::delete($productImage)) {
                    $deleted++;
                }
            }

            // Devolver una respuesta con el número de imágenes eliminadas
            return response()->json([
                'status' => 'success',
                'message' => 'Se han eliminado ' . $deleted . ' imágenes del producto ' . $productId . '.'
            ]);
        } catch (Exception $e) {
            return response(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
