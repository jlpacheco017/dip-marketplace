const obtenerNombreDeLaTienda = () => {

    const inputshopName = document.querySelector('#shop_name');
    return inputshopName.value;
}

const obtenerElNombreCompleto = () => {

    const inputName = document.querySelector('#responsible');
    return inputName.value;
}

const obtenerElID = () => {

    const inputID = document.querySelector('#id');
    return inputID.value;
}

const obtenerElTitleStore = () => {

    const inputTitleStore = document.querySelector('#titleStore');
    return inputTitleStore.value;
}


validarShopName = () => {
    const inputShopName = obtenerNombreDeLaTienda();

    const patternShopName = /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ]+$/u
    const shopText = document.querySelector('#shop_text');

    if (inputShopName.length === 0) {
        shopText.setAttribute("hidden", false);
    } else if (inputShopName.length != 0) {
        shopText.removeAttribute("hidden");
    }

    // CHECK CHARACTERS
    if (!inputShopName.match(patternShopName)) {
        shopText.classList.add("errorpass");
    } else {
        shopText.setAttribute("hidden", false);
    }
};

validarName = () => {
    const inputName = obtenerElNombreCompleto();
    const patternName = /^[a-zA-ZáéíóúÁÉÍÓÚñÑüÜ\s]+$/
    const nameText = document.querySelector('#name_text');

    if (inputName.length === 0) {
        nameText.setAttribute("hidden", false);
    } else if (inputName.length != 0) {
        nameText.removeAttribute("hidden");
    }

    // CHECK NAME
    if (!inputName.match(patternName)) {
        nameText.classList.add("errorpass");
    } else {
        nameText.setAttribute("hidden", false);
    }
};

validarID = () => {
    const inputID = obtenerElID();

    const radios = document.getElementsByName("nifocif");

    radios.forEach(function (radio) {
        if (radio.checked) {
            if (radio.value === "nif") {

                const patternNIF = /^[0-9]{8}[a-zA-Z]$/;
                const IDTextNIF = document.querySelector('#IDText');

                if (inputID.length === 0) {
                    IDTextNIF.setAttribute("hidden", false);
                } else if (inputID.length != 0) {
                    IDTextNIF.removeAttribute("hidden");
                    IDTextNIF.innerHTML = "● El formato del NIF debe contener ocho dígitos y una letra al final.";
                }

                // CHECK NIF
                if (!inputID.match(patternNIF)) {
                    IDTextNIF.classList.add("errorpass");

                } else {
                    IDTextNIF.setAttribute("hidden", false);
                }

            } else if (radio.value === "cif") {

                const patternCIF = /^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/;
                const IDTextCIF = document.querySelector('#IDText');

                if (inputID.length === 0) {
                    IDTextCIF.setAttribute("hidden", false);
                } else if (inputID.length != 0) {
                    IDTextCIF.removeAttribute("hidden");
                    IDTextCIF.innerHTML = "● El formato del CIF debe ser una letra seguida de siete dígitos y otra letra o número al final.";

                }

                // CHECK CIF
                if (!inputID.match(patternCIF)) {
                    IDTextCIF.classList.add("errorpass");
                } else {
                    IDTextCIF.setAttribute("hidden", false);
                }
            }
        }
    });
}

validarTitleStore = () => {

    const inputTitleStore = obtenerElTitleStore();
    const lengthTitle = document.querySelector('#lengthTitle');

    if (inputTitleStore.length > 25) {
        lengthTitle.removeAttribute('hidden');
        lengthTitle.classList.add("errorpass");
    } else {
        lengthTitle.setAttribute("hidden", false);
    }

}

if (document.querySelector('#shop_name')) {
    const inputShopName = document.querySelector('#shop_name');

    inputShopName.addEventListener('input', () => {
        validarShopName();
    });
}

if (document.querySelector('#responsible')) {
    const inputName = document.querySelector('#responsible');

    inputName.addEventListener('input', () => {
        validarName();
    });
}

if (document.querySelector('#id')) {
    const inputID = document.querySelector('#id');

    inputID.addEventListener('input', () => {
        validarID();
    });
}

const radios = document.getElementsByName("nifocif");

radios.forEach(function (radio) {
    radio.addEventListener("click", function () {
        validarID();
    });
});


if (document.querySelector('#titleStore')) {
    const inputTitleStore = document.querySelector('#titleStore');

    inputTitleStore.addEventListener('input', () => {
        validarTitleStore();
    });
}