import Carrito from "./clases/Carrito.js";
import Producto from "./clases/Producto.js";

// Crear una instancia de la clase Carrito
const carrito = new Carrito();

const obtenerTodosLosProductos = () => {
    const productos = document.querySelector('.productos');
    if (!productos) {
        const productos = document.querySelector('.detalles');
    }
    return productos;
};

window.addEventListener('pageshow', function(event) {
    if (event.persisted) {
      window.location.reload()
    }
  });

obtenerTodosLosProductos().addEventListener('click', evento => {
    if (evento.target.nodeName === 'BUTTON') {
        const id = evento.target.parentElement.parentElement.parentElement.id;
        const img = evento.target.parentElement.parentElement.parentElement.children[0].children[0].children[0].getAttribute('src');
        const titulo = evento.target.parentElement.parentElement.parentElement.children[0].children[1].children[0].textContent;
        const precio = evento.target.parentElement.parentElement.parentElement.children[0].children[1].children[1].textContent;

        const producto = new Producto(id, img, titulo, precio);

        if (evento.target.classList.contains('btn_add')) {
            carrito.agregarProducto(producto);
            eliminarBoton(evento);
            recuentoDeProductos();
        } else {
            let carritoArray = JSON.parse(localStorage.getItem("carrito")) || [];
            const indice = carritoArray.findIndex(carrito => carrito.id == id);

            carrito.eliminarProducto(indice);
            agregarBoton(evento);
            recuentoDeProductos();
        }
    }

});

const eliminarBoton = (evento) => {
    evento.target.classList.remove('btn_add');
    evento.target.classList.add('btn_remove');
    evento.target.innerHTML = "Eliminar de la <i class='bx bxs-shopping-bag'></i>";
};

const agregarBoton = (evento) => {
    evento.target.classList.remove('btn_remove');
    evento.target.classList.add('btn_add');
    evento.target.innerHTML = "Añadir a la <i class='bx bxs-shopping-bag'></i>";
};

function cambiarEstiloDelBotonAlRecargarLaPagina() {
    const carritoJSON = JSON.parse(localStorage.getItem("carrito")) || [];
    const ids = carritoJSON.map(obj => parseInt(obj.id));

    ids.forEach(id => {
        const divProducto = document.getElementById(id);
        if (divProducto) {
            const btnAdd = divProducto.querySelector('.btn_add');
            btnAdd.setAttribute('class', 'btn_remove');
            btnAdd.innerHTML = "Eliminar de la <i class='bx bxs-shopping-bag'></i>";

        }
    });
}

const recuentoDeProductos = () => {
    if (!(localStorage.getItem('carrito') === null)) {
        const count = JSON.parse(localStorage.getItem('carrito')).length;
        document.getElementById("carrito_numero").innerText = count;
    }
};

cambiarEstiloDelBotonAlRecargarLaPagina();
recuentoDeProductos();