<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Producto;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Database\Seeders\UserSeeder;
use Database\Seeders\StoreSeeder;

class AddProductTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     */
    public function test_crear_un_producto_correctamente(): void
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);

        $user = User::find(1);

        // Entrar con el usuario creado.
        $this->actingAs($user);

        $shop_id = 1;

        $this->post(route('store.addProduct', $shop_id), [
            'title' => 'Titulo',
            'description' => 'descripcion',
            'price' => 1000,
        ]);


        $this->assertDatabaseHas('productos', ['titulo' => 'Titulo', 'descripcion' => 'descripcion']);
    }

    public function test_crear_un_producto_con_un_titulo_invalido(): void
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);

        $user = User::find(1);

        // Entrar con el usuario creado.
        $this->actingAs($user);

        $shop_id = 1;

        $this->post(route('store.addProduct', $shop_id), [
            'title' => '<h1>Titulo</h1>',
            'description' => 'descripcion',
            'price' => 1000,
        ]);


        $this->assertDatabaseMissing('productos', ['titulo' => '<h1>Titulo</h1>', 'descripcion' => 'descripcion']);
    }

    public function test_crear_un_producto_con_un_precio_invalido(): void
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);

        $user = User::find(1);

        // Entrar con el usuario creado.
        $this->actingAs($user);

        $shop_id = 1;

        $this->post(route('store.addProduct', $shop_id), [
            'title' => 'Titulo',
            'description' => 'descripcion',
            'price' => 100.000,
        ]);


        $this->assertDatabaseMissing('productos', ['titulo' => 'Titulo', 'descripcion' => 'descripcion', 'price' => 100.000]);
    }

    public function test_crear_un_producto_con_una_descripcion_de_mas_de_255_caracteres(): void
    {
        $this->seed(UserSeeder::class);
        $this->seed(StoreSeeder::class);

        $user = User::find(1);

        // Entrar con el usuario creado.
        $this->actingAs($user);

        $shop_id = 1;

        $this->post(route('store.addProduct', $shop_id), [
            'title' => 'Titulo',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id 
            ligula in neque laoreet ornare id cursus orci. Nullam ornare tincidunt finibus. Suspendisse 
            lobortis varius elit vitae rhoncus. Cras ut magna massa. Nam commodo nulla augue, quis posuere 
            turpis faucibus et. Aenean non fringilla ligula. Duis sed sagittis dui. Etiam velit urna, rhoncus non
             venenatis non, vehicula sit amet lectus. Suspendisse interdum, leo eu semper volutpat, neque tortor rutrum 
             nisl, a laoreet erat leo et massa. Nam nec volutpat augue. Donec lacinia leo luctus risus varius porttitor.',
            'price' => 100,
        ]);


        $this->assertDatabaseMissing('productos', ['titulo' => 'Titulo', 'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id 
        ligula in neque laoreet ornare id cursus orci. Nullam ornare tincidunt finibus. Suspendisse 
        lobortis varius elit vitae rhoncus. Cras ut magna massa. Nam commodo nulla augue, quis posuere 
        turpis faucibus et. Aenean non fringilla ligula. Duis sed sagittis dui. Etiam velit urna, rhoncus non
         venenatis non, vehicula sit amet lectus. Suspendisse interdum, leo eu semper volutpat, neque tortor rutrum 
         nisl, a laoreet erat leo et massa. Nam nec volutpat augue. Donec lacinia leo luctus risus varius porttitor.', 'price' => 100.000]);
    }
}
