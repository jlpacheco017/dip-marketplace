<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('id_chat');
            $table->foreign('id_chat')->references('id')->on('chat_usuarios')->cascadeOnDelete();

            $table->bigInteger('enviado_por');
            $table->bigInteger('recibido_por');
            $table->string('mensaje');
            $table->boolean('leido')->default(false); 
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
