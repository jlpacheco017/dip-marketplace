const obtenerElTituloDelProducto = () => {

    const inputTitle = document.querySelector('#title');
    return inputTitle.value;
}

const obtenerLaDescripcionDelProducto = () => {

    const inputDescription = document.querySelector('#description');
    return inputDescription.value;
}

const obtenerElPrecioDelProducto = () => {

    const inputPrice = document.querySelector('#price');
    return inputPrice.value;
}

validarTitle = () => {
    const inputTitle = obtenerElTituloDelProducto();

    const patternTitle = /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ.'"\s\-()]+$/

    const patternTitleText = document.querySelector('#patternTitleText');
    const patternTitleTextMaxLength = document.querySelector('#patternTitleTextMaxLength');

    if (inputTitle.length === 0) {
        patternTitleText.setAttribute("hidden", false);
    } else if (inputTitle.length != 0) {
        patternTitleText.removeAttribute("hidden");
    }

    if (inputTitle.length >= 50) {
        patternTitleTextMaxLength.removeAttribute("hidden");
    } else {
        patternTitleTextMaxLength.setAttribute("hidden", false);
    }

    if (!inputTitle.match(patternTitle)) {
        patternTitleText.classList.remove("validated");
        patternTitleText.classList.add("errorpass");
    } else {
        patternTitleText.setAttribute("hidden", false);
    }
}


validarDescripcion = () => {
    const inputDescription = obtenerLaDescripcionDelProducto();

    const patternDescriptionTextMaxLength = document.querySelector('#patternDescriptionTextMaxLength');

    if (inputDescription.length >= 255) {
        patternDescriptionTextMaxLength.removeAttribute("hidden");
    } else {
        patternDescriptionTextMaxLength.setAttribute("hidden", false);
    }
}

validarPrice = () => {
    const inputPrice = obtenerElPrecioDelProducto();

    const PatternPrice = /^[0-9]\d*$/
    const patternPriceType = document.querySelector('#patternPriceType');
    const patternPriceRange = document.querySelector('#patternPriceRange');
    if (inputPrice.length === 0) {
        patternPriceType.setAttribute("hidden", false);
        patternPriceRange.setAttribute("hidden", false);
    } else if (inputPrice.length != 0) {
        patternPriceRange.removeAttribute("hidden");
        patternPriceType.removeAttribute("hidden");
    }

    if (!inputPrice.match(PatternPrice)) {
        patternPriceType.classList.add("errorpass");
    } else {
        patternPriceType.setAttribute("hidden", false);
    }

    if (inputPrice > 0 && inputPrice <= 50000) {
        patternPriceRange.setAttribute("hidden", false);
    } else {
        patternPriceRange.removeAttribute("hidden");
    }
}


if (document.querySelector('#title')) {
    const inputTitle = document.querySelector('#title');
    inputTitle.addEventListener('input', () => {
        validarTitle();
    });
}

if (document.querySelector('#description')) {
    const inputDescription = document.querySelector('#description');
    inputDescription.addEventListener('input', () => {
        validarDescripcion();
    });
}

if (document.querySelector('#price')) {
    const inputPrice = document.querySelector('#price');
    inputPrice.addEventListener('input', () => {
        validarPrice();
    });
}