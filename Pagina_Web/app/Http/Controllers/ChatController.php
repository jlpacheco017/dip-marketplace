<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\User;
use App\Models\Store;
use App\Models\ChatUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function showChat($id)
    {

        $soy = "";
        $chatUsuarios = ChatUsuario::where('id', $id)->first();
        $chat = Chat::where('id_chat', $chatUsuarios->id)->get();
        $vendedor = User::find($chatUsuarios->id_vendedor);
        $comprador = User::find($chatUsuarios->id_comprador);
        if ($vendedor->id == Auth::user()->id || $comprador->id == Auth::user()->id) {
            $shop = Store::where('user_id', $vendedor->id)->first();
            if (Auth::user()->id == $vendedor->id) {
                $soy = "vendedor";
                foreach ($chat as $mensaje) {
                    if ($mensaje->recibido_por == $vendedor->id) {
                        $mensaje->leido = true;
                        $mensaje->save();
                    }
                }
            } else {
                $soy = "comprador";
                if (Auth::user()->id == $comprador->id) {
                    foreach ($chat as $mensaje) {
                        if ($mensaje->recibido_por == $comprador->id) {
                            $mensaje->leido = true;
                            $mensaje->save();
                        }
                    }
                }
            }

            return view('users.chat', ['chat' => $chat, 'pedido_id' => $id, 'vendedor' => $vendedor, 'comprador' => $comprador, 'shop' => $shop, 'soy' => $soy]);
        } else {
            return redirect(Route('errorTienda.showErrorTienda'));
        }
    }

    public function sendChat(Request $request, $id)
    {
        $chat = new Chat();
        $chat_select = ChatUsuario::find($id);
        $chat->id_chat = $id;
        if (Auth::user()->id == $chat_select->id_vendedor) {
            $chat->enviado_por = $chat_select->id_vendedor;
            $chat->recibido_por = $chat_select->id_comprador;
        } else {
            $chat->enviado_por = $chat_select->id_comprador;
            $chat->recibido_por = $chat_select->id_vendedor;
        }
        $chat->mensaje = $request->mensaje;
        $chat->save();
        return back();
    }
}
