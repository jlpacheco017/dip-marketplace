
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
  <title>Restablecimiento de contraseña - DIP-Markeplace</title>
</head>
<body>
  <header>
    <h1><i class='bx bxs-box'></i> DIP-Markeplace</h1>
  </header>
  <main>
    <p>Estimado/a <strong>{{$username->username}}</strong>,</p>
    <p>Hemos recibido una solicitud para restablecer tu contraseña en DIP-Markeplace.</p>
    <p>Por favor, sigue el siguiente enlace para restablecer tu contraseña:</p>
    <p><a href="{{ url('reset-password/' . $token) }}">Restablecer contraseña</a></p>
    <p>Si no solicitaste un restablecimiento de contraseña, puedes ignorar este correo.</p>
    <p>Si tienes alguna pregunta o necesitas ayuda, por favor no dudes en contactarnos.</p>
    <p>Gracias,</p>
    <p>El equipo de DIP-Markeplace</p>
  </main>
</body>
<style>
    /* Estilo global */
    body {
      background-color: #F3F3FF;
      color: #333;
      font-family: Arial, sans-serif;
      font-size: 16px;
      line-height: 1.5;
      margin: 0;
      padding: 0;
    }

    /* Encabezado */
    header {
      background-color: #4E23D6;
      padding: 40px;
      text-align: center;
    }

    h1 {
      color: #fff;
      font-size: 24px;
      margin: 0;
    }

    /* Contenido principal */
    main {
      background-color: #fff;
      border: 1px solid #ddd;
      border-top: none;
      padding: 20px;
    }

    /* Enlaces */
    a {
      color: #4E23D6;
      text-decoration: none;
    }

    a:hover {
      text-decoration: underline;
    }

    /* Botones */
    .boton {
      background-color: #4E23D6;
      border: none;
      border-radius: 5px;
      color: #fff;
      cursor: pointer;
      display: inline-block;
      font-size: 16px;
      margin-top: 20px;
      padding: 10px 20px;
      text-align: center;
      text-decoration: none;
      transition: background-color 0.3s ease;
    }

    .boton:hover {
      background-color: #3C1CAE;
    }
  </style>
</html>