export default class Carrito {
    item;
    
    constructor() {
        this.item = [];
        this.recuperarCarrito();
    }

    crearCarrito() {
        localStorage.setItem("carrito", JSON.stringify(this.item));
    }

    recuperarCarrito() {
        const carritoGuardado = JSON.parse(localStorage.getItem("carrito"));
        if (carritoGuardado) {
            this.item = carritoGuardado;
        } else {
            this.item = [];
        }
    }

    agregarProducto(producto) {
        // Agregar el producto al carrito
        this.item.push(producto);

        // Actualizar el carrito en localStorage
        this.crearCarrito();
    }

    eliminarProducto(index) {
        // Eliminar el producto del carrito
        this.item.splice(index, 1);

        // Actualizar el carrito en localStorage
        this.crearCarrito();
    }
}