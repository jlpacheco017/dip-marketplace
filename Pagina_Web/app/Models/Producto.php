<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Producto extends Model
{
    use HasFactory;
    public function categorias(){
        return $this->belongsToMany(Categoria::class);
    }
    public function user(){
        return $this->belongsToMany(User::class);
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'titulo',
        'precio',
    ];
    public function getProductoPorId($id){
        return $this->query()->where('id', $id)->first();
    }
}
