@extends('layouts.master')

@section('title', 'DIP Marketplace')

@section('content')
<section class="hero">
    <div class="hero-text">
        <h1>La <span>artesanía</span> es una forma de honrar el <span>pasado</span> mientras construimos el
            <span>futuro</span>
        </h1>
        <p>Made by Diego Fernandez, Pacheco Guerrero e Iván Reyes.</p>
        <a href="#categorias">Explorar los productos</a>
    </div>
    <div class="hero-image">
        <img draggable="false" src="{{ asset('img/artesania.svg') }}" alt="Artesanía">
    </div>
</section>
@if (Auth::check() && $carrito != null)
<p id="carrito-value" hidden>{{ count($idsCarrito) }}</p>
@endif
<form action="{{ route('productos.index') }}" method="GET">
    {{-- Listado categorias --}}
    <div id="categorias">
        <button hidden type="submit" value="{{ old('categoria') }}">
        </button>
        <button class="categoria" type="submit" value="0">
            Todas las categorías
        </button>
        @foreach ($categorias as $categoria)
        <button class="categoria" type="submit" value="{{ $categoria->id }}">
            {{ $categoria->titulo }}
        </button>
        @endforeach
    </div>
    <input id="inputHidden" type="hidden" name="categoria" value="{{ old('categoria') }}">

    {{-- Barra de busqueda --}}
    <div id="barraDeBusqueda">
        <input type="search" placeholder="Buscar productos..." name="search" value="{{ old('search') }}">
        <button id="mySubmit" type="submit"><i class='bx bx-search'></i></button>
    </div>
</form>

@if ($productos_suficientes == true)
<section class="productos" id="productos">
    <div class="division_superior">
        <h3 class="titulos">Los productos mas populares!!</h3>
        <a href="{{ route('productos.index') }}">Ver mas...</a>
    </div>

    <div class="productos__center" id="productos_landing__center">

        @forelse ($productos as $producto)
        <div id="{{ $producto->id }}" class="producto">
            <a href="{{ route('productos.show', $producto->id) }}">
                <div class="imagen__container">
                    <img src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                </div>
                <div class="producto__footer">
                    <h4>{{ $producto->titulo }}</h4>
                    <div class="precio">{{ $producto->precio }}€</div>
                </div>
            </a>
            <div class="bottom">
                <div class="btn__group">
                    @if (in_array($producto->id, $productosStore))
                    <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                    @else
                    @if (Auth::check() && $carrito != null)
                    @if (in_array($producto->id, $idsCarrito))
                    <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                    @else
                    <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                    @endif
                    @else
                    <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                    @endif
                    @endif
                </div>
            </div>
        </div>
        @empty
        <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente para
            reponer
            nuestro inventario lo antes posible.</p>
        @endforelse
    </div>

    <div class="cat_container">
        <section class="productos" id="productos">
            <div class="division_superior">
                <h3 class="titulos_sub">{{ $categoria1->titulo }}</h3>
                <a href="{{ route('productos.index') }}">Ver mas...</a>
            </div>

            <div class="productos__center" id="cats">

                @forelse ($cat1 as $producto)
                <div id="{{ $producto->id }}" class="producto">
                    <a href="{{ route('productos.show', $producto->id) }}">
                        <div class="imagen__container">
                            <img src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                        </div>
                        <div class="producto__footer">
                            <h4>{{ $producto->titulo }}</h4>
                            <div class="precio">{{ $producto->precio }}€</div>
                        </div>
                    </a>
                    <div class="bottom">
                        <div class="btn__group">
                            @if (in_array($producto->id, $productosStore))
                            <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                            @else
                            @if (Auth::check() && $carrito != null)
                            @if (in_array($producto->id, $idsCarrito))
                            <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @empty
                <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente
                    para
                    reponer
                    nuestro inventario lo antes posible.</p>
                @endforelse
            </div>
        </section>
        <section class="productos" id="productos">
            <div class="division_superior">
                <h3 class="titulos_sub">{{ $categoria2->titulo }}</h3>
                <a href="{{ route('productos.index') }}">Ver mas...</a>
            </div>

            <div class="productos__center" id="cats">

                @forelse ($cat2 as $producto)
                <div id="{{ $producto->id }}" class="producto">
                    <a href="{{ route('productos.show', $producto->id) }}">
                        <div class="imagen__container">
                            <img src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                        </div>
                        <div class="producto__footer">
                            <h4>{{ $producto->titulo }}</h4>
                            <div class="precio">{{ $producto->precio }}€</div>
                        </div>
                    </a>
                    <div class="bottom">
                        <div class="btn__group">
                            @if (in_array($producto->id, $productosStore))
                            <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                            @else
                            @if (Auth::check() && $carrito != null)
                            @if (in_array($producto->id, $idsCarrito))
                            <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @empty
                <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente
                    para
                    reponer
                    nuestro inventario lo antes posible.</p>
                @endforelse
            </div>
        </section>
        <section class="productos" id="productos">
            <div class="division_superior">
                <h3 class="titulos_sub">{{ $categoria3->titulo }}</h3>
                <a href="{{ route('productos.index') }}">Ver mas...</a>
            </div>

            <div class="productos__center" id="cats">

                @forelse ($cat3 as $producto)
                <div id="{{ $producto->id }}" class="producto">
                    <a href="{{ route('productos.show', $producto->id) }}">
                        <div class="imagen__container">
                            <img src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                        </div>
                        <div class="producto__footer">
                            <h4>{{ $producto->titulo }}</h4>
                            <div class="precio">{{ $producto->precio }}€</div>
                        </div>
                    </a>
                    <div class="bottom">
                        <div class="btn__group">
                            @if (in_array($producto->id, $productosStore))
                            <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                            @else
                            @if (Auth::check() && $carrito != null)
                            @if (in_array($producto->id, $idsCarrito))
                            <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @empty
                <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente
                    para
                    reponer
                    nuestro inventario lo antes posible.</p>
                @endforelse
            </div>
        </section>
        <section class="productos" id="productos">
            <div class="division_superior">
                <h3 class="titulos_sub">{{ $categoria4->titulo }}</h3>
                <a href="{{ route('productos.index') }}">Ver mas...</a>
            </div>

            <div class="productos__center" id="cats">

                @forelse ($cat4 as $producto)
                <div id="{{ $producto->id }}" class="producto">
                    <a href="{{ route('productos.show', $producto->id) }}">
                        <div class="imagen__container">
                            <img src="{{env('API_IMAGE') . $producto->id }}/1" alt="">
                        </div>
                        <div class="producto__footer">
                            <h4>{{ $producto->titulo }}</h4>
                            <div class="precio">{{ $producto->precio }}€</div>
                        </div>
                    </a>
                    <div class="bottom">
                        <div class="btn__group">
                            @if (in_array($producto->id, $productosStore))
                            <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                            @else
                            @if (Auth::check() && $carrito != null)
                            @if (in_array($producto->id, $idsCarrito))
                            <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                            @endif
                            @endif
                        </div>
                    </div>
                </div>
                @empty
                <p>Lamentablemente en este momento no contamos con productos disponibles. Trabajamos arduamente
                    para
                    reponer
                    nuestro inventario lo antes posible.</p>
                @endforelse
            </div>
        </section>
    </div>
</section>
@else
<div class="no-product">
    <h1 class="landing_noHay">Lamentablemente en este momento no contamos con productos suficientes. Trabajamos arduamente para reponer nuestro inventario lo antes posible.</h1>
    <img class="img_noHay" src="/img/No_products.png" alt="">
    <a href="{{ route('productos.index') }}" class="btn_todosProductos"><i class='bx bxs-grid-alt'></i> Ir a
        todos los productos </a>
</div>
@endif

@endsection

@section('js')
<script script src="/js/categorias.js"></script>
<script src="/js/header.js"></script>
@if (Auth::check())
<script src="/js/carritoIndexDB.js" type="module"></script>
@else
<script src="/js/carritoIndex.js" type="module"></script>
@endif
@endsection
