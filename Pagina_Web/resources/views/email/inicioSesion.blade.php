<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Inicio de sesión - DIP-Markeplace</title>
</head>
<body>
  <header style="background-color: #4E23D6; padding: 40px; text-align: center;">
    <h1 style="color: #fff; font-size: 24px; margin: 0;"><i class='bx bxs-box'></i> DIP-Markeplace</h1>
  </header>
  <main style="background-color: #fff; border: 1px solid #ddd; border-top: none; padding: 20px;">
    <p>Estimado/a <strong>{{$username->username}}</strong>,</p>
    <p>Te informamos que se ha iniciado sesión en tu cuenta de DIP-Markeplace desde la siguiente ip= '{{$request->ip()}}''.</p>
    <p>Si no reconoces esta actividad, por favor, restablece la contraseña.</p>
    <p><a href="{{ url('/recuperarContrasena') }}">Restablecer contraseña</a></p>
    <p>Si tienes alguna pregunta o necesitas ayuda, por favor no dudes en contactarnos.</p>
    <p>Gracias,</p>
    <p>El equipo de DIP-Markeplace</p>
  </main>
</body>
<style>
    /* Estilo global */
    body {
      background-color: #F3F3FF;
      color: #333;
      font-family: Arial, sans-serif;
      font-size: 16px;
      line-height: 1.5;
      margin: 0;
      padding: 0;
    }

    /* Enlaces */
    a {
      color: #4E23D6;
      text-decoration: none;
    }

    a:hover {
      text-decoration: underline;
    }

    /* Botones */
    .boton {
      background-color: #4E23D6;
      border: none;
      border-radius: 5px;
      color: #fff;
      cursor: pointer;
      display: inline-block;
      font-size: 16px;
      margin-top: 20px;
      padding: 10px 20px;
      text-align: center;
      text-decoration: none;
      transition: background-color 0.3s ease;
    }

    .boton:hover {
      background-color: #3C1CAE;
    }
  </style>
</html>