@extends('layouts.master')

@section('title', 'DIP Marketplace - SHOW')

@section('content')

    @if (Auth::check())
        <p id="carrito-value" hidden>{{ $carrito }}</p>
    @endif

    <div class="slider">
        <div class="slides">
            @foreach ($imagenes as $imagen)
                <div class="slide"><img src="{{ $imagen }}" alt="Image 1"></div>
            @endforeach
        </div>
        <button class="arrow arrow-left"><i class='bx bxs-chevron-left'></i></button>
        <button class="arrow arrow-right"><i class='bx bxs-chevron-right'></i></button>
    </div>
    <div id="{{ $producto->id }}" class="product-details">
        <h1>{{ $producto->titulo }}</h1>
        <p>{{ $producto->descripcion }} </p>
        @if ($producto->disponible == true)
            <h2>{{ $producto->precio }} €</h2>

            @if (Auth::check())
                @if (in_array($producto->id, $productosStore))
                    <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i
                            class='bx bxs-edit'></i></a>
                @else
                    @if ($producto->disponible == true)
                        @if ($checker == true)
                            <button class="btn_remove" id="btn_add">Eliminar de la <i
                                    class='bx bxs-shopping-bag'></i></button>
                        @else
                            <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                        @endif
                    @endif
                @endif
            @else
                <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
            @endif
        @else
            <h4 class="precio" style="color:red">PRODUCTO NO DISPONIBLE</h4>
        @endif
    </div>


    {{-- <div id="{{ $producto->id }}" class="detalles">
<article class="detalles__grid">
    <div class="slider">
        <div class="slides">
            @foreach ($imagenes as $imagen)
            <div class="slide"><img src="{{$imagen}}" alt="Image 1"></div>
            @endforeach
        </div>
        <button class="arrow arrow-left">&larr;</button>
        <button class="arrow arrow-right">&rarr;</button>
    </div>
    <article class="detalles__content">
        <h2>{{ $producto->titulo }}</h2>
        @if ($producto->disponible == true)
        <h4 class="precio">{{ $producto->precio }} €</h4>
        @else
        <h4 class="precio" style="color:red">PRODUCTO NO DISPONIBLE</h4>
        @endif
        <p class="descripcion"><b>Descripción: </b>{{ $producto->descripcion }}</p>
        <div class="bottom">
            <div class="btn__group">
                @if (in_array($producto->id, $productosStore))
                <a class="btn_add" href="{{ route('store.manage.edit', $producto->id) }}">Editar <i class='bx bxs-edit'></i></a>
                @else
                @if (Auth::check())
                @if ($producto->disponible == true)
                @if ($checker == true)
                <button class="btn_remove" id="btn_add">Eliminar de la <i class='bx bxs-shopping-bag'></i></button>
                @else
                <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>
                @endif
                @else
                @endif
                @else
                <button class="btn_add" id="btn_add">Añadir a la <i class='bx bxs-shopping-bag'></i></button>

                <button class="btn_add" id="btn_add">Comprar producto <i class='bx bxs-paper-plane'></i></button>
                @endif
                @endif
            </div>

        </div>
    </article>
    </div> --}}
@endsection

@section('js')
    <script src="/js/header.js"></script>
    <script src="/js/slider.js"></script>
    @if (Auth::check())
        <script src="/js/carritoShowDB.js" type="module"></script>
    @else
        <script src="/js/carritoShow.js" type="module"></script>
    @endif
@endsection
