<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Store extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function owner($id_tienda)
    {
        if (Auth::user()) {
            $tienda = Store::where('user_id', Auth::user()->id)->first();
            if ($tienda) {
                if ($tienda->id === $id_tienda) {
                    return true;
                }
            }
        }
        return false;
    }

    public function deshabilitar($producto)
    {
        $producto->disponible = false;
        $producto->save();
    }

    public function habilitar($producto)
    {
        $producto->disponible = true;
        $producto->save();
    }
}
