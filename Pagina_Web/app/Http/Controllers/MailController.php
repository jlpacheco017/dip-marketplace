<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Illuminate\Support\Facades\Log;

class MailController extends Controller
{
    public function index()
    {
            return view('email.index');
    }
    public function sendEmail(Request $request)
    {
        $mail = new PHPMailer(true);

            // Detalles del servidor de correo electrónico de Mailtrap
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host       = env('MAIL_HOST');
            $mail->SMTPAuth   = env('MAIL_MAILER');
            $mail->Username   = env('MAIL_USERNAME');
            $mail->Password   = env('MAIL_PASSWORD');
            $mail->SMTPSecure = env('MAIL_MAILER');
            $mail->Port       = env('MAIL_PORT');

            // Destinatario
            $mail->setFrom($request->email, 'Iván Reyes');
            $mail->addAddress('destinatario@mailtrap.io', 'Destinatario');

            // Contenido del correo electrónico
            $mail->isHTML(true);
            $mail->Subject = 'Asunto del correo';
            $mail->Body    = 'Este es el contenido del correo electrónico';

            $mail->send();
            Log::info('El correo electrónico ha sido enviado correctamente.');
            return redirect()->route('auth.showLogin');
    }
}
