Vagrant.configure("2") do |config|
  config.vm.define "api" do |api|
    api.vm.box = "debian/bullseye64"
    api.vm.hostname = "api"
    api.vm.network "private_network", ip: "172.16.50.60"
    api.vm.network "forwarded_port", guest: 80, host: 8000
    api.vm.provider "virtualbox" do |vb|
      vb.name = "api" 
      vb.memory = "1024"
      vb.cpus = 1
    end
    config.vm.provision "shell", inline: <<-SHELL
    echo -e "\n########## Instalamos Apache2 y PHP ##########\n"
    apt-get install -y apache2
    a2enmod rewrite
    sudo systemctl restart apache2.service
    
    echo -e "\n########## Instalamos PHP 8.2 y módulos requeridos ##########\n"
    
    apt-get install -y apt-transport-https lsb-release ca-certificates
    wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list
    apt-get update
    apt-get install -y php8.2 libapache2-mod-php8.2 php8.2-cli php8.2-mbstring php8.2-dom php8.2-mysql php8.2-gd php8.2-curl php8.2-zip unzip p7zip openssl php-mysql php-sqlite3
    sudo systemctl restart apache2.service

    sudo cp -r /vagrant/ /var/www/html/
    sudo mv /var/www/html/vagrant /var/www/html/api

    chown -R www-data:www-data /var/www/html/api/
    chmod -R 775 /var/www/html/api/
    chmod -R 777 /var/www/html/api/storage

    echo -e "\n########## Copiamos el archivo api.conf a /etc/apache2/sites-available y habilitamos el sitio ##########\n"

    cp /vagrant/api.conf /etc/apache2/sites-available
    cd /etc/apache2/sites-available/
    a2dissite 000-default.conf
    a2ensite api.conf
    sudo systemctl restart apache2.service

    echo -e "\n########## Instalamos Curl y Composer ##########\n"

    sudo apt install curl -y
    curl -sS https://getcomposer.org/installer | php 
    mv composer.phar /usr/local/bin/composer
    # apt-get install composer -y
    
    apt-get install git -y

    cp /vagrant/php.ini /etc/php/8.2/apache2/
    sudo touch /var/www/html/api/storage/logs/laravel.log
    sudo chmod -R 777 /var/www/html/api/storage/logs/laravel.log

    cp /vagrant/php.ini /etc/php/8.2/apache2/

    cd /var/www/html/api/ 

    cp .env.example .env
    chown -R www-data:www-data /var/www/html/api/
    chmod -R 775 /var/www/html/api/
    chmod -R 777 /var/www/html/api/storage

    echo -e "\n########## Vamos a nuestra Página Web y ejecutamos el composer install ##########\n"
    # cd /var/www/html/Pagina_Web/ && sudo composer install
    cd /var/www/html/api/ 
    sudo composer update
    sudo composer install --ignore-platform-req=ext-pdo_sqlite --ignore-platform-req=ext-sqlite3

    php artisan key:generate
    php artisan storage:link

    sudo systemctl stop apache2.service
    nohup php artisan serve --port=80 --host=172.16.50.60 >/dev/null 2>&1 &
    echo -e "\n########## DIP API OK ##########\n"
    SHELL
  end
end