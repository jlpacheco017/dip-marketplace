export default class Producto {
    id;
    img;
    titulo;
    precio;

    constructor(id, img, titulo, precio) {
        this.id = id;
        this.img = img;
        this.titulo = titulo;
        this.precio = precio;
    }
}