@extends('layouts.master')

@section('content')
<div id="login-container">
    <form method="POST" id="auth-form" action="{{ route('auth.doRecoveryPassword') }}">
        @csrf
        <h4>RECUPERAR LA CONTRASEÑA</h4>
        <div class="form-group">
            <label for="email">Correo electrónico: </label>
            <input type="email" id="email" name="email" required value="{{ old('email') }}">
        </div>
        @error('email')
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $message }}
        </div>
        @enderror
        @if(session('message'))
        <div class="alert-success">
            <i class='bx bx-check-square'></i> {{ session('message') }}
        </div>
        @endif
        <div class="form-group">
            <button type="submit">Enviar correo</button>
        </div>
    </form>

</div>
@endsection
@section('js')
<script src="/js/login_carrito.js"></script>
<script src="/js/header.js"></script>
@endsection
