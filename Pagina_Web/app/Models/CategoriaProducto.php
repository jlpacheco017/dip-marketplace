<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoriaProducto extends Model
{
    use HasFactory;

    protected $table = 'categoria_producto';
    protected $fillable = [
        'categoria_id',
        'producto_id',
    ];
    public $timestamps = false;
    
    // Relación con la tabla Categoría
    public function categoria()
    {
        return $this->belongsTo('App\Models\Categoria');
    }

    // Relación con la tabla Producto
    public function producto()
    {
        return $this->belongsTo('App\Models\Producto');
    }
}
