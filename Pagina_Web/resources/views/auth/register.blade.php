@extends('layouts.master')

@section('content')
<div id="login-container">
    <form method="POST" id="auth-form">
        @csrf
        <h4>REGISTRARSE</h4>
        <div class="form-group">
            <label for="username">Nombre de usuario: </label>
            <input type="text" id="username" name="username" value="{{ old('username') }}" required>
            <div class="feedback_register">
                <p hidden id="characters" class="text-p-small errorpass"> ● No puede contener caracteres especiales y espacios.</p>
            </div>
        </div>
        <div class="form-group">
            <label for="email">Correo electrónico: </label>
            <input type="email" id="email" name="email" value="{{ old('email') }}" required>
            <div class="feedback_register">
                <p hidden id="emailText" class="text-p-small errorpass"> ● No es una dirección de correo válida.</p>
            </div>
        </div>
        <div class="form-group">
            <label for="password">Contraseña: </label>
            <input type="password" id="password" name="password" required>
            <div class="feedback_register">
                <p hidden id="lenght" class="text-p-small errorpass">● Debe contener almenos 8 caracteres.</p>
                <p hidden id="mayuscula" class="text-p-small errorpass">● Una letra en mayúscula.</p>
                <p hidden id="minuscula" class="text-p-small errorpass">● Una letra en minúscula.</p>
                <p hidden id="numero" class="text-p-small errorpass"> ● Un número.</p>
            </div>

        </div>
        <div class="form-group">
            <label for="password_confirmation">Repite la Contraseña</label>
            <input type="password" id="password_confirmation" name="password_confirmation" required>
            <div class="feedback_register">
                <div id="passEquals">
                </div>
                <p hidden id="passEquals" class="text-p-small errorpass">● Las contraseñas no coinciden.</p>
            </div>
        </div>
        @if ($errors->has('username'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('username') }}
        </div>
        @endif
        @if ($errors->has('email'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('email') }}
        </div>
        @endif
        @if ($errors->has('password'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('password') }}
        </div>
        @endif
        @if ($errors->has('password_confirmation'))
        <div class="error-message">
            <i class='bx bx-error'></i> {{ $errors->first('password_confirmation') }}
        </div>
        @endif
        <div class="form-group">
            <button type="submit" id="btn_avance">Registrarse</button>
        </div>
        <div class="signup-link">
            <p>¿Ya tienes una cuenta? <a href="{{ route('auth.showLogin') }}">Entra</a></p>
        </div>

    </form>
</div>
@endsection

@section('js')
<script src="/js/header.js"></script>
<script src="/js/passwordCheck.js"></script>
<script src="/js/login_carrito.js"></script>

@endsection
