<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LineasCarrito extends Model
{
    use HasFactory;

    protected $fillable = [
        'carrito_id',
        'producto_id',
        'precio'
    ];

    public function carrito()
    {
        return $this->belongsTo(Carrito::class, 'carrito_id');
    }
    
    public function producto()
    {
        return $this->belongsTo(Producto::class, 'producto_id');
    }
    public function getLineaCarritoPorCarritoId($id){
        return $this->query()->where('carrito_id', $id)->get();
    }
}
